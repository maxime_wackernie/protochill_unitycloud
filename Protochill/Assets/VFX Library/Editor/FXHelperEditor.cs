﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class FXHelperEditor : Editor
{
    [MenuItem("GAMELOFT/FXHelper/Order ALL Particle System")]
    private static void OrderParticleSystem()
    {
        foreach(GameObject gameObject in Selection.gameObjects)
        {
            ParticleSystemRenderer[] particleSystems = gameObject.GetComponentsInChildren<ParticleSystemRenderer>();

            for (int i = 0; i < particleSystems.Length; i++)
            {
                particleSystems[i].sortingOrder = particleSystems.Length - i;
            }

            EditorUtility.SetDirty(gameObject);
        }
    }

    [MenuItem("GAMELOFT/FXHelper/Remove Trail Material")]
    private static void RemoveTrailMaterial()
    {
        foreach (GameObject gameObject in Selection.gameObjects)
        {
            ParticleSystemRenderer[] particleSystems = gameObject.GetComponentsInChildren<ParticleSystemRenderer>();

            for (int i = 0; i < particleSystems.Length; i++)
            {
                particleSystems[i].trailMaterial = null;
            }

            EditorUtility.SetDirty(gameObject);
        }
    }
}
