﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TileFood", menuName = "Data/Tile/TileFood", order = 1)]
public class TileFood : TileResource
{
    [Header("TileFood")]
    [SerializeField]
    private Sprite slotSprite = null;
    [SerializeField]
    private GameObject foodPrefab = null;
    [SerializeField]
    private ProductionData slotCapacity = null;
    [SerializeField]
    private ProductionData cooldownBonus = null;
    [SerializeField]
    private ProductionData eatBonus = null;
    [SerializeField]
    private float regrowDuration = 10f;
    [SerializeField]
    private float eatDuration = 5f;

    public Sprite SlotSprite { get { return slotSprite; } }
    public GameObject FoodPrefab { get { return foodPrefab; } }
    public ProductionData SlotCapacity { get { return slotCapacity; } }
    public ProductionData CooldownBonus { get { return cooldownBonus; } }
    public ProductionData EatBonus { get { return eatBonus; } }
    public float RegrowDuration { get { return regrowDuration; } }
    public float EatDuration { get { return eatDuration; } }

    public override void OnNeighbourUpdate(MyTile myTile, MyTile neighbour)
    {
    }

    public override void OnClick(MyTile myTile)
    {
        base.OnClick(myTile);

        FoodInfoUI.Instance.Show(myTile.GetComponent<Farm>());
    }

    public override void OnPlace(MyTile myTile, bool load)
    {
        base.OnPlace(myTile, load);

        myTile.gameObject.AddComponent<Farm>().Init(this, myTile, load);
    }

    public override void OnStartDrag()
    {
        base.OnStartDrag();

        DragHandler.Instance.SetDarkerTile(true, DragHandler.isNotEditable, DragHandler.hasTileResource);
    }

    public override bool OnUpdateDrag(MyTile myTile)
    {
        return IsLocationValid(myTile);
    }

    public override void OnEndDrag(MyTile myTile)
    {
        base.OnEndDrag(myTile);
    }
}
