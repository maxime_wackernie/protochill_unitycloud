﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;
using DG.Tweening;

public class Farm : BaseResourceComponentType<TileFood>
{
    private GridLayoutGroup3D gridLayoutGroup;

    private Dictionary<Animal, int> queuedAnimals = new Dictionary<Animal, int>();
    private List<int> slotTakenIds = new List<int>();

    public System.Action slotUpdatedEvent;

    public int SlotTakenCount { get { return slotTakenIds.Count; } }
    public int AvailableSlotCount { get { return SlotCount - SlotTakenCount; } }
    public int SlotCount { get { return (int)tileResource.SlotCapacity.GetProduction(level + 1); } }

    public float RegrowDuration 
    {
        get { return tileResource.RegrowDuration * (1f - (tileResource.CooldownBonus.GetProduction(level) / 100f)); } 
    }
    public float EatDuration 
    { 
        get { return tileResource.EatDuration * (1f - (tileResource.EatBonus.GetProduction(level) / 100f)); } 
    }

    public bool HasAvailableSlot { get { return slotTakenIds.Count < SlotCount; } }

    private bool isInitialized;

    protected override void Awake()
    {
        base.Awake();

        gridLayoutGroup = GetComponentInChildren<GridLayoutGroup3D>();
    }

    private void Start()
    {
    }

    public override BaseResourceComponentSave GetSave()
    {
        return null;
    }

    public override void OnStartReset()
    {
        base.OnStartReset();

        myTile.OnTileDefaultPosition -= MyTile_OnTileDefaultPosition;
    }

    public override void OnRemove()
    {
        base.OnRemove();

        foreach (Animal animal in queuedAnimals.Keys)
        {
            animal.CurrentNeed.NeedBehaviour.NeedFailed(animal);
        }

        LevelManager.Instance.Farms.Remove(this);
    }

    public override void Init(TileFood tileResource, MyTile myTile, bool load)
    {
        LevelManager.Instance.Farms.Add(this);
        myTile.OnTileDefaultPosition += MyTile_OnTileDefaultPosition;

        if (load)
        {
            gridLayoutGroup.Init();
            isInitialized = true;
        }

        base.Init(tileResource, myTile, load);
    }

    private void MyTile_OnTileDefaultPosition()
    {
        isInitialized = true;

        gridLayoutGroup.Init();
        UpdateFood(true);
    }

    protected override void OnLevelUpdate()
    {
        UpdateFood(false);
    }

    private void UpdateFood(bool doAnim)
    {
        if (isInitialized == false)
            return;

        Transform instanceTr;
        int count = (int)SlotCount;
        int diff = count - gridLayoutGroup.transform.childCount;
        if (diff > 0)
        {
            for (int i = 0; i < diff; ++i)
            {
                instanceTr = GameObject.Instantiate(tileResource.FoodPrefab, gridLayoutGroup.transform).transform;
                if (doAnim)
                {
                    instanceTr.localScale = Vector3.zero;

                    Sequence sequence = DOTween.Sequence();
                    sequence.SetDelay(i * 0.05f);
                    sequence.Append(instanceTr.DOScale(1.1f, 0.2f));
                    sequence.Append(instanceTr.DOScale(1f, 0.1f));
                    sequence.Play();
                }
            }
        }

        gridLayoutGroup.Refresh();
    }

    public bool EnterQueue(Animal animal, out Transform slot)
    {
        int id = GetFreeSlot();
        if (id != -1)
        {
            animal.NeedBlackboard.farm = this;
            queuedAnimals.Add(animal, id);
            slotTakenIds.Add(id);

            slot = gridLayoutGroup.transform.GetChild(id);

            if (slotUpdatedEvent != null)
            {
                slotUpdatedEvent.Invoke();
            }
        }
        else
        {
            slot = null;
        }

        return id != -1;
    }

    public void DoEat(Animal animal, System.Action<Animal> callback)
    {
        float duration = EatDuration;
        animal.AnimalUI.DoNeedProgress(duration, () => {
            if (callback != null)
            {
                callback.Invoke(animal);
            }

            SlotEaten(animal);
        });

        animal.NeedBlackboard.isEating = true;
    }

    private void SlotEaten(Animal animal)
    {
        int slot = queuedAnimals[animal];
        ReloadSlot(slot);
        queuedAnimals.Remove(animal);
    }

    private void ReloadSlot(int slotId)
    {
        StartCoroutine(ReloadSlotEnumerator(slotId, RegrowDuration));
    }
     
    private IEnumerator ReloadSlotEnumerator(int slotId, float duration)
    {
        if (gridLayoutGroup.transform == null)
            yield break;

        Transform slot = gridLayoutGroup.transform.GetChild(slotId);

        float time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;
            slot.transform.localScale = slot.transform.localScale = Vector3.one * (time / duration);
            yield return null;
        }

        slot.localScale = Vector3.one;
        slotTakenIds.Remove(slotId);

        if (slotUpdatedEvent != null)
        {
            slotUpdatedEvent.Invoke();
        }
    }

    private int GetFreeSlot()
    {
        for (int i = 0; i < gridLayoutGroup.transform.childCount; ++i)
        {
            if (slotTakenIds.Contains(i) == false)
            {
                return i;
            }
        }

        return -1;
    }
}
