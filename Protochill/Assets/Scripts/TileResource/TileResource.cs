﻿using DG.Tweening;
using GLUnity.IOC;
using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TileResource : ScriptableObject
{
    [Header("TileResource")]
    [SerializeField]
    private string countKey = string.Empty;
    [SerializeField]
    private string levelKey = string.Empty;
    [SerializeField]
    protected ETileType allowedTile = ETileType.NONE;

    [SerializeField]
    private IncrementalData buyCost = null;
    [SerializeField]
    private IncrementalData upgradeCost = null;
    [SerializeField]
    private ProductionData productionStep = null;

    [SerializeField]
    private Sprite resourceSprite = null;
    [SerializeField]
    private string resourceName = null;

    [SerializeField]
    private GameObject prefab = null;

    [SerializeField]
    public GameObject spawnParticles;
    [SerializeField]
    public float spawnParticlesOffsetY;

    public IncrementalData BuyCost { get { return buyCost; } }
    public IncrementalData UpgradeCost { get { return upgradeCost; } }
    public ProductionData ProductionStep { get { return productionStep; } }
    public string ResourceName { get { return resourceName; } }
    public string CountKey { get { return countKey; } }
    public string LevelKey { get { return levelKey; } }
    public Sprite ResourceSprite { get { return resourceSprite; } }
    public GameObject Prefab { get { return prefab; } }

    private void SpawnPrefab(MyTile myTile, bool doAnim)
    {
        if (prefab != null)
        {
            Transform instance = GameObject.Instantiate(prefab, myTile.transform).transform;
            instance.position += LevelManager.Instance.Offset;

            if (doAnim)
            {
                instance.DOScale(Vector3.zero, TweenValueManager.Instance.spawnDuration).From();
                instance.DOMoveY(instance.position.y + TweenValueManager.Instance.heightSpawnOffset, TweenValueManager.Instance.heightMoveDuration).From().SetEase(Ease.InBack);
                MMVibrationManager.Haptic(HapticTypes.LightImpact);
            }

            myTile.tileResourcePrefab = instance.gameObject;
        }
    }

    public BaseResourceComponentSave GetSave(MyTile myTile)
    {
        return myTile.GetComponent<BaseResourceComponent>().GetSave();
    }

    public void Load(Island island, MyTile myTile, BaseResourceComponentSave save)
    {
        OnPlace(myTile, true);

        myTile.GetComponent<BaseResourceComponent>().Load(island, save);
    }

    public virtual void OnClick(MyTile myTile)
    {
        myTile.GetComponent<BaseResourceComponent>().OnInfoMenuOpen();
    }

    public virtual void OnPlace(MyTile myTile, bool load)
    {
        LevelManager.Instance.RaiseResourcePlaced(myTile, this);

        myTile.TileMeshRenderer.material = LevelManager.Instance.GetTileMaterial(myTile.tileType, true);

        SpawnPrefab(myTile, !load);

        myTile.spawnParticlesHabitat = spawnParticles;
        myTile.spawnParticlesOffsetY = spawnParticlesOffsetY;
    }

    public void StartReset(MyTile myTile)
    {
        BaseResourceComponent baseResourceComponent = myTile.GetComponent<BaseResourceComponent>();
        if (baseResourceComponent)
        {
            baseResourceComponent.OnStartReset();
        }
    }

    public virtual void OnRemove(MyTile myTile)
    {
        myTile.SpawnDustParticle(0f);
        myTile.TileMeshRenderer.material = LevelManager.Instance.GetTileMaterial(myTile.tileType, false);

        BaseResourceComponent baseResourceComponent = myTile.GetComponent<BaseResourceComponent>();
        if (baseResourceComponent)
        {
            baseResourceComponent.OnRemove();
            Destroy(baseResourceComponent);
        }
    }

    public abstract void OnNeighbourUpdate(MyTile myTile, MyTile neighbour);

    public virtual bool IsLocationValid(MyTile myTile)
    {
        return myTile != null 
            && myTile.IsEditable // is editable
            && myTile.TileResource == null // no resource
            && (allowedTile != ETileType.NONE ? allowedTile == myTile.tileType : true); // allowed type
    }

    public virtual void OnStartDrag()
    {
        ShopUI.Instance.CloseCategory();

        DragHandler.Instance.InvokeStartDrag();

        if (allowedTile != ETileType.NONE)
        {
            GetTileCondition isNotTileType = new GetTileCondition(DragHandler.IsNotTileType, allowedTile);

            DragHandler.Instance.SetDarkerTile(true, 
                DragHandler.isNotEditable,
                DragHandler.hasTileResource,
                isNotTileType);
        }
    }

    public abstract bool OnUpdateDrag(MyTile myTile);

    public virtual void OnEndDrag(MyTile myTile)
    {
        DragHandler.Instance.InvokeEndDrag();

        DragHandler.Instance.SetDarkerTile(false, DragHandler.isDark);

        /*if (IsLocationValid(myTile) == false)
        {
            ShopUI.Instance.OpenPreviousCategory();
        }*/
    }
}
