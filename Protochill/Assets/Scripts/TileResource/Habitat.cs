﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using GLUnity;
using DG.Tweening;
using UnityEngine.UIElements;
using GameAnalyticsSDK.Setup;

[System.Serializable]
public class HabitatSave : BaseResourceComponentSave
{
    public HabitatSave(Habitat habitat)
    {
        linksSaves = new LinkSave[habitat.Links.Count];

        for (int i = 0; i < linksSaves.Length; ++i)
        {
            linksSaves[i] = new LinkSave(habitat.Links[i]);
        }

        if (habitat.Recipes.Count > 0)
        {
            recipesId = new int[habitat.Recipes.Count];
            for (int i = 0; i < recipesId.Length; ++i)
            {
                recipesId[i] = habitat.TileResource.GetRecipeId(habitat.Recipes[i]);
            }
        }

        int count = habitat.HatchingEggs.Count + habitat.ReadyEggs.Count;
        if (count > 0)
        {
            eggSaves = new EggSave[count];

            int id = 0;
            // save ready eggs
            foreach (EggHatchData eggHatchData in habitat.ReadyEggs)
            {
                eggSaves[id] = new EggSave(eggHatchData);
                ++id;
            }

            //save hatching eggs
            foreach (EggHatchData eggHatchData in habitat.HatchingEggs)
            {
                eggSaves[id] = new EggSave(eggHatchData);
                ++id;
            }
        }
    }

    public EggSave[] eggSaves;
    public LinkSave[] linksSaves;
    public int[] recipesId;
}

[System.Serializable]
public struct LinkSave
{
    public LinkSave(Link link)
    {
        mainTile = new SerializableVector2Int(link.mainHabitat.MyTile.GridPosition);

        if (link.neighbours != null)
        {
            neighbours = new SerializableVector2Int[link.neighbours.Count];
            for (int i = 0; i < neighbours.Length; ++i)
            {
                neighbours[i] = new SerializableVector2Int(link.neighbours[i].GridPosition);
            }
        }
        else
        {
            neighbours = null;
        }
    }

    public SerializableVector2Int mainTile;
    public SerializableVector2Int[] neighbours;
}

public class Link
{
    public Link(Habitat mainHabitat, List<MyTile> neighbours)
    {
        this.mainHabitat = mainHabitat;
        this.neighbours = neighbours;
    }

    public Link(MyTileMapComponent tileMapComponent, LinkSave linkSave)
    {
        mainHabitat = tileMapComponent.TileMap.GetTile(linkSave.mainTile.ToVector2Int()).GetComponent<Habitat>();

        if (linkSave.neighbours != null)
        {
            neighbours = new List<MyTile>();
            foreach (SerializableVector2Int gridPosition in linkSave.neighbours)
            {
                neighbours.Add(tileMapComponent.TileMap.GetTile(gridPosition.ToVector2Int()));
            }
        }
    }


    public Habitat mainHabitat;
    public List<MyTile> neighbours;
}

public class Habitat : BaseResourceComponentType<TileHabitat>
{
    private List<EggHatchData> hatchingEggs = new List<EggHatchData>();
    private List<EggHatchData> readyEggs = new List<EggHatchData>();

    private List<Animal> animals = new List<Animal>();
    private List<Recipe> recipes = new List<Recipe>();

    private EggTimerUI eggTimerUI;

    public System.Action addEggEvent;
    public System.Action eggHatchEvent;

    private List<Link> links = new List<Link>();

    private List<MyTile> roadLinks = new List<MyTile>();
    private List<GameObject> roadObjects = new List<GameObject>();

    public List<Link> Links { get { return links; } }
    public List<Recipe> Recipes { get { return recipes; } }
    public List<Animal> Animals { get { return animals; } }
    public List<EggHatchData> HatchingEggs { get { return hatchingEggs; } }
    public List<EggHatchData> ReadyEggs { get { return readyEggs; } }

    private List<int> timersIdEgg = new List<int>();
    private GameObject clickableEgg;
    protected override void OnLevelUpdate()
    {
    }

    public override void OnRemove()
    {
        base.OnRemove();

        links.Clear();
        foreach (GameObject r in roadObjects)
            Destroy(r);
        roadObjects.Clear();
        roadLinks.Clear();

        eggTimerUI?.OnDestroy();
        eggHatchEvent = null;
        hatchingEggs.Clear();
        readyEggs.Clear();

        foreach(int i in timersIdEgg)
            TimerManager.Instance.RemoveTimer(i);

        timersIdEgg.Clear();
        if (clickableEgg != null)
            Destroy(clickableEgg);

        foreach (Animal a in Animals)
            a.Destroy();
    }

    public override void Init(TileHabitat tileHabitat, MyTile myTile, bool load)
    {
        base.Init(tileHabitat, myTile, load);

        if (tileResource.IsAddOn == false)
        {
            eggTimerUI = PrefabHolder.Instance.InstantiatePrefab<EggTimerUI>("egg_timer");
            eggTimerUI.Init(this);
        }

        if (load == false && tileHabitat.StartEgg != null)
        {
            Link link = new Link(this, null);
            AddHatchingEgg(tileHabitat.StartEgg, ERarity.COMMON, GetHatchDuration(ERarity.COMMON), link);
        }
    }

    #region Save
    public override BaseResourceComponentSave GetSave()
    {
        return new HabitatSave(this);
    }

    public override void Load(Island island, BaseResourceComponentSave save)
    {
        base.Load(island, save);

        HabitatSave habitatSave = (HabitatSave)save;

        LoadLinks(island, habitatSave);
        LoadRecipes(habitatSave);
        LoadEggs(habitatSave);
    }

    private void LoadLinks(Island island, HabitatSave habitatSave)
    {
        Link current;
        if (habitatSave.linksSaves != null)
        {
            foreach (LinkSave linkSave in habitatSave.linksSaves)
            {
                current = new Link(island.TileMapComponent, linkSave);
                links.Add(current);

                // load roads
                if (current.neighbours != null)
                {
                    foreach (MyTile fn in current.neighbours)
                    {
                        CreateHabitatConnection(fn, false);
                    }
                }
            }
        }
    }

    private void LoadRecipes(HabitatSave habitatSave)
    {
        if (habitatSave.recipesId != null)
        {
            foreach (int id in habitatSave.recipesId)
            {
                recipes.Add(tileResource.Recipes[id]);
            }
        }
    }

    private void LoadEggs(HabitatSave habitatSave)
    {
        float duration;
        if (habitatSave.eggSaves != null)
        {
            foreach (EggSave eggSave in habitatSave.eggSaves)
            {
                if (eggSave.isReady == false)
                {
                    duration = eggSave.timeLeft - SaveManager.offlineSeconds;
                    if (duration > 0f)
                    {
                        AddHatchingEgg(SaveManager.Instance.GetScriptableObject<EggData>(eggSave.eggDataKey), eggSave.eggRarity, duration, null);
                        return;
                    }

                    ++SaveManager.offlineReadyEggs[eggSave.type];
                }

                AddReadyEgg(new EggHatchData(eggSave, this));
            }
        }
    }

    public void LoadEggs()
    {
        float timeLeft;
        EggHatchData eggHatchData;
        int i = 0;
        while (i < hatchingEggs.Count)
        {
            eggHatchData = hatchingEggs[i];
            timeLeft = TimerManager.Instance.GetTimerTimeLeft(eggHatchData.id) - SaveManager.offlineSeconds;
            if (timeLeft > 0)
            {
                TimerManager.Instance.SetTimerDuration(eggHatchData.id, timeLeft, true);
                ++i;
            }
            else
            {
                ++SaveManager.offlineReadyEggs[myTile.tileType];
                OnEggTimerComplete(eggHatchData.id);
            }
        }
    }
    #endregion

    public void OnRecipe(Recipe recipe, List<MyTile> neighbours)
    {
        recipes.Add(recipe);

        Link link = new Link(this, neighbours);
        AddHatchingEgg(recipe.EggData, recipe.EggRarity, GetHatchDuration(recipe.EggRarity), link);

        foreach (MyTile fn in neighbours)
        {
            CreateHabitatConnection(fn, true);
        }
    }

    private float GetHatchDuration(ERarity rarity)
    {
        return LevelManager.Instance.RarityData.GetRarityData(rarity).hatchDuration * (1f - (tileResource.HatchReduceProduction.GetProduction(level) / 100f));
    }
    
    public void CreateHabitatConnection(MyTile resource, bool doAnim)
    {
        if (!roadLinks.Contains(resource))
        {
            roadLinks.Add(resource);
            GameObject road = PrefabHolder.Instance.GetPrefab("road");
            GameObject r1 = Instantiate(road, transform);
            GameObject r2 = Instantiate(road, transform);
            roadObjects.Add(r1);
            roadObjects.Add(r2);

            Vector3 pos = transform.position;
            pos.y = road.transform.position.y + transform.position.y;
            r1.transform.parent = transform;
            r1.transform.position = pos;
            r1.transform.Rotate(0f, 0f, RoadHaveToRotate(resource));

            pos = resource.transform.position;
            pos.y = road.transform.position.y + resource.transform.position.y;
            r2.transform.parent = resource.transform;
            r2.transform.position = pos;
            r2.transform.Rotate(0f, 0f, RoadHaveToRotate(resource) + 180f);

            if (doAnim)
            {
                r1.transform.DOScaleY(0f, 1f).From().SetDelay(TweenValueManager.Instance.spawnRoadDelay);
                r2.transform.DOScaleY(0f, 1f).From().SetDelay(TweenValueManager.Instance.spawnRoadDelay);
            }
        }
    }

    public float RoadHaveToRotate(MyTile resource)
    {
        Vector3 dif = (resource.transform.position - this.transform.position);
        if (dif.x != 0f)
        {
            if (dif.x < 0f)
                return 90f;
            if (dif.x > 0f)
                return -90f;
        }
        else
        {
            if (dif.z > 0f)
                return 0f;
            if (dif.z < 0f)
                return 180f;
        }
        return 0f;
    }

    public bool HasRecipe(Recipe recipe)
    {
        return recipes.Contains(recipe);
    }

    public bool HasRarerRecipe(Recipe recipe)
    {
        foreach (Recipe it in recipes)
        {
            if (it.TileHabitats.Length > recipe.TileHabitats.Length)
            {
                return true;
            }
        }

        return false;
    }

    public bool HasInLink(List<MyTile> neighbours)
    {
        foreach (Link link in links)
        {
            foreach (MyTile tile in neighbours)
            {
                if (link.neighbours != null && link.neighbours.Contains(tile))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool HasLink(List<MyTile> neighbours)
    {
        if (links.Count == 0)
        {
            return false;
        }

        neighbours.Sort((x, y) => System.Convert.ToInt32(x.Position.magnitude > y.Position.magnitude));

        foreach (Link link in links)
        {
            if (link.neighbours != null)
            {
                if (link.neighbours.Count != neighbours.Count)
                    continue;

                if (link.neighbours.Count > 1)
                {
                    link.neighbours.Sort((x, y) => System.Convert.ToInt32(x.Position.magnitude > y.Position.magnitude));

                    if (Enumerable.SequenceEqual(neighbours, link.neighbours))
                    {
                        return true;
                    }
                }
                else
                {
                    return link.neighbours[0] == neighbours[0];
                }
            }
        }

        return false;
    }

    private EggHatchData GetEggHatch(int id)
    {
        foreach (EggHatchData egg in hatchingEggs)
        {
            if (egg.id == id)
            {
                return egg;
            }
        }

        return null;
    }

    private void OnEggTimerComplete(int id)
    {
        EggHatchData eggHatchData = GetEggHatch(id);

        AddReadyEgg(eggHatchData);
        hatchingEggs.Remove(eggHatchData);
        timersIdEgg.Remove(id);

        if (eggHatchEvent != null)
        {
            eggHatchEvent.Invoke();
        }

        TimerManager.Instance.RemoveTimer(id);
    }

    private void AddHatchingEgg(EggData eggData, ERarity rarity, float duration, Link link)
    {
        if (link != null)
        {
            links.Add(link);
        }
        int timerIdEgg = 0;
        timerIdEgg = TimerManager.Instance.AddTimer(duration, true, false, OnEggTimerComplete);
        timersIdEgg.Add(timerIdEgg);
        EggHatchData eggHatchData = new EggHatchData(timerIdEgg, eggData, rarity, this, false);
        hatchingEggs.Add(eggHatchData);

        if (addEggEvent != null)
        {
            addEggEvent.Invoke();
        }
    }

    private void AddReadyEgg(EggHatchData egg)
    {
        if (readyEggs.Count == 0)
        {
            ClickableEgg c = PrefabHolder.Instance.InstantiatePrefab<ClickableEgg>("egg");
            c.Init(egg, myTile.tileType);
            clickableEgg = c.gameObject;
        }

        egg.isReady = true;
        readyEggs.Add(egg);
    }

    public void RemoveReadyEgg(EggHatchData egg)
    {
        if (readyEggs.Remove(egg) == false)
        {
            Debug.LogError("egg not found");
        }

        if (readyEggs.Count > 0)
        {
            PrefabHolder.Instance.InstantiatePrefab<ClickableEgg>("egg").Init(readyEggs[0], MyTile.tileType);
        }
    }
}