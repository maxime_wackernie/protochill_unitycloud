﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class BaseResourceComponentSave
{
}

public abstract class BaseResourceComponent : MonoBehaviour
{
    protected MyTile myTile;

    protected int level = 0;

    protected Generator levelGenerator;

    public System.Action levelUpdateEvent;
    public System.Action infoMenuOpen;
    public System.Action infoMenuClose;

    public MyTile MyTile { get { return myTile; } }

    public int Level
    {
        get { return level; }
        set
        {
            level = value;

            OnLevelUpdate();

            if (levelUpdateEvent != null)
            {
                levelUpdateEvent.Invoke();
            }
        }
    }

    protected virtual void Awake()
    {
        myTile = GetComponent<MyTile>();
    }

    public abstract BaseResourceComponentSave GetSave();

    public virtual void Load(Island island, BaseResourceComponentSave save)
    {

    }

    protected abstract void OnLevelUpdate();

    public void OnInfoMenuOpen()
    {
        if (infoMenuOpen != null)
        {
            infoMenuOpen.Invoke();
        }
    }

    public void OnInfoMenuClose()
    {
        if (infoMenuClose != null)
        {
            infoMenuClose.Invoke();
        }
    }

    protected void OnGeneratorLevelUpdate()
    {
        Level = levelGenerator.Level;
    }

    public virtual void OnStartReset()
    {
        if (levelGenerator != null)
        {
            levelGenerator.levelUpdateEvent -= OnGeneratorLevelUpdate;
            levelGenerator = null;
        }
    }

    public virtual void OnRemove()
    {
        Destroy(myTile.tileResourcePrefab);
        myTile.tileResourcePrefab = null;
    }
}
