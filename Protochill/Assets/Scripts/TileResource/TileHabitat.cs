﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RecipeCheck
{
    public RecipeCheck(Recipe recipe)
    {
        this.check = new Dictionary<TileHabitat, bool>(recipe.TileHabitats.Length);

        foreach (TileHabitat tileHabitat in recipe.TileHabitats)
        {
            check.Add(tileHabitat, false);
        }

    }

    public bool DoCheck(TileHabitat tileHabitat)
    {
        if (check.ContainsKey(tileHabitat))
        {
            if (check[tileHabitat] == false)
            {
                check[tileHabitat] = true;

                return true;
            }
        }

        return false;
    }

    public void Reset()
    {
        check.Keys.ToList<TileHabitat>().ForEach(x => check[x] = false);
    }

    private Dictionary<TileHabitat, bool> check;
    public int TargetCount { get { return check.Count; } }
}

[System.Serializable]
public class Recipe
{
    [SerializeField]
    private TileHabitat[] tileHabitats = null;
    [SerializeField]
    private EggData eggData = null;
    [SerializeField]
    private ERarity eggRarity = ERarity.RARE;
    [SerializeField]
    private bool canDuplicate = false;

    public TileHabitat[] TileHabitats { get { return tileHabitats; } }
    public EggData EggData { get { return eggData; } }
    public ERarity EggRarity { get { return eggRarity; } }
    public bool CanDuplicate { get { return canDuplicate; } }
}

[CreateAssetMenu(fileName = "TileHabitat", menuName = "Data/Tile/TileHabitat", order = 1)]
public class TileHabitat : TileResource
{
    [Header("TileHabitat")]
    [SerializeField]
    private ProductionData hatchReduceProduction = null;
    [SerializeField]
    private ProductionData rarityProduction = null;
    [SerializeField]
    private ProductionData happinessProduction = null;

    [SerializeField]
    private bool isAddOn = false;
    [SerializeField]
    private EggData startEgg = null;
    [SerializeField]
    private Recipe[] recipes = null;

    public ProductionData HatchReduceProduction { get { return hatchReduceProduction; } }
    public ProductionData RarityProduction { get { return rarityProduction; } }
    public ProductionData HappinessProduction { get { return happinessProduction; } }
    public bool IsAddOn { get { return isAddOn; } }
    public EggData StartEgg { get { return startEgg; } }
    public Recipe[] Recipes { get { return recipes; } }

    public override void OnClick(MyTile myTile)
    {
        base.OnClick(myTile);

        if (isAddOn == false)
        {
            HabitatInfoUI.Instance.Show(myTile.GetComponent<Habitat>());
        }
    }

    public override void OnPlace(MyTile myTile, bool load)
    {
        base.OnPlace(myTile, load);

        myTile.gameObject.AddComponent<Habitat>().Init(this, myTile, load);

        if (load == false)
        {
            UpdateRecipes(myTile, null);
        }
    }

    public override void OnNeighbourUpdate(MyTile myTile, MyTile neighbour)
    {
        UpdateRecipes(myTile, neighbour);
    }

    public override bool IsLocationValid(MyTile myTile)
    {
        if (base.IsLocationValid(myTile))
        {
            if (isAddOn)
            {
                return myTile.TileResource == null && HasHabitatNeighbour(myTile, false);
            }
            else
            {
                return myTile.TileResource == null;
            }
        }
        else
        {
            return false;
        }
    }

    public override void OnStartDrag()
    {
        if (isAddOn)
        {
            ShopUI.Instance.CloseCategory();
            DragHandler.Instance.InvokeStartDrag();

            DragHandler.Instance.SetDarkerTile(true, 
                DragHandler.isNotEditable,
                DragHandler.hasTileResource,
                DragHandler.isNotPossibleAddon);
        }
        else
        {
            base.OnStartDrag();
        }
    }

    public override bool OnUpdateDrag(MyTile myTile)
    {
        bool isValid = IsLocationValid(myTile);

        DragHandler.Instance.ClearFeedback();

        //List<Recipe> totalRecipes = new List<Recipe>();

        if (isValid && myTile != null && myTile)
        {
            TileHabitat tileHabitat;
            List<Recipe> recipes;

            foreach (MyTile neighbour in myTile.Neighbours)
            {
                // has tile resources
                if (neighbour.TileResource != null)
                {
                    tileHabitat = neighbour.TileResource as TileHabitat;

                    // is habitat 
                    if (tileHabitat != null)
                    {
                        // does neighbour synergise with self
                        if (tileHabitat.HasRecipeWith(neighbour, myTile, this, out recipes, true))
                        {
                            DragHandler.Instance.SpawnRecipes(recipes, neighbour);

                            //totalRecipes.AddRange(recipes);
                        }

                        // does self synergise with neighbour
                        if (this.HasRecipeWith(neighbour, myTile, tileHabitat, out recipes, true))
                        {
                            DragHandler.Instance.SpawnRecipes(recipes, myTile);

                            //totalRecipes.AddRange(recipes);
                        }
                    }
                }
            }
        }

        return isValid;
    }

    public override void OnEndDrag(MyTile myTile)
    {
        base.OnEndDrag(myTile);

        DragHandler.Instance.ClearFeedback();
    }

    private void UpdateRecipes(MyTile myTile, MyTile neighbour)
    {
        Recipe rarest = null;
        Habitat habitat = myTile.GetComponent<Habitat>();
        List<MyTile> neighbours;
        List<MyTile> finalNeighbours = new List<MyTile>();

        foreach (Recipe recipe in recipes)
        {
            if (recipe.CanDuplicate == false && habitat.HasRecipe(recipe))
                continue;

            if (HasRecipe(myTile, recipe, out neighbours))
            {
                if (neighbour != null && neighbours.Contains(neighbour) == false)
                    continue;
                if (rarest != null && rarest.TileHabitats.Length > recipe.TileHabitats.Length)
                    continue;
                if (recipe.CanDuplicate == false && habitat.HasRarerRecipe(recipe))
                    continue;

                rarest = recipe;
                finalNeighbours = neighbours;
            }
        }

        if (rarest != null)
        {
            //Debug.Log("Res: " + rarest.EggData.name);
            habitat.OnRecipe(rarest, finalNeighbours);
        }    
    }

    public bool HasRecipeWith(MyTile myTile, MyTile targetTile, TileHabitat targetTileHabitat, out List<Recipe> resultRecipe, bool onlyRarest = false)
    {
        resultRecipe = new List<Recipe>();

        List<MyTile> neighbours;
        foreach (Recipe recipe in recipes)
        {
            if (HasRecipe(myTile, recipe, out neighbours, targetTile, targetTileHabitat))
            {
                if (neighbours.Contains(targetTile))
                {
                    if (resultRecipe.Count > 0 && onlyRarest)
                    {
                        if (resultRecipe[0].TileHabitats.Length < recipe.TileHabitats.Length)
                        {
                            resultRecipe[0] = recipe;
                        }
                        continue;
                    }
                    resultRecipe.Add(recipe);
                }
            }
        }

        return resultRecipe.Count > 0;
    }

    private bool HasRecipe(MyTile myTile, Recipe recipe, out List<MyTile> neighbours, MyTile targetTile = null, TileHabitat targetTileHabitat = null)
    {
        neighbours = new List<MyTile>();

        int count = 0;
        RecipeCheck recipeCheck = new RecipeCheck(recipe);

        Habitat habitat = myTile.GetComponent<Habitat>();
        TileHabitat current;
        foreach (MyTile neighbour in myTile.Neighbours)
        {
            // for preview
            if (neighbour == targetTile)
            {
                current = targetTileHabitat;
            }
            else
            {
                current = neighbour.TileResource as TileHabitat;
            }

            if (current != null)
            {
                if (recipeCheck.DoCheck(current))
                {
                    ++count;
                    neighbours.Add(neighbour);
                }

                if (count == recipeCheck.TargetCount)
                {
                    if (habitat.HasLink(neighbours) || (recipe.CanDuplicate && habitat.HasInLink(neighbours)))
                    {
                        recipeCheck.Reset();
                        neighbours.Clear();
                        count = 0;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private bool HasHabitatNeighbour(MyTile myTile, bool includeAddons)
    {
        TileHabitat tileHabitat;
        foreach (MyTile neighbour in myTile.Neighbours)
        {
            tileHabitat = neighbour.TileResource as TileHabitat;
            if (tileHabitat != null && tileHabitat.IsAddOn == includeAddons)
            {
                return true;
            }
        }

        return false;
    }

    public int GetRecipeId(Recipe recipe)
    {
        for (int i = 0; i < recipes.Length; ++i)
        {
            if (recipe == recipes[i])
            {
                return i;
            }
        }

        return -1;
    }
}