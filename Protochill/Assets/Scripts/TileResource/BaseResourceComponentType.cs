﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseResourceComponentType<T> : BaseResourceComponent where T : TileResource
{
    protected T tileResource;

    public T TileResource { get { return tileResource; } }

    public virtual void Init(T tileResource, MyTile myTile, bool load)
    {
        this.tileResource = tileResource;

        if (string.IsNullOrEmpty(tileResource.LevelKey) == false)
        {
            levelGenerator = GeneratorHandler.Instance.GetGenerator(tileResource.LevelKey);
            levelGenerator.levelUpdateEvent += OnGeneratorLevelUpdate;

            Level = levelGenerator.Level;
        }
    }

    public override void OnRemove()
    {
        base.OnRemove();

        if (string.IsNullOrEmpty(tileResource.CountKey) == false)
        {
            GeneratorSaveData generator = SaveManager.Instance.LoadGenerator(tileResource.CountKey);
            --generator.level;
            SaveManager.Instance.SaveGenerator(tileResource.CountKey, generator);
        }
    }
}
