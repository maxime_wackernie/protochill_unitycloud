﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GLUnity.IOC;

[RequireComponent(typeof(MeshCollider))]
public class CameraCollider : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    [Dependency]
    private CameraController cameraController = null;

    private Vector3 startDrag;
    private Vector3 velocity;

    private bool isDrag;

    private void Update()
    {
        if (isDrag == false && Input.touchCount == 2)
        {
            DoubleTouchUpdate();
        }
    }

    private void DoubleTouchUpdate()
    {
        Touch touchZero = Input.GetTouch(0);
        Touch touchOne = Input.GetTouch(1);

        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
        float currTouchDeltaMag = (touchZero.position - touchOne.position).magnitude;

        float deltaMagnitudeDiff = prevTouchDeltaMag - currTouchDeltaMag;

        cameraController.AddPerspectiveZoom(deltaMagnitudeDiff);
    }

    #region Event system

   
    public void OnBeginDrag(PointerEventData data)
    {
        LevelManager.Instance.LastClicked = null;

        cameraController.EnableGraphicRaycasters(false);

        startDrag = data.pointerCurrentRaycast.worldPosition;
        isDrag = true;

        cameraController.StopMove();

        //Debug.Log("startPos : " + data.position);
    }

    public void OnDrag(PointerEventData data)
    {
        if (Input.touchCount == 2)
        {
            isDrag = false;
        }
        else
        {
            if (!isDrag)
                return;
            Vector3 currentPosition = data.pointerCurrentRaycast.worldPosition;
            velocity = startDrag - currentPosition;
            cameraController.AddTranslate(velocity);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        cameraController.EnableGraphicRaycasters(true);

        if (isDrag)
        {
            //Debug.Log("endPos : " + eventData.position);
            cameraController.DoInertia(eventData.delta);
        }

        isDrag = false;

    }
    #endregion
}
