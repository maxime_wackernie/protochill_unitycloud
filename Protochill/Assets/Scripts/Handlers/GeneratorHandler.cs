﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

public class GeneratorHandler : Singleton<GeneratorHandler>
{
    private Generator[] generators = null;

    private void Awake()
    {
        generators = GetComponentsInChildren<Generator>();
    }

    public Generator GetGenerator(string key)
    {
        foreach (Generator generator in generators)
        {
            if (generator.Key == key)
            {
                return generator;
            }    
        }

        return null;
    }
}
