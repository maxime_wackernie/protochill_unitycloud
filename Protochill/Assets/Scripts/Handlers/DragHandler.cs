﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;
using System;

public class DragHandler : Singleton<DragHandler>
{
    [SerializeField]
    private DragFeedbackUI dragFeedbackUIPrefab = null;

    [SerializeField]
    private float darkerDuration = 1f;

    private List<DragFeedbackUI> dragFeedbacks = new List<DragFeedbackUI>();

    private const float darkValue = 0.35f;
    private const float clearValue = 0.9f;

    public static int darkerPropertyId = Shader.PropertyToID("_darker");

    public System.Action startDragEvent;
    public System.Action endDragEvent;

    private Coroutine setDarkerCoroutine;

    #region GetTileConditions
    public static GetTileCondition isEditable = new GetTileCondition(IsEditable, true);
    public static GetTileCondition isNotEditable = new GetTileCondition(IsEditable, false);
    public static GetTileCondition hasTileResource = new GetTileCondition(HasTileResource, true);
    public static GetTileCondition hasNoTileResource = new GetTileCondition(HasTileResource, false);
    public static GetTileCondition isDark = new GetTileCondition(IsDark, true);
    public static GetTileCondition isNotDark = new GetTileCondition(IsDark, false);
    public static GetTileCondition isNotPossibleAddon = new GetTileCondition(IsNotPossibleAddon);
    #endregion

    public void InvokeStartDrag()
    {
        if (startDragEvent != null)
        {
            startDragEvent.Invoke();
        }
    }

    public void InvokeEndDrag()
    {
        if (endDragEvent != null)
        {
            endDragEvent.Invoke();
        }
    }

    public void SpawnRecipes(List<Recipe> recipes, MyTile tile)
    {
        DragFeedbackUI pooled = ObjectPool.Instance.GetAvailable(dragFeedbackUIPrefab) as DragFeedbackUI;
        pooled.Set(recipes, tile);

        dragFeedbacks.Add(pooled);
    }

    public void ClearFeedback()
    {
        if (dragFeedbacks.Count > 0)
        {
            foreach (DragFeedbackUI dragFeedback in dragFeedbacks)
            {
                dragFeedback.SetAvailable();
            }

            dragFeedbacks.Clear();
        }
    }

    public void SetDarkerTile(bool dark, params GetTileCondition[] conditions)
    {
        List<MyTile> tiles;
        LevelManager.Instance.GetTileCondition(out tiles, conditions);

        float start = dark ? clearValue : darkValue;
        float target = dark ? darkValue : clearValue;

        if (setDarkerCoroutine != null)
        {
            StopCoroutine(setDarkerCoroutine);
        }

        setDarkerCoroutine = StartCoroutine(SetDarkerEnumerator(tiles, darkerDuration, start, target));
    }

    private IEnumerator SetDarkerEnumerator(List<MyTile> tiles, float duration, float start, float target)
    {
        float time = 0f;
        float value;
        while (time < duration)
        {
            time += Time.deltaTime;
            value = Mathf.Lerp(start, target, time / duration);
            
            foreach (MyTile tile in tiles)
            {
                tile.DarkValue = value;
            }

            yield return null;
        }

        setDarkerCoroutine = null;
    }

    public static bool IsNotPossibleAddon(MyTile tile, params object[] parameters)
    {
        TileHabitat tileHabitat;
        foreach (MyTile neighbour in tile.Neighbours)
        {
            tileHabitat = neighbour.TileResource as TileHabitat;
            if (tileHabitat && tileHabitat.IsAddOn == false)
            {
                return false;
            }
        }

        return true;
    }

    public static bool HasTileResource(MyTile tile, params object[] parameters)
    {
        bool target = (bool)parameters[0];
        return target ? tile.TileResource != null : tile.TileResource == null;
    }

    public static bool IsNotTileType (MyTile tile, params object[] parameters)
    {
        ETileType tileType = (ETileType)parameters[0];

        return tile.tileType != tileType;
    }

    public static bool IsEditable(MyTile tile, params object[] parameters)
    {
        bool target = (bool)parameters[0];
        return tile.IsEditable == target;
    }

    public static bool IsDark(MyTile tile, params object[] parameters)
    {
        bool target = (bool)parameters[0];

        return target ? !Mathf.Approximately(tile.DarkValue, clearValue) :
            Mathf.Approximately(tile.DarkValue, clearValue);
    }
}