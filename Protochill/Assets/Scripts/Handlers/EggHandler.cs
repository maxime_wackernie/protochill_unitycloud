﻿using DG.Tweening;
using GLUnity;
using GLUnity.IOC;
using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct EggSave
{
    public EggSave(EggHatchData eggHatchData)
    {
        this.eggDataKey = eggHatchData.eggData.name;
        this.eggRarity = eggHatchData.eggRarity;
        this.isReady = eggHatchData.isReady;

        this.type = eggHatchData.habitat.MyTile.tileType;

        timeLeft = isReady ? -1f : TimerManager.Instance.GetTimerTimeLeft(eggHatchData.id);
    }

    public string eggDataKey;
    public ERarity eggRarity;
    public ETileType type;
    public bool isReady;
    public float timeLeft;
}

public class EggHatchData
{
    public EggHatchData(int id, EggData eggData, ERarity eggRarity, Habitat habitat, bool isReady)
    {
        this.id = id;
        this.eggData = eggData;
        this.eggRarity = eggRarity;
        this.habitat = habitat;
        this.isReady = isReady;
    }

    public EggHatchData(EggSave eggSave, Habitat habitat)
    {
        this.id = -1;
        this.eggData = SaveManager.Instance.GetScriptableObject<EggData>(eggSave.eggDataKey);
        this.eggRarity = eggSave.eggRarity;
        this.habitat = habitat;
        this.isReady = eggSave.isReady;
    }

    public int id;
    public EggData eggData;
    public ERarity eggRarity;
    public Habitat habitat;
    public bool isReady;
}

[System.Serializable]
public struct EggTypeData
{
    public ETileType tileType;
    public Texture texture;
}

[IOCExpose]
public class EggHandler : MonoBehaviour
{
    [Header("Egg")]
    [SerializeField]
    private Vector3 eggOffset = Vector3.zero;
    [SerializeField]
    private Vector3 birdRotation = Vector3.zero;

    [SerializeField]
    private float stayOnEggDuration = 2f;
    [SerializeField]
    private float moveToGroundDuration = 1f;
    [SerializeField]
    private float freezeDuration = 3.5f;
    [SerializeField]
    private float birdScaleMult = 1.5f;

    [Header("Camera")]
    [SerializeField]
    private Vector3 cameraOffset = Vector3.zero;
    [SerializeField]
    private float cameraDistance = 10f;

    [Header("Parameters")]
    [SerializeField]
    private RectTransform topUI = null;
    [SerializeField]
    private RectTransform bottomUI = null;

    [SerializeField]
    private EggTypeData[] eggTypeDatas = null;

    private bool isEggHatching;

    private BirdNotifUI birdNotifUI = null;
    private CollectionUI collectionUI = null;

    private ParticleSystem vfxNewBird = null;

    public UnityEvent startHatchEvent;
    public UnityEvent endHatchEvent;

    public bool IsEggHatching 
    { 
        get { return isEggHatching; }
    }

    public float CameraDistance { get { return cameraDistance; } }
    public Vector3 EggOffset { get { return eggOffset; } }
    public Vector3 CameraOffset { get { return cameraOffset; } }
    public float FreezeDuration { get { return freezeDuration; } }

    private int eggTextureId = Shader.PropertyToID("egg_texture");

    private void Awake()
    {
        birdNotifUI = FindObjectOfType<BirdNotifUI>();
        collectionUI = FindObjectOfType<CollectionUI>();
    }

    private void Start()
    {
        vfxNewBird = GameObject.Instantiate(PrefabHolder.Instance.GetPrefab<ParticleSystem>("vfxNewBrid"));
        vfxNewBird.gameObject.SetActive(false);
    }

    public void StartHatch(EggHatchData eggHatchData, AnimalData animalData)
    {
        isEggHatching = true;

        topUI.DOLocalMoveY(topUI.localPosition.y + 500f, 1f);
        bottomUI.DOLocalMoveY(bottomUI.localPosition.y - 500f, 1f);

        if (startHatchEvent != null)
        {
            startHatchEvent.Invoke();
        }

        birdNotifUI.Show(eggHatchData, animalData, AnimalManager.Instance.HasUnlocked(animalData) == false);        
    }

    public void EndHatch()
    {
        topUI.DOLocalMoveY(topUI.localPosition.y - 500f, 1f).onComplete += HatchAnimationComplete;
        bottomUI.DOLocalMoveY(bottomUI.localPosition.y + 500f, 1f);

        if (endHatchEvent != null)
        {
            endHatchEvent.Invoke();
        }
    }

    private void HatchAnimationComplete()
    {
        isEggHatching = false;
    }

    public void SpawnEgg(AnimalData animalData, ERarity resultRarity, Habitat habitat, Vector3 position)
    {
        ParticleSystem particleSystem = LevelManager.Instance.RarityData.GetRarityData(resultRarity).hatchParticleSystem;
        Destroy(GameObject.Instantiate(particleSystem, position, Quaternion.identity).gameObject, particleSystem.main.duration);

        if (AnimalManager.Instance.HasUnlocked(animalData) == false)
        {
            vfxNewBird.transform.position = position;
            vfxNewBird.gameObject.SetActive(true);
            vfxNewBird.Play(true);
            CoroutineHelper.Instance.ExecuteAfter(vfxNewBird.main.duration, () => vfxNewBird.gameObject.SetActive(false));
        }

        MMVibrationManager.Haptic(HapticTypes.Success);

        Animal instance = AnimalManager.Instance.SpawnAnimal(animalData, position);
        instance.InitFromEgg(animalData, habitat, stayOnEggDuration, moveToGroundDuration, EndHatch);
        float scale = instance.transform.localScale.x;

        if (collectionUI != null)
            collectionUI.Add(animalData);

        instance.transform.eulerAngles = birdRotation;
        instance.transform.localScale = Vector3.zero;
        Sequence sequence = DOTween.Sequence();
        sequence.Append(instance.transform.DOScale(scale * (birdScaleMult * 1.3f), 0.25f));
        sequence.Append(instance.transform.DOScale(scale * birdScaleMult, 0.1f));
        sequence.Play();
    }

    public Material GetEggMaterial(ETileType tileType, ERarity eggRarity)
    {
        Material material = new Material(LevelManager.Instance.RarityData.GetRarityData(eggRarity).material);
        foreach (EggTypeData eggTypeData in eggTypeDatas)
        {
            if (eggTypeData.tileType == tileType)
            {
                material.SetTexture(eggTextureId, eggTypeData.texture);
                return material;
            }
        }

        return null;
    }
}
