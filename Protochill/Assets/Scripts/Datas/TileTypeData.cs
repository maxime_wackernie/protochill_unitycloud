﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum ETileType
{
    FOREST,
    SAND,
    SNOW,
    NONE
}

[System.Serializable]
public struct TileMaterialData
{
    public ETileType tileType;

    public Material offMaterial;
    public Material onMaterial;
}

[CreateAssetMenu(fileName = "TileTypeData", menuName = "Data/Tile/TileTypeData", order = 1)]
public class TileTypeData : ScriptableObject
{
    [SerializeField]
    private TileMaterialData[] datas = null;

    public Material GetTileMaterial(ETileType tileType, bool on)
    {
        foreach (TileMaterialData tileMaterialData in datas)
        {
            if (tileMaterialData.tileType == tileType)
            {
                return on ? tileMaterialData.onMaterial : tileMaterialData.offMaterial;
            }
        }

        return null;
    }

#if UNITY_EDITOR
    public static TileTypeData Load()
    {
        return AssetDatabase.LoadAssetAtPath("Assets/Datas/TileTypeData.asset", typeof(TileTypeData)) as TileTypeData;
    }
#endif
}
