﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct EggDataArray
{
    [SerializeField]
    private AnimalData[] array;

    public AnimalData GetRandom()
    {
        return array[Random.Range(0, array.Length)];
    }
}

[CreateAssetMenu(fileName = "EggData", menuName = "Data/EggData", order = 1)]
public class EggData : ScriptableObject
{
    [SerializeField]
    private Sprite eggSprite = null;
    [SerializeField]
    private EggDataArray[] animalDatas = null;

    public Sprite EggSprite { get { return eggSprite; } }
    public EggDataArray[] AnimalDatas { get { return animalDatas; } }

    public AnimalData GetRandomAnimalData(ERarity rarity, out ERarity resRarity)
    {
        resRarity = ERarity.COMMON;
        float[] rarities = LevelManager.Instance.RarityData.GetRarityData(rarity).chances;

        float random = Random.Range(0f, 100f);
        float value = 0f;

        for (int i = 0; i < rarities.Length; ++i)
        {
            value += rarities[i];
            if (random < value)
            {
                if (i >= animalDatas.Length)
                {
                    Debug.LogError(name +  " GetRandomAnimalData " + rarity + " failed");
                    return null;
                }
                else
                {
                    resRarity = (ERarity)i;
                    return animalDatas[i].GetRandom();
                }
            }
        }

        return null;
    }
}
