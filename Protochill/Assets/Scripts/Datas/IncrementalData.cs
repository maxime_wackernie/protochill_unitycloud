﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "IncrementalData", menuName = "Data/IncrementalData", order = 1)]
public class IncrementalData : ScriptableObject
{
    [SerializeField]
    private float baseCost = 100;

    [SerializeField]
    private float rateGrowth = 1.2f;

    public float BaseCost { get { return baseCost; } }
    public float RateGrowth { get { return rateGrowth; } }

    public float GetCost(int level)
    {
        return baseCost * (Mathf.Pow(rateGrowth, level));
    }
}
