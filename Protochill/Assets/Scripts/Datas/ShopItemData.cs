﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShopItemData", menuName = "Data/ShopItemData", order = 1)]
public class ShopItemData : ScriptableObject
{
    [SerializeField]
    private TileResource tileResource = null;

    public TileResource TileResource { get { return tileResource; } }
}
