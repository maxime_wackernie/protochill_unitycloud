﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EProductionType
{
    DEFAULT,
    ADD,
    ARRAY,
    ARRAY_INDEX,
    ARRAY_TOTAL
}

[CreateAssetMenu(fileName = "ProductionData", menuName = "Data/ProductionData", order = 1)]
public class ProductionData : ScriptableObject
{
    [SerializeField]
    private EProductionType productionType = EProductionType.DEFAULT;

    [SerializeField]
    private float production = 10f;

    [SerializeField]
    private float addValue = 0f;

    [SerializeField]
    private float[] productionArray = null;

    public float BaseProduction { get { return production; } }

    public float GetProduction(int level)
    {
        return GetProduction(level, productionType);
    }

    public float GetProduction(int level, EProductionType productionType)
    {
        switch (productionType)
        {
            case EProductionType.DEFAULT:
                return production * level;
            case EProductionType.ADD:
                return production + (level * addValue);
            case EProductionType.ARRAY:
                return productionArray[level < productionArray.Length ? level : productionArray.Length - 1];
            case EProductionType.ARRAY_INDEX:
                {
                    float current = 0f;
                    for (int i = 0; i < productionArray.Length; ++i)
                    {
                        current += productionArray[i];
                        if (current > level)
                        {
                            return i;
                        }
                    }

                    return productionArray.Length;
                }
            case EProductionType.ARRAY_TOTAL:
                {
                    float value = 0f;
                    int count = level;// <= productionArray.Length ? level : productionArray.Length - 1;
                    for (int i = 0; i < count; ++i)
                    {
                        value += productionArray[i];
                    }

                    return value;
                }

            default: 
                return 0f;
        }
    }
}
