﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malee;
using Malee.List;

[System.Serializable]
public class ReorderableTutorialList : ReorderableArray<ITutorial>
{
}

[CreateAssetMenu(fileName = "TutorialData", menuName = "Data/TutorialData", order = 1)]
public class TutorialData : ScriptableObject
{
    [SerializeField]
    [Reorderable(paginate = true, pageSize = 0)]
    private ReorderableTutorialList tutorialSteps = null;

    public ReorderableTutorialList TutorialSteps { get { return tutorialSteps; } }

    public void AddStep(ITutorial tutorial)
    {
        if (tutorialSteps == null)
        {
            tutorialSteps = new ReorderableTutorialList();
        }

        tutorialSteps.Add(tutorial);
    }
}
