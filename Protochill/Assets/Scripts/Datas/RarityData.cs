﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ERarity
{
    COMMON,
    RARE,
    EPIC,
    LEGENDARY
}

public enum EType
{
    NONE,
    DARK,
    FAMOUS,
    MAGIC
}

[System.Serializable]
public struct RarityPercData
{
    public ERarity rarity;

    [Range(0f, 100f)]
    public float[] chances;

    public float hatchDuration;
    public ParticleSystem hatchParticleSystem;
    public Material material;
    public Color color;
}

[CreateAssetMenu(fileName = "RarityData", menuName = "Data/RarityData", order = 1)]
public class RarityData : ScriptableObject
{
    [SerializeField]
    private RarityPercData[] rarityPercDatas = null;

    public RarityPercData GetRarityData(ERarity rarity)
    {
        foreach (RarityPercData rarityPercData in rarityPercDatas)
        {
            if (rarity == rarityPercData.rarity)
            {
                return rarityPercData;
            }
        }

        Debug.LogError("Failed GetRarityData");
        return new RarityPercData();
    }
}
