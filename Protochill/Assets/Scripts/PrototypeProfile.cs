﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;
using GLUnity.Profile;
using System;

public class PrototypeProfile : Profile<PrototypeProfileData>
{
}

[System.Serializable]
public struct GeneratorSaveData
{
    public GeneratorSaveData(GeneratorSaveData toCopy)
    {
        this.key = toCopy.key;
        this.level = toCopy.level;
        this.stepLevel = toCopy.stepLevel;
    }

    public GeneratorSaveData(string key, int level, int stepLevel)
    {
        this.key = key;
        this.level = level;
        this.stepLevel = stepLevel;
    }

    public string key;
    public int level;
    public int stepLevel;
}

[System.Serializable]
public struct AnimalsUnlockedSaveData
{
    public AnimalsUnlockedSaveData(AnimalsUnlockedSaveData toCopy)
    {
        this.name = toCopy.name;
        this.count = toCopy.count;
    }

    public AnimalsUnlockedSaveData(string name, int count)
    {
        this.name = name;
        this.count = count;
    }

    public string name;
    public int count;
}

[System.Serializable]
public class PrototypeProfileData
{
    public bool hasSave;
    public float income;
    public float averageIncome;

    public DateTime lastDateTime;

    public AnimalsUnlockedSaveData[] animalsUnlockedSaveDatas;
    public GeneratorSaveData[] generatorSaveDatas;
    public IslandSave[] islandSaves;
    public AnimalSave[] animalSaves;
}