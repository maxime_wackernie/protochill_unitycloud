﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OfflineUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI incomeText = null;
    [SerializeField]
    private TextMeshProUGUI hoursText = null;

    [SerializeField]
    private TextMeshProUGUI[] offlineEggTexts = null;

    public void Init()
    {
        Init(SaveManager.Instance.MaxOfflineHours, SaveManager.offlineIncome, SaveManager.offlineReadyEggs);
    }

    public void Init(int maxOfflineHours, float offlineIncome, Dictionary<ETileType, int> offlineReadyEggs)
    {
        gameObject.SetActive(true);
        GetComponent<Animator>().Play("In");

        string incomeDisplay;
        IncomeManager.Instance.IncomeToString(offlineIncome, out incomeDisplay);
        incomeText.SetText(incomeDisplay);

        string text = hoursText.text;
        hoursText.SetText(text, maxOfflineHours);

        int i = 0;
        foreach (var it in offlineReadyEggs)
        {
            offlineEggTexts[i].transform.parent.parent.gameObject.SetActive(it.Value > 0);

            if (it.Value > 0)
            {
                offlineEggTexts[i].SetText(it.Value.ToString());
            }
            ++i;
        }
    }

    public void OnCollect()
    {
        IncomeManager.Instance.PlusIncome(SaveManager.offlineIncome);
    }
}
