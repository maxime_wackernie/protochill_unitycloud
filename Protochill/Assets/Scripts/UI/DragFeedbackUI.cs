﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;
using UnityEngine.UI;
using TMPro;

public class DragFeedbackUI : RectFollowTarget
{
    [SerializeField]
    private Transform content = null;

    private Transform endSpace;

    protected override void Awake()
    {
        base.Awake();

        endSpace = content.GetChild(content.childCount - 1);
    }

    public void Set(List<Recipe> recipes, MyTile myTile)
    {
        foreach (Recipe recipe in recipes)
        {
            Set(recipe);
        }

        Init(myTile.transform);

        endSpace.SetAsLastSibling();
    }

    private void Set(Recipe recipe)
    {
        Transform current;
        float[] chances = LevelManager.Instance.RarityData.GetRarityData(recipe.EggRarity).chances;
        for (int i = 0; i <  content.childCount; ++i)
        {
            current = content.GetChild(i);

            current.gameObject.SetActive(i < recipe.EggData.AnimalDatas.Length);

            if (i < recipe.EggData.AnimalDatas.Length)
            {
                current.GetComponentInChildren<TextMeshProUGUI>().SetText(chances[i] + "%");
            }
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(content.parent.GetComponent<RectTransform>());
    }
}
