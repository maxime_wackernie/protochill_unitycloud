﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RepairUI : MonoBehaviour
{
    [SerializeField]
    private Button buyButton = null;
    [SerializeField]
    private TextMeshProUGUI costText = null;

    private Island island;
    private RectFollowTarget rectFollowTarget;

    private float price;

    private void Awake()
    {
        rectFollowTarget = GetComponent<RectFollowTarget>();
    }

    private void Start()
    {
        IncomeManager.Instance.incomeUpdateEvent += OnIncomeUpdate;
        DragHandler.Instance.startDragEvent += Hide;
        DragHandler.Instance.endDragEvent += Show;
    }

    private void OnDestroy()
    {
        DragHandler.Instance.startDragEvent -= Hide;
        DragHandler.Instance.endDragEvent -= Show;
        IncomeManager.Instance.incomeUpdateEvent -= OnIncomeUpdate;
    }

    public void Init(Island island, Transform target, float price)
    {
        string display;
        IncomeManager.Instance.IncomeToString(price, out display);
        costText.SetText(display);
        LayoutRebuilder.ForceRebuildLayoutImmediate(costText.transform.parent.GetComponent<RectTransform>());

        rectFollowTarget.Init(target);

        this.island = island;
        this.price = price;

        OnIncomeUpdate(IncomeManager.Instance.Income);
    }

    private void OnIncomeUpdate(float value)
    {
        buyButton.interactable = price <= value;
    }

    public void OnBuy()
    {
        if (IncomeManager.Instance.CanBuy(price))
        {
            IncomeManager.Instance.MinusIncome(price);
            island.OnBuy();
        }
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }
}
