﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionItemImageUI : MonoBehaviour
{
    public Image GetImage()
    {
        return GetComponent<Image>();
    }
}
