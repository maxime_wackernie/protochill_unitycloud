﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HappinessUI : MonoBehaviour
{
    [SerializeField]
    private Transform emojiContent = null;

    [SerializeField]
    private Slider slider = null;

    [SerializeField]
    private Image emojiImage = null;

    private void Awake()
    {
        SaveManager.Instance.loadCompleteEvent += Init;
    }

    private void Start()
    {
        HappinessManager.Instance.averageHappinessUpdateEvent += OnAverageHappinessUpdate;
        HappinessManager.Instance.happinessStepUpdate += OnHappinessStepUpdate;

        HappinessMultiplierItemUI current;

        int i = 0;
        foreach (HappinessStep step in HappinessManager.Instance.HappinessSteps)
        {
            current = emojiContent.GetChild(i).GetComponent<HappinessMultiplierItemUI>();
            current.Set(step);
            ++i;
        }
    }

    private void Init()
    {
        OnAverageHappinessUpdate(0f);
        OnHappinessStepUpdate(HappinessManager.Instance.CurrentHappinessStep);
    }

    private void OnHappinessStepUpdate(HappinessStep step)
    {
        emojiImage.sprite = step.emoji;
        slider.fillRect.GetComponent<Image>().color = step.color;
    }

    private void OnAverageHappinessUpdate(float value)
    {
        slider.value = HappinessManager.Instance.HappinessRatio;
    }
}
