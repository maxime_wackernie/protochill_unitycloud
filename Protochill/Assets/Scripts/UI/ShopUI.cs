﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GLUnity;
using DG.Tweening;

[System.Serializable]
public struct ShopItemDataArray
{
    public ShopItemData[] shopItemDatas;
}

public class ShopUI : Singleton<ShopUI>
{
    [SerializeField]
    private GameObject dragContent = null;

    [SerializeField]
    private Transform buttonContent = null;

    [SerializeField]
    private Animator animator = null;

    [SerializeField]
    public ShopItemDataArray[] shopItemDatas = null;

    private PrimaryItemUI[] primaryItemUIs;

    public PrimaryItemUI[] PrimaryItemUIs
    {
        get { return primaryItemUIs; }
    }

    private int prevId = -1;
    private int currentId = -1;

    private bool lockShop;

    public bool LockShop
    {
        get { return lockShop; }
        set { lockShop = value; }
    }

    public int CurrentId
    {
        get { return currentId; }
        set
        {
            if (lockShop)
                return;

            if (currentId != -1)
            {
                buttonContent.GetChild(currentId).DOScale(1f, 0.15f);
            }

            bool wasOpen = currentId != -1;
            prevId = currentId;
            currentId = currentId == value ? -1 : value;

            if (currentId != -1)
            {
                EnableArrow(currentId, true);
                buttonContent.GetChild(currentId).DOScale(0.75f, 0.15f);
            }

            string sateString = "";
            if (currentId < 0)
            {
                sateString = "MenuOut";
            }
            else
            {
                sateString = wasOpen ? "MenuSwitch" : "MenuIn";
            }

            dragContent.SetActive(true);
            animator.Play(sateString, -1, 0f);

            if (prevId != -1)
            {
                EnableArrow(prevId, false);
            }

            if (sateString != "MenuSwitch")
            {
                SetShopData(currentId);
            }
        }
    }


    private void Awake()
    {
        primaryItemUIs = dragContent.GetComponentsInChildren<PrimaryItemUI>(true);
    }

    public void OpenPreviousCategory()
    {
        CurrentId = prevId;
    }

    public void CloseCategory()
    {
        if (CurrentId != -1)
        {
            CurrentId = -1;
        }
    }

    private void EnableArrow(int id, bool enable)
    {
        RectTransform arrow = buttonContent.GetChild(id).Find("Arrow").GetComponent<RectTransform>();
        Image arrowImage = arrow.GetComponent<Image>();
        Sequence sequence = DOTween.Sequence();
        if (enable)
        {
            sequence.Append(arrow.DOAnchorPosY(149f, 0.1f));
            sequence.Join(arrowImage.DOColor(new Color(1f, 1f, 1f, 1f), 0.1f));
            sequence.Append(arrow.DOAnchorPosY(133f, 0.05f));
            sequence.Play();
        }
        else
        {
            arrow.DOAnchorPosY(64f, 0.1f);
            sequence.Join(arrowImage.DOColor(new Color(1f, 1f, 1f, 0f), 0.1f));
        }
    }

    public void OnCategoryButton(int id)
    {
        CurrentId = id;
    }

    private void SetShopData(int id)
    {
        if (id < 0)
            return;

        int i = 0;
        foreach (PrimaryItemUI primaryItemUI in primaryItemUIs)
        {
            if (primaryItemUI.gameObject.activeSelf)
            {
                primaryItemUI.Set(shopItemDatas[id].shopItemDatas[i]);
            }

            ++i;
        }
    }

    public void OnMenuSwitch()
    {
        SetShopData(currentId);
    }
}
