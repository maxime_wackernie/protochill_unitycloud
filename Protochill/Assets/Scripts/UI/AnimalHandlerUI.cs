﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AnimalHandlerUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI animalCountText = null;

    private void Awake()
    {
        SaveManager.Instance.loadCompleteEvent += Init;
    }

    private void Start()
    {
        AnimalManager.Instance.animalCountUpdateEvent += OnAnimalCountUpdate;
    }

    private void Init()
    {
        OnAnimalCountUpdate(null);
    }

    private void OnAnimalCountUpdate(Animal value)
    {
        animalCountText.text = AnimalManager.Instance.Animals.Count.ToString();
    }
}
