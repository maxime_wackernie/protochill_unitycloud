﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GLUnity;

public class IncomeUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI incomeText = null;

    [SerializeField]
    private TextMeshProUGUI incomeAverageText = null;

    [SerializeField]
    private TextMeshProUGUI incomeMultiplierText = null;

    [SerializeField]
    private Image incomeProgress = null;

    private void Awake()
    {
        SaveManager.Instance.loadCompleteEvent += Init;
    }

    private void Start()
    {
        if (incomeText)
            IncomeManager.Instance.incomeUpdateEvent += OnIncomeUpdate;
        if (incomeAverageText)
            IncomeManager.Instance.incomeAverageUpdateEvent += OnIncomeAverageUpdate;
        if (incomeMultiplierText)
            IncomeManager.Instance.incomeMultiplierUpdateEvent += OnIncomeMultiplierUpdate;

        TimerManager.Instance.SubscribeToUpdate(IncomeManager.Instance.IncomeTimerId, OnIncomeTimerUpdate);
        OnIncomeUpdate(IncomeManager.Instance.Income);
        OnIncomeMultiplierUpdate(IncomeManager.Instance.IncomeMultiplier);
    }

    private void OnIncomeTimerUpdate(UpdateTimerData updateTimerData)
    {
        incomeProgress.fillAmount = updateTimerData.ElapsedPercentage;
    }

    private void Init()
    {
        OnIncomeUpdate(IncomeManager.Instance.Income);
        OnIncomeMultiplierUpdate(IncomeManager.Instance.IncomeMultiplier);
    }

    private void OnIncomeUpdate(float value)
    {
        string incomeDisplay;
        IncomeManager.Instance.IncomeToString(value, out incomeDisplay);
        incomeText.text = incomeDisplay;
    }

    private void OnIncomeAverageUpdate(float value)
    {
        string incomeDisplay;
        IncomeManager.Instance.IncomeToString(IncomeManager.Instance.TotalIncomeAverage, out incomeDisplay);
        incomeAverageText.text = incomeDisplay + "/s";
    }

    private void OnIncomeMultiplierUpdate(float value)
    {
        incomeMultiplierText.text = (value > 0 ? "+" : "") + value + "%";

        if (incomeAverageText)
        {
            OnIncomeAverageUpdate(0);
        }
    }
}
