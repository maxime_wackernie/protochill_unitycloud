﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GLUnity;

public class BirdNotifUI : MonoBehaviour
{
    [SerializeField]
    private Transform percContent = null;

    [SerializeField]
    private Image birdImage = null;
    [SerializeField]
    private TextMeshProUGUI newBirdText = null;
    [SerializeField]
    private TextMeshProUGUI birdNameText = null;

    [SerializeField]
    private float notifDuration = 3f;

    private Animator animator;
    private IEnumerator showEnumerator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void Show(EggHatchData eggHatchData, AnimalData animalData, bool isNewBird)
    {
        newBirdText.SetText(isNewBird ? "NEW BIRD UNLOCKED!" : "NEW BIRD!");

        gameObject.SetActive(true);

        if (showEnumerator != null)
        {
            CoroutineHelper.Instance.StopCoroutine(showEnumerator);
        }

        SetPerc(eggHatchData.eggRarity, eggHatchData.eggData);

        showEnumerator = CoroutineHelper.Instance.ExecuteAfter(notifDuration, OnComplete);

        animator.Play("BirdNotifIn");
        birdImage.sprite = animalData.AnimalSprite;
        birdNameText.text = animalData.AnimalName;

        birdNameText.color = LevelManager.Instance.RarityData.GetRarityData(animalData.rarity).color;
    }

    private void SetPerc(ERarity eggRarity, EggData eggData)
    {
        Transform current;
        float[] chances = LevelManager.Instance.RarityData.GetRarityData(eggRarity).chances;
        for (int i = 0; i < percContent.childCount; ++i)
        {
            current = percContent.GetChild(i);

            current.gameObject.SetActive(i < eggData.AnimalDatas.Length);

            if (i < eggData.AnimalDatas.Length)
            {
                current.GetComponentInChildren<TextMeshProUGUI>().SetText(chances[i] + "%");
            }
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(percContent.parent.GetComponent<RectTransform>());
    }

    private void OnComplete()
    {
        showEnumerator = null;

        animator.Play("BirdNotifOut");
    }
}
