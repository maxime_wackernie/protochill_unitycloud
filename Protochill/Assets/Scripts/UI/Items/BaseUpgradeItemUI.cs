﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MoreMountains.NiceVibrations;

public enum EUpgradeDisplaySignType
{
    MINUS,
    PLUS,
    NONE
}

public enum EUpgradeDisplayType
{
    VALUE,
    DIFF_PERC
}

public abstract class BaseUpgradeItemUI<T> : MonoBehaviour where T : TileResource
{
    [SerializeField]
    protected T tileResource = null;

    [SerializeField]
    private Image resourceImage = null;

    protected GeneratorUI generatorUI;

    private void Awake()
    {
        generatorUI = GetComponent<GeneratorUI>();
        generatorUI.upgradeEvent += OnUpgrade;
        generatorUI.upgradeStepEvent += OnUpgradeStep;
    }

    private void Start()
    {
        Init(tileResource);
    }

    private void OnEnable()
    {
        BaseRefresh();
    }

    public virtual void Init(T tileResource)
    {
        resourceImage.sprite = tileResource.ResourceSprite;

        generatorUI.Generator = GeneratorHandler.Instance.GetGenerator(tileResource.LevelKey);
        generatorUI.InitSlider(true);

        generatorUI.Generator.IncrementalData = (tileResource.UpgradeCost);

        generatorUI.RefreshCost();
        Refresh();
    }

    private void BaseRefresh()
    {
        if (generatorUI.Generator != null)
        {
            Refresh();
        }
    }

    private void OnUpgrade()
    {
        MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }

    private void OnUpgradeStep()
    {
        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
        Refresh();
    }

    public virtual void Refresh()
    {
    }

    protected void UpdateText(string displayString, TextMeshProUGUI text, ProductionData productionData, int level, 
        EUpgradeDisplayType displayType, EUpgradeDisplaySignType displaySignType)
    {
        ++level;

        float current = productionData.GetProduction(level);
        float next = productionData.GetProduction(level + 1);
        float nextDisplay = 0f;

        string prefix = "";
        switch (displaySignType)
        {
            case EUpgradeDisplaySignType.MINUS:
                current = Mathf.Abs(current) * -1f;
                break;
            case EUpgradeDisplaySignType.PLUS:
                prefix = "+";
                break;
        }

        switch (displayType)
        {
            case EUpgradeDisplayType.VALUE:
                nextDisplay = next;
                break;
            case EUpgradeDisplayType.DIFF_PERC:
                {
                    nextDisplay = ((next - current) / current) * 100f;
                    break;
                }
        }

        text.SetText(UpgradeUI.color + prefix + displayString, current, nextDisplay);
    }
}
