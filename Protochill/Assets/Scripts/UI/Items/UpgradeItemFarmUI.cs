﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpgradeItemFarmUI : BaseUpgradeItemUI<TileFood>
{
    [SerializeField]
    private TextMeshProUGUI slotText = null;
    [SerializeField]
    private TextMeshProUGUI growText = null;
    [SerializeField]
    private TextMeshProUGUI eatText = null;

    public override void Refresh()
    {
        base.Refresh();

        int level = generatorUI.Generator.Level;
        UpdateText(UpgradeUI.stringUpgradeDefault, slotText, tileResource.SlotCapacity, level, EUpgradeDisplayType.VALUE, EUpgradeDisplaySignType.NONE);
        UpdateText(UpgradeUI.stringUpgradePerc, growText, tileResource.CooldownBonus, level, EUpgradeDisplayType.VALUE, EUpgradeDisplaySignType.PLUS);
        UpdateText(UpgradeUI.stringUpgradePerc, eatText, tileResource.EatBonus, level, EUpgradeDisplayType.VALUE, EUpgradeDisplaySignType.PLUS);
    }
}
