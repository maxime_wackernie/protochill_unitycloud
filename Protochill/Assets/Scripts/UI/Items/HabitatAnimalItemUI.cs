﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class HabitatAnimalItemUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI animalCount = null;
    [SerializeField]
    private Image animalImage = null;

    private Animal animal;

    public Animal Animal { get { return animal; } }

    public void Init(Animal animal)
    {
        this.animal = animal;

        animalCount.text = "1";
        animalImage.sprite = animal.AnimalData.AnimalSprite;
    }

    public void AddCount()
    {
        int count = int.Parse(animalCount.text) + 1;
        animalCount.text = count.ToString();
    }

    public void RefreshCount()
    {
        animalCount.text = 0.ToString();
    }
}
