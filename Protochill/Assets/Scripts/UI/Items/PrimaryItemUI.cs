﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using TMPro;

public class PrimaryItemUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]
    private Image image = null;

    [SerializeField]
    private TextMeshProUGUI nameText = null;

    [SerializeField]
    private Image[] buyFeedbackImages = null;

    [SerializeField]
    private TileResource tileResource = null;

    [SerializeField]
    private LayerMask dragLayer = 0;

    private GameObject toDrag;
    private MyTile currentTile;
    private bool isValid;

    private GeneratorUI generatorUI;
    private ShopItemData shopItemData;

    private void Awake()
    {
        generatorUI = GetComponent<GeneratorUI>();
        generatorUI.buyStateUpdate += OnBuyStateUpdate;
    }

    public void Set(ShopItemData shopItemData)
    {
        generatorUI.Generator.IncrementalData = shopItemData.TileResource.BuyCost;
        generatorUI.Generator.Load(shopItemData.TileResource.CountKey);
        generatorUI.OnIncomeUpdate(IncomeManager.Instance.Income);

        tileResource = shopItemData.TileResource;
        image.sprite = shopItemData.TileResource.ResourceSprite;
        nameText.SetText(shopItemData.TileResource.ResourceName);

        this.shopItemData = shopItemData;
    }

    private void OnBuyStateUpdate(bool canBuy)
    {
        Color color = canBuy ? Color.white : Color.red;
        foreach (Image image in buyFeedbackImages)
        {
            image.color = color;
        }
    }

    #region Events
    public void OnBeginDrag(PointerEventData eventData)
    {
        isValid = false;

        if (IncomeManager.Instance.CanBuy(generatorUI.Generator.Cost))
        {
            shopItemData.TileResource.OnStartDrag();
            toDrag = GameObject.Instantiate(tileResource.Prefab);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (toDrag != null)
        {
            Vector3 position;
#if UNITY_EDITOR
            position = Input.mousePosition;
#else
        position = Input.GetTouch(0).position;
#endif
            MyTile previousTile = currentTile;

            Ray ray = Camera.main.ScreenPointToRay(position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 200f, dragLayer))
            {
                MyTile myTile = hit.collider.GetComponent<MyTile>();

                // same tile
                if (myTile != null && myTile == currentTile)
                {
                    return;
                }

                if (myTile != null)
                {
                    currentTile = myTile;
                    toDrag.transform.position = myTile.transform.position + LevelManager.Instance.Offset;
                }
                else
                {
                    currentTile = null;
                    toDrag.transform.position = hit.point;
                }
            }
            else
            {
                currentTile = null;
            }

            isValid = shopItemData.TileResource.OnUpdateDrag(currentTile);

            if (previousTile != currentTile)
            {
                if (previousTile != null)
                {
                    previousTile.EnableDecoration(true);
                }
                if (currentTile != null)
                {
                    currentTile.EnableDecoration(false);
                }
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (toDrag != null)
        {
            if (currentTile != null)
            {
                currentTile.EnableDecoration(true);
            }

            shopItemData.TileResource.OnEndDrag(currentTile);

            if (isValid)
            {
                currentTile.TileResource = tileResource;

                generatorUI.OnUpgradeButton();

                SaveManager.Instance.SaveGenerator(shopItemData.TileResource.CountKey, generatorUI.Generator);
            }

            Destroy(toDrag);

            toDrag = null;
            currentTile = null;
        }
    }
#endregion
}