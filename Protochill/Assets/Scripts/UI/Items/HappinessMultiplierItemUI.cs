﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HappinessMultiplierItemUI : MonoBehaviour
{
    [SerializeField]
    private Image emojiImage = null;

    [SerializeField]
    private TextMeshProUGUI multiplierText = null;

    public void Set(HappinessStep happinessStep)
    {
        string sign = happinessStep.perc < 0f ? "" : "+";
        emojiImage.sprite = happinessStep.emoji;
        multiplierText.text = sign + (int)happinessStep.perc + "%";
        multiplierText.color = happinessStep.color;
    }
}
