﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UpgradeItemHabitatUI : BaseUpgradeItemUI<TileHabitat>
{
    [SerializeField]
    private TextMeshProUGUI hatchText = null;
    [SerializeField]
    private TextMeshProUGUI rarityText = null;
    [SerializeField]
    private TextMeshProUGUI happinessText = null;

    public override void Refresh()
    {
        base.Refresh();

        int level = generatorUI.Generator.Level;
        UpdateText(UpgradeUI.stringUpgradePerc, hatchText, tileResource.HatchReduceProduction, level, EUpgradeDisplayType.VALUE, EUpgradeDisplaySignType.MINUS);
        UpdateText(UpgradeUI.stringUpgradePerc, rarityText, tileResource.RarityProduction, level, EUpgradeDisplayType.VALUE, EUpgradeDisplaySignType.PLUS);
        UpdateText(UpgradeUI.stringUpgradePerc, happinessText, tileResource.HappinessProduction, level, EUpgradeDisplayType.VALUE, EUpgradeDisplaySignType.PLUS);
    }
}
