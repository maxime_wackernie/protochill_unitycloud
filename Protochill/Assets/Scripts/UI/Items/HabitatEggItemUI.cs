﻿using GLUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HabitatEggItemUI : MonoBehaviour
{
    [SerializeField]
    private Image progressImage = null;
    [SerializeField]
    private Image eggImage = null;
    [SerializeField]
    private TextMeshProUGUI timerText = null;

    private EggHatchData eggHatchData;

    public void Init(EggHatchData eggHatchData)
    {
        this.eggHatchData = eggHatchData;

        eggImage.sprite = eggHatchData.eggData.EggSprite;

        TimerManager.Instance.SubscribeToUpdate(eggHatchData.id, OnEggTimerUpdate);
    }

    private void OnDestroy()
    {
        if (TimerManager.Instance.HasTimer(eggHatchData.id))
        {
            TimerManager.Instance.UnsubscribeFromUpdate(eggHatchData.id, OnEggTimerUpdate);
        }
    }

    private void OnEggTimerUpdate(UpdateTimerData data)
    {
        progressImage.fillAmount = TimerManager.Instance.GetTimerElapsedPercentage(eggHatchData.id);
        timerText.text = TimeSpan.FromSeconds(data.TimeLeft + 1f).ToString(@"hh\:mm\:ss");
    }
}