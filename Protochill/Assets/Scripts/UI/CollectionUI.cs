﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionUI : MonoBehaviour
{
    [SerializeField]
    private int birdsPerContainer = 0;

    [SerializeField]
    private GameObject forestContainer = null;
    [SerializeField]
    private GameObject desertContainer = null;
    [SerializeField]
    private GameObject snowContainer = null;
    [SerializeField]
    private GameObject notif = null;

    [SerializeField]
    public Color commonColor = Color.white;
    [SerializeField]
    public Color rareColor = Color.white;
    [SerializeField]
    public Color epicColor = Color.white;
    [SerializeField]
    public Color legendaryColor = Color.white;

    [SerializeField]
    public Sprite darkSprite = null;
    [SerializeField]
    public Sprite famousSprite = null;
    [SerializeField]
    public Sprite magicSprite = null;

    private int newBirdsCounter = 0;

    private void Start()
    {
        foreach (AnimalData a in AnimalManager.Instance.animalDatas)
        {
            GetCollectionItemUI(a).SetAnimalData(a);
        }      
    }

    public void Add(AnimalData animalData)
    {
        GetCollectionItemUI(animalData).Add();
        AnimalManager.Instance.FillAnimalsUnlocked(animalData);
    }

    public void Load(AnimalData animalData, int count)
    {
        GetCollectionItemUI(animalData).Load(animalData, count);
    }

    private CollectionItemUI GetCollectionItemUI(AnimalData animalData)
    {
        int pos = AnimalManager.Instance.GetAnimalId(animalData);
        GameObject container = forestContainer;

        if (pos < birdsPerContainer)
            container = forestContainer;
        if (pos >= birdsPerContainer && pos < birdsPerContainer * 2)
            container = desertContainer;
        if (pos >= birdsPerContainer * 2 && pos < birdsPerContainer * 3)
            container = snowContainer;

        return container.transform.GetChild((pos % birdsPerContainer)+1).GetComponent<CollectionItemUI>();
    }

    public void OnEnableCollection()
    {
        newBirdsCounter = 0;
        notif.SetActive(false);
    }

    public void NewItem()
    {
        newBirdsCounter++;
        notif.SetActive(true);
        notif.GetComponentInChildren<TMPro.TMP_Text>().text = newBirdsCounter.ToString();
    }
}
