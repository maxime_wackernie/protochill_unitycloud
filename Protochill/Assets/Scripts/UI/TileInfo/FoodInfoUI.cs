﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FoodInfoUI : BaseTileInfoUI<Farm>
{
    [Header("FoodInfoUI")]
    [SerializeField]
    private Image slotImage = null;
    [SerializeField]
    private TextMeshProUGUI slotText = null;
    [SerializeField]
    private TextMeshProUGUI cooldownText = null;
    protected override void OnCurrentUpdated(Farm previousValue, Farm currentValue)
    {
        if (previousValue != null)
        {
            previousValue.slotUpdatedEvent -= InitSlots;
        }
        if (currentValue != null)
        {
            currentValue.slotUpdatedEvent += InitSlots;
        }
    }

    public override void Show(Farm tileComponent)
    {
        base.Show(tileComponent);

        tileImage.sprite = tileComponent.TileResource.ResourceSprite;
        InitSlots();
        InitCooldown();
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Hide()
    {
        base.Hide();
    }

    public void InitSlots()
    {
        slotImage.sprite = current.TileResource.SlotSprite;
        slotText.SetText(current.AvailableSlotCount + "/" + current.SlotCount);
    }

    public void InitCooldown()
    {
        cooldownText.SetText("{0:1}s", current.RegrowDuration);
    }
}
