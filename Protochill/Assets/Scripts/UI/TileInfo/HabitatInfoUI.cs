﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HabitatInfoUI : BaseTileInfoUI<Habitat>
{
    [Header("HabitatUI")]
    [Header("Parameters")]

    [SerializeField]
    private TextMeshProUGUI incomeText = null;
    [SerializeField]
    private Transform eggItemContent = null;
    [SerializeField]
    private Transform animalItemContent = null;
    [SerializeField]
    private GameObject birdsInfoContent = null;

    [Header("Items")]
    [SerializeField]
    private HabitatEggItemUI habitatEggItemPrefab = null;
    [SerializeField]
    private HabitatAnimalItemUI animalItemPrefab = null;

    protected override void OnCurrentUpdated(Habitat previousValue, Habitat currentValue)
    {
        if (previousValue != null)
        {
            previousValue.eggHatchEvent -= Refresh;
        }
        if (currentValue != null)
        {
            currentValue.eggHatchEvent += Refresh;
        }
    }

    public override void Show(Habitat tileComponent)
    {
        base.Show(tileComponent);

        tileImage.sprite = tileComponent.TileResource.ResourceSprite;

        birdsInfoContent.SetActive(tileComponent.Animals.Count > 0);

        InitIncome(tileComponent.Animals);
        InitEggs(tileComponent.HatchingEggs);
        InitAnimals(tileComponent.Animals);
    }

    public override void Refresh()
    {
        base.Refresh();

        Habitat copy = current;
        Hide(true, false);
        RefreshAnimalsCount();
        Show(copy);
    }

    public override void Hide()
    {
        base.Hide();

        Hide(true, true);
    }

    public void Hide(bool destroyEggs, bool destroyAnimals)
    {
        if (destroyEggs)
        {
            foreach (HabitatEggItemUI item in eggItemContent.gameObject.GetComponentsInChildren<HabitatEggItemUI>())
            {
                Destroy(item.gameObject);
            }
        }

        if (destroyAnimals)
        {
            foreach (HabitatAnimalItemUI item in animalItemContent.gameObject.GetComponentsInChildren<HabitatAnimalItemUI>())
            {
                Destroy(item.gameObject);
            }
        }
    }

    private void InitIncome(List<Animal> animals)
    {
        if (incomeText == null)
            return;

        float value = 0;
        foreach (Animal animal in animals)
        {
            value += animal.AnimalData.AnimalIncome.GetProduction(1);
        }

        string display;
        IncomeManager.Instance.IncomeToString(value, out display);
        incomeText.text = display; 
    }

    private void InitEggs(List<EggHatchData> eggHatchDatas)
    {
        HabitatEggItemUI instance;
        foreach (EggHatchData eggHatchData in eggHatchDatas)
        {
            instance = GameObject.Instantiate(habitatEggItemPrefab, eggItemContent);
            instance.Init(eggHatchData);
        }
    }

    private void InitAnimals(List<Animal> animals)
    {
        HabitatAnimalItemUI instance;
        HabitatAnimalItemUI item;
        foreach (Animal animal in animals)
        {
            if (GetAnimalItem(animal.AnimalData, out item) == false)
            {
                instance = GameObject.Instantiate(animalItemPrefab, animalItemContent);
                instance.Init(animal);
            }
            else
            {
                item.AddCount();
            }
        }
    }

    private void RefreshAnimalsCount()
    {
        HabitatAnimalItemUI current;
        foreach (Transform child in animalItemContent)
        {
            current = child.GetComponent<HabitatAnimalItemUI>();
            if (current != null)
            {
                current.RefreshCount();
            }
        }
    }

    private bool GetAnimalItem(AnimalData animalData, out HabitatAnimalItemUI item)
    {
        HabitatAnimalItemUI current;
        foreach (Transform child in animalItemContent)
        {
            current = child.GetComponent<HabitatAnimalItemUI>();
            if (current.Animal.AnimalData == animalData)
            {
                item = current;
                return true;
            }
        }

        item = null;
        return false;
    }
}
