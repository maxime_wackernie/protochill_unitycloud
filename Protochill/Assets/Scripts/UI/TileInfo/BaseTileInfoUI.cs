﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using GLUnity;
using GLUnity.IOC;

[RequireComponent(typeof(RectFollowTarget))]
[RequireComponent(typeof(OutsideClickComponent))]
public abstract class BaseTileInfoUI<T> : Singleton<BaseTileInfoUI<T>> where T : BaseResourceComponent
{
    [Header("BaseTileInfoUI")]
    [SerializeField]
    private float focusDistance = 10f;
    [SerializeField]
    protected Image tileImage = null;
    [SerializeField]
    private TextMeshProUGUI levelText = null;

    protected T current;
    private RectFollowTarget rectFollowTarget;

    [Dependency]
    private CameraController cameraController;

    protected T Current
    {
        get { return current; }
        set
        {
            OnCurrentUpdated(current, value);

            current = value;
        }
    }

    protected virtual void Awake()
    {
        SetInstance(this);

        rectFollowTarget = GetComponent<RectFollowTarget>();
        GetComponent<OutsideClickComponent>().Callback.AddListener(Hide);
    }

    private void Start()
    {
        this.Inject();
        if (cameraController == null)
        {
            cameraController = Camera.main.GetComponent<CameraController>();
        }

        Hide();
    }

    protected abstract void OnCurrentUpdated(T previousValue, T currentValue);

    public virtual void Show(T tileComponent)
    {
        levelText.text = "LEVEL " + (tileComponent.Level + 1);

        gameObject.SetActive(true);

        Current = tileComponent;

        rectFollowTarget.Init(tileComponent.transform);

        cameraController.CenterCameraOnPosition(tileComponent.transform.position, focusDistance);
    }

    public virtual void Refresh()
    {

    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);

        if (current != null)
        {
            current.OnInfoMenuClose();
            Current = null;
        }
    }
}
