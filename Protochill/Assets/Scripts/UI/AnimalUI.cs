﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GLUnity;

public class AnimalUI : RectFollowTarget
{
    [Header("AnimalUI")]
    [SerializeField]
    private AnimEventListener emoteListener = null;

    [SerializeField]
    private Gradient happinessGradiant = null;

    [SerializeField]
    private Animator emoteAnimator = null;
    [SerializeField]
    private Image emoteImage = null;

    [SerializeField]
    private Image happinessImage = null;

    [SerializeField]
    private Image needProgressImage = null;

    private Animal animal;
    private Coroutine needProgressCoroutine;
    private IEnumerator emoteCoroutine;

    protected override void Awake()
    {
        base.Awake();

        emoteListener.animationEnd += OnEmoteEnd;
    }

    public void Init(Animal animal)
    {
        base.Init(animal.transform);

        this.animal = animal;

        animal.happinessUpdateEvent += OnHappinessUpdate;
    }

    private void OnEmoteEnd()
    {
        emoteCoroutine = null;

        emoteAnimator.gameObject.SetActive(false);
    }

    private void OnHappinessUpdate(float value)
    {
        float ratio = value / HappinessManager.Instance.MaxHappiness;
        happinessImage.color = happinessGradiant.Evaluate(ratio);
    }

    public override void SetAvailable()
    {
        base.SetAvailable();

        if (animal != null)
        {
            animal.happinessUpdateEvent -= OnHappinessUpdate;
            animal = null;
        }
    }

    public void DoNeedProgress(float duration, System.Action callback)
    {
        if (needProgressCoroutine == null)
        {
            needProgressCoroutine = StartCoroutine(NeedProgressEnumerator(duration, callback));
        }
    }

    public void SpawnEmote(Sprite emote, float delay)
    {
        emoteImage.sprite = emote;
        emoteAnimator.gameObject.SetActive(true);
        emoteAnimator.Play("EmoteIn");

        if (emoteCoroutine != null)
        {
            CoroutineHelper.Instance.StopCoroutine(emoteCoroutine);
        }


        emoteCoroutine = CoroutineHelper.Instance.ExecuteAfter(delay, () =>
        {
            if (emoteAnimator.gameObject.activeSelf)
            {
                emoteAnimator.Play("EmoteOut");
            }

            emoteCoroutine = null;
        });
    }

    private IEnumerator NeedProgressEnumerator(float duration, System.Action callback)
    {
        needProgressImage.transform.parent.gameObject.SetActive(true);
        float time = 0;
        while (time < duration)
        {
            time += Time.deltaTime;
            needProgressImage.fillAmount = time / duration;

            yield return null;
        }

        if (callback != null)
        {
            callback.Invoke();
        }


        needProgressImage.transform.parent.gameObject.SetActive(false);
        needProgressCoroutine = null;
    }

    public void OnNeedFailed()
    {
        if (needProgressCoroutine != null)
        {
            needProgressImage.transform.parent.gameObject.SetActive(false);

            StopCoroutine(needProgressCoroutine);
            needProgressCoroutine = null;
        }
    }
}
