﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionItemCountUI : MonoBehaviour
{
    public TMPro.TMP_Text GetText()
    {
        return GetComponent<TMPro.TMP_Text>();
    }
}
