﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GeneratorUI : MonoBehaviour
{
    [Header("Parameters")]
    [SerializeField]
    private bool upgradeOnBuy = true;
    [SerializeField]
    private bool upgradeOnStep = false;
    [SerializeField]
    private bool minusIncomeOnBuy = true;
    [SerializeField]
    private bool useStepCost = false;

    [Header("UI")]
    [SerializeField]
    private TextMeshProUGUI priceText = null;
    [SerializeField]
    private TextMeshProUGUI levelText = null;
    [SerializeField]
    private Button upgradeButton = null;
    [SerializeField]
    private Image coinImage = null;
    [SerializeField]
    private bool showDecimal = true;

    [Header("Slider")]
    [SerializeField]
    private Slider slider = null;
    [SerializeField]
    private bool displaySliderLevel = false;

    private Generator generator;

    public System.Action upgradeEvent;
    public System.Action upgradeStepEvent;

    public System.Action<bool> buyStateUpdate;

    public float Cost { get { return useStepCost ? generator.StepCost : generator.Cost; } }
    public Generator Generator 
    { 
        get { return generator; }
        set
        {
            if (generator == value)
                return;

            if (value)
            {
                value.levelUpdateEvent += LevelUpdateEvent;
                value.stepLevelUpdateEvent += LevelUpdateEvent;
            }

            if (generator != null)
            {
                generator.levelUpdateEvent -= LevelUpdateEvent;
                generator.stepLevelUpdateEvent -= LevelUpdateEvent;
            }

            generator = value;
            if (generator)
            {
                LevelUpdateEvent();
            }
        }
    }

    private void Awake()
    {
        Generator = GetComponent<Generator>();

        if (upgradeButton)
        {
            upgradeButton.onClick.AddListener(OnUpgradeButton);
        }
    }

    private void Start()
    {
        IncomeManager.Instance.incomeUpdateEvent += OnIncomeUpdate;
    }

    public void InitSlider(bool displaySliderLevel)
    {
        slider.gameObject.SetActive(true);

        this.displaySliderLevel = displaySliderLevel;

        LevelUpdateEvent();
    }

    public void OnIncomeUpdate(float value)
    {
        bool canBuy = (value >= Cost) && !generator.IsMaxLevel;
        if (upgradeButton != null)
        {
            upgradeButton.interactable = canBuy;
        }

        if (buyStateUpdate != null)
        {
            buyStateUpdate.Invoke(canBuy);
        }
    }

    private void LevelUpdateEvent()
    {
        if (generator.IsMaxLevel)
        {
            upgradeButton.interactable = false;
            levelText.SetText("MAX LVL");
            priceText.enabled = false;
            coinImage.enabled = false;
            if (slider != null)
            {
                slider.value = 1;
            }
        }
        else
        {
            string priceDisplay;
            IncomeManager.Instance.IncomeToString(Cost, 6, showDecimal ? 2 : 0, out priceDisplay);
            priceText.text = priceDisplay;

            int level;
            if (displaySliderLevel)
            {
                level = (int)generator.ProductionStep.GetProduction(generator.StepLevel, EProductionType.ARRAY_INDEX);
                
                if (levelText != null)
                    levelText.text = "LVL " + (level + 1);

                float total = generator.StepLevel - generator.ProductionStep.GetProduction(level, EProductionType.ARRAY_TOTAL);
                float target = generator.ProductionStep.GetProduction(level, EProductionType.ARRAY);

                float progress = total / target;
                slider.value = progress;
            }
            else
            {
                if (levelText != null)
                    levelText.text = "LVL " + (generator.Level + 1);
            }
        }
    }

    public void RefreshCost()
    {
        string priceDisplay;
        IncomeManager.Instance.IncomeToString(Cost, out priceDisplay);
        priceText.text = priceDisplay;

        OnIncomeUpdate(IncomeManager.Instance.Income);
    }

    public void OnUpgradeButton()
    {
        float cost = Cost;
        if (IncomeManager.Instance.CanBuy(Cost))
        {
            if (upgradeOnBuy)
            {
                if (upgradeOnStep)
                    ++generator.StepLevel;
                else
                    ++generator.Level;

                
            }

            if (minusIncomeOnBuy)
                IncomeManager.Instance.MinusIncome(cost);

            if (displaySliderLevel)
            {
                int level = (int)generator.ProductionStep.GetProduction(generator.StepLevel, EProductionType.ARRAY_INDEX);
                float total = generator.StepLevel - generator.ProductionStep.GetProduction(level, EProductionType.ARRAY_TOTAL);
                if ((int)total == 0)
                {
                    if (upgradeOnStep)
                        ++generator.Level;

                    if (upgradeStepEvent != null)
                        upgradeStepEvent.Invoke();
                }
            }
            if (upgradeEvent != null)
            {
                upgradeEvent.Invoke();
            }
        }
    }
}
