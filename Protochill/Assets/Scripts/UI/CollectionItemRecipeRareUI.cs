﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionItemRecipeRareUI : MonoBehaviour
{
    public Image image1;

    public void SetImage(Sprite sprite1)
    {
        image1.sprite = sprite1;
    }
}
