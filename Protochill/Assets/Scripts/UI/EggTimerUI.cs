﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GLUnity;

public class EggTimerUI : MonoBehaviour
{
    [SerializeField]
    private Slider slider = null;
    [SerializeField]
    private Image eggImage = null;
    [SerializeField]
    private TextMeshProUGUI timerText = null;
    [SerializeField]
    private TextMeshProUGUI notificationText = null;

    private Habitat habitat;
    private EggHatchData currentEgg;
    private RectFollowTarget rectFollowTarget;

    private EggHatchData CurrentEgg
    {
        get { return currentEgg; }
        set
        {
            if (currentEgg == value)
                return;

            if (currentEgg != null)
            {
                TimerManager.Instance.UnsubscribeFromUpdate(currentEgg.id, OnTimerUpdate);
            }

            if (value != null)
            {
                InitEgg(value);
            }

            currentEgg = value;
            gameObject.SetActive(value != null);
        }
    }

    private void Awake()
    {
        rectFollowTarget = GetComponent<RectFollowTarget>();
    }

    private void Start()
    {
        DragHandler.Instance.startDragEvent += Hide;
        DragHandler.Instance.endDragEvent += Show;
    }

    public void Init(Habitat habitat)
    {
        rectFollowTarget.Init(habitat.transform);

        this.habitat = habitat;

        habitat.addEggEvent += Refresh;
        habitat.eggHatchEvent += Refresh;

        habitat.infoMenuOpen += Hide;
        habitat.infoMenuClose += Show;

        gameObject.SetActive(false);
    }

    public void OnDestroy()
    {
        habitat.addEggEvent -= Refresh;
        habitat.eggHatchEvent -= Refresh;

        habitat.infoMenuOpen -= Hide;
        habitat.infoMenuClose -= Show;

        Hide();
    }

    private void Refresh()
    {
        notificationText.transform.parent.gameObject.SetActive(habitat.HatchingEggs.Count > 1);
        notificationText.text = (habitat.HatchingEggs.Count - 1).ToString();

        CurrentEgg = GetSoonerEgg();
    }

    private void InitEgg(EggHatchData eggHatchData)
    {
        eggImage.sprite = eggHatchData.eggData.EggSprite;

        TimerManager.Instance.SubscribeToUpdate(eggHatchData.id, OnTimerUpdate);
    }

    private void OnTimerUpdate(UpdateTimerData data)
    {
        slider.value = data.ElapsedPercentage;
        timerText.text = System.TimeSpan.FromSeconds(data.TimeLeft).ToString(@"hh\:mm\:ss");
    }

    private EggHatchData GetSoonerEgg()
    {
        float sooner = float.MaxValue;
        float time;
        EggHatchData res = null;
        if (habitat.HatchingEggs.Count > 0)
        { 
            foreach (EggHatchData egg in habitat.HatchingEggs)
            {
                time = TimerManager.Instance.GetTimerTimeLeft(egg.id);
                if (time < sooner)
                {
                    sooner = time;
                    res = egg;
                }
            }
        }
        return res;
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    private void Show()
    {
        if (habitat.HatchingEggs.Count > 0)
        {
            gameObject.SetActive(true);
        }
    }
}
