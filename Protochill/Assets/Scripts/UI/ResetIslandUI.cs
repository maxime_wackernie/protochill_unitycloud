﻿using GameAnalyticsSDK;
using GameAnalyticsSDK.Setup;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResetIslandUI : MonoBehaviour
{
    [SerializeField]
    private Button confirmationButton;
    [SerializeField]
    private Button cancelButton;
    [SerializeField]
    private TMP_Text priceText;
    [SerializeField]
    private TMP_Text birdsCoundText;

    private ResetIsland resetIsland;

    private void Start()
    {
        IncomeManager.Instance.incomeUpdateEvent += OnIncomeUpdate;
    }

    public void Init(ResetIsland resetIsland, float price, float birdsCount)
    {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);

        this.resetIsland = resetIsland;

        priceText.text = price.ToString();
        birdsCoundText.text = birdsCount.ToString();
        GetComponent<Animator>().SetTrigger("Show");
    }

    public void Unshow()
    {
        GetComponent<Animator>().SetTrigger("Unshow");
    }

    private void OnIncomeUpdate(float value)
    {
        confirmationButton.interactable = value >= resetIsland.ResetPrice;
    }

    public void OnBuy()
    {
        resetIsland.OnValidatePopUp();
    }

    public void OnCancel()
    {
        resetIsland.OnRefusePopUp();
    }
}
