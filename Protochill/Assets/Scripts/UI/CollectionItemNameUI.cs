﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionItemNameUI : MonoBehaviour
{
    public TMPro.TMP_Text GetText()
    {
        return GetComponent<TMPro.TMP_Text>();
    }
}
