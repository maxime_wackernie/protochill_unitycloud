﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CollectionItemUI : MonoBehaviour
{
    private int count = 0;

    private AnimalData animalData;
    private Image birdImage;
    private TMP_Text birdName;
    private TMP_Text birdIncome;
    private TMP_Text birdCount;
    private GameObject newBird;
    private GameObject rarity;

    public GameObject commonRecipe;
    public GameObject rareRecipe;
    public GameObject epicRecipe;
    public GameObject legendaryRecipe;

    private bool isNewBird = false;

    private void Awake()
    {
        birdImage = GetComponentInChildren<CollectionItemImageUI>().GetImage();
        birdName = GetComponentInChildren<CollectionItemNameUI>().GetText();
        birdIncome = GetComponentInChildren<CollectionItemIncomeUI>().GetText();
        birdCount = GetComponentInChildren<CollectionItemCountUI>().GetText();
        newBird = GetComponentInChildren<CollectionItemNewUI>().gameObject;


        rarity = GetComponentInChildren<CollectionItemRarityUI>().gameObject;
        commonRecipe.SetActive(false);
        rareRecipe.SetActive(false);
        epicRecipe.SetActive(false);
        legendaryRecipe.SetActive(false);
    }

    public void SetAnimalData(AnimalData animalData)
    {
        this.animalData = animalData;
    }

    public void Add()
    {       
        //if (count == 0)
        if (!AnimalManager.Instance.HasUnlocked(animalData))
            NewItem();
        count++;
    }

    public void Load(AnimalData animalData, int count)
    {
        this.animalData = animalData;
        this.count = count;
    }

    private void NewItem()
    {
        GetComponentInParent<CollectionUI>().NewItem();
        isNewBird = true;
    }

    private void OnEnable()
    {
        switch (animalData.rarity)
        {
            case (ERarity.COMMON):
                rarity.GetComponent<Image>().color = GetComponentInParent<CollectionUI>().commonColor;
                commonRecipe.SetActive(true);
                break;
            case (ERarity.RARE):
                rarity.GetComponent<Image>().color = GetComponentInParent<CollectionUI>().rareColor;
                switch (animalData.type)
                {
                    case (EType.DARK): rareRecipe.GetComponent<CollectionItemRecipeRareUI>().SetImage(GetComponentInParent<CollectionUI>().darkSprite); break;
                    case (EType.FAMOUS): rareRecipe.GetComponent<CollectionItemRecipeRareUI>().SetImage(GetComponentInParent<CollectionUI>().famousSprite); break;
                    case (EType.MAGIC): rareRecipe.GetComponent<CollectionItemRecipeRareUI>().SetImage(GetComponentInParent<CollectionUI>().magicSprite); break;
                }
                rareRecipe.SetActive(true);
                break;
            case (ERarity.EPIC):
                rarity.GetComponent<Image>().color = GetComponentInParent<CollectionUI>().epicColor;
                switch (animalData.type)
                {
                    case (EType.DARK): epicRecipe.GetComponent<CollectionItemRecipeEpicUI>().SetImages(GetComponentInParent<CollectionUI>().darkSprite, GetComponentInParent<CollectionUI>().magicSprite); break;
                    case (EType.FAMOUS): epicRecipe.GetComponent<CollectionItemRecipeEpicUI>().SetImages(GetComponentInParent<CollectionUI>().famousSprite, GetComponentInParent<CollectionUI>().darkSprite); break;
                    case (EType.MAGIC): epicRecipe.GetComponent<CollectionItemRecipeEpicUI>().SetImages(GetComponentInParent<CollectionUI>().magicSprite, GetComponentInParent<CollectionUI>().famousSprite); break;
                }
                epicRecipe.SetActive(true);
                break;
            case (ERarity.LEGENDARY):
                rarity.GetComponent<Image>().color = GetComponentInParent<CollectionUI>().legendaryColor;
                legendaryRecipe.SetActive(true);
                break;
        }

        birdIncome.text = animalData.AnimalIncome.BaseProduction.ToString();
        birdCount.text = AnimalManager.Instance.CountAnimal(animalData).ToString();
        birdImage.sprite = animalData.AnimalSprite;
        if (AnimalManager.Instance.HasUnlocked(animalData))
        {            
            birdImage.color = Color.white;
            birdName.text = animalData.AnimalName;
        }
            
        if (isNewBird)
            newBird.SetActive(true);
        else
            newBird.SetActive(false);
    }

    private void OnDisable()
    {
        isNewBird = false;
        newBird.SetActive(false);
    }
}
