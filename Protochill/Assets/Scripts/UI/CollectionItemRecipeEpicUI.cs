﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionItemRecipeEpicUI : MonoBehaviour
{
    public Image image1;
    public Image image2;
    
    public void SetImages(Sprite sprite1, Sprite sprite2)
    {
        image1.sprite = sprite1;
        image2.sprite = sprite2;
    }
}
