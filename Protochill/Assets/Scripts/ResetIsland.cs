﻿using DG.Tweening;
using GLUnity;
using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResetIsland : MonoBehaviour
{
    [SerializeField]
    private Island island;
    [SerializeField]
    private Vector3 offsetCameraReset;
    [SerializeField]
    private float distanceCameraReset = 35f;
    [SerializeField]
    private GameObject canvas;
    private Quaternion startCanvasRotation;
    [SerializeField]
    private GameObject confirmationPopUp;
    [SerializeField]
    private WarningUI warningUIPrefab;
    
    public float ResetPrice { get { return island.resetIslandPrice; } }

    private List<WarningUI> warningUIList = new List<WarningUI>();

    private void Start()
    {
        startCanvasRotation = canvas.transform.rotation;
    }
    public void OnPointerDown()
    {
        if (!island.IsUnlocked)
            return;

        canvas.transform.rotation = startCanvasRotation;
        StopAllCoroutines();

        DisplayWarningUI();
        StartCoroutine(RotateArrow(0.2f, 360f));
        confirmationPopUp.GetComponent<ResetIslandUI>().Init(this, island.resetIslandPrice, island.GetAllAnimals().Count);

        Camera.main.GetComponent<CameraController>().CenterCameraOnPosition(island.transform.position + offsetCameraReset, distanceCameraReset);

        MMVibrationManager.Haptic(HapticTypes.Warning);
    }

    private void DisplayWarningUI()
    {
        foreach (MyTile tile in island.GetOccupedTiles())
        {
            WarningUI w = ObjectPool.Instance.GetAvailable(warningUIPrefab) as WarningUI;
            w.Init(tile.transform);
            warningUIList.Add(w);
        }

        foreach (Animal animal in island.GetAllAnimals())
        {
            WarningUI w = ObjectPool.Instance.GetAvailable(warningUIPrefab) as WarningUI;
            w.Init(animal.transform);
            warningUIList.Add(w);
        }
    }

    private void RemoveWarningUI()
    {
        foreach (WarningUI w in warningUIList)
        {
            w.SetAvailable();
        }
    }

    public void OnRefusePopUp()
    {
        confirmationPopUp.GetComponent<ResetIslandUI>().Unshow();
        RemoveWarningUI();
    }

    public void OnValidatePopUp()
    {
        confirmationPopUp.GetComponent<ResetIslandUI>().Unshow();
        RemoveWarningUI();
        IncomeManager.Instance.MinusIncome(island.resetIslandPrice);
        island.ResetIsland();
        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
    }

    IEnumerator RotateArrow(float duration, float degrees, float delay = 0f)
    {
        yield return new WaitForSeconds(delay);
        float startRotation = canvas.transform.eulerAngles.y;
        float endRotation = startRotation + degrees;
        float t = 0.0f;
        while (t < duration)
        {
            t += Time.deltaTime;
            float yRotation = Mathf.Lerp(startRotation, endRotation, t / duration) % 360.0f;
            canvas.transform.eulerAngles = new Vector3(canvas.transform.eulerAngles.x, yRotation,
            canvas.transform.eulerAngles.z);
            yield return null;
        }
    }

    
}
