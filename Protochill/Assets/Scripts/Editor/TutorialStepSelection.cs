﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TutorialStepSelection : PopupWindowContent
{
    private Vector2 scrollPos;
    private IEnumerable<BaseTutorialStep> tutorialSteps;
    private System.Action<BaseTutorialStep> callback;

    public void Init(System.Action<BaseTutorialStep> callback)
    {
        this.callback = null;
        this.callback += callback;
    }

    public override void OnOpen()
    {
        base.OnOpen();

        tutorialSteps = ReflectiveEnumerator.GetEnumerableOfType<BaseTutorialStep>();
    }

    public override void OnGUI(Rect rect)
    {
        editorWindow.minSize = new Vector2(400, 600);
        editorWindow.maxSize = new Vector2(400, 1000);

        float width = editorWindow.position.width;
        float height = editorWindow.position.height;

        EditorGUILayout.BeginHorizontal();
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, true, GUILayout.Width(width), GUILayout.Height(height));

        foreach (var it in tutorialSteps)
        {
            if (GUILayout.Button(it.StepName.ToString(), GUILayout.Width(width - 10f), GUILayout.Height(25)))
            {
                if (callback != null)
                {
                    callback.Invoke(it);
                }

                editorWindow.Close();
            }
        }

        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndHorizontal();
    }
}
