﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(ProductionData)), CanEditMultipleObjects]
public class ProductionDataEditor : Editor
{
    public SerializedProperty
        production,
        productionType,
        addValue,
        productionArray;

    private void OnEnable()
    {
        production = serializedObject.FindProperty("production");
        productionType = serializedObject.FindProperty("productionType");
        addValue = serializedObject.FindProperty("addValue");
        productionArray = serializedObject.FindProperty("productionArray");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(productionType);

        EProductionType value = (EProductionType)productionType.enumValueIndex;
        if (value == EProductionType.DEFAULT || value == EProductionType.ADD)
        {
            EditorGUILayout.PropertyField(production);
        }

        switch (value)
        {
            case EProductionType.ADD:
                EditorGUILayout.PropertyField(addValue, new GUIContent("addValue"));
                break;
            case EProductionType.ARRAY:
            case EProductionType.ARRAY_INDEX:
                EditorGUILayout.PropertyField(productionArray, new GUIContent("productionArray"));
                break;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
