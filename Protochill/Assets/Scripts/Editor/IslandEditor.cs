﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(Island))]
public class IslandEditor : Editor
{
    private static float heightTreshold = 4.00f;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Island island = (Island)target;

        if (GUILayout.Button("Update editable"))
        {
            UpdateEditable(island);
        }

        if (GUILayout.Button("Reset hight"))
        {
            ResetHeight(island);
        }

        if (GUILayout.Button("Set all editable"))
        {

        }

        if (GUILayout.Button("Fix names"))
        {
            RenameTiles(island);
        }
    }

    private void SetDerty(MonoBehaviour target)
    {
        EditorUtility.SetDirty(target);
        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }

    private void UpdateEditable(Island island)
    {
        foreach (MyTile myTile in island.GetComponentsInChildren<MyTile>())
        {
            myTile.SetIsEditable(myTile.tileType != ETileType.NONE &&
                myTile.transform.localPosition.y < heightTreshold, true);
        }

        SetDerty(island);
    }

    private void ResetHeight(Island island)
    {
        foreach (MyTile myTile in island.GetComponentsInChildren<MyTile>())
        {
            Vector3 localPosition = myTile.transform.localPosition;
            localPosition.y = 0f;

            myTile.transform.localPosition = localPosition;
            myTile.TileMeshRenderer.transform.localPosition = Vector3.zero;
        }

        SetDerty(island);
    }

    private void RenameTiles(Island island)
    {
        for (int i = 0; i < island.transform.childCount; ++i)
        {
            island.transform.GetChild(i).name = "Row_" + i;
        }

        Vector2Int gridPosition = new Vector2Int();
        foreach (MyTile tile in island.GetComponentsInChildren<MyTile>())
        {
            gridPosition.x = tile.transform.GetSiblingIndex();
            gridPosition.y = tile.transform.parent.GetSiblingIndex();

            tile.name = "Tile [" + gridPosition.x + ", " + gridPosition.y + "]";
        }

        SetDerty(island);
    }
}
