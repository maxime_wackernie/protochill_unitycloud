﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System;

[CustomEditor(typeof(TutorialData))]
public class TutorialDataEditor : Editor
{
    TutorialData tutorialData;

    public override void OnInspectorGUI()
    {
        tutorialData = (TutorialData)target;

        if (GUILayout.Button("Add Step"))
        {
            ShowPopup();
        }

        DrawDefaultInspector();
    }

    private void ShowPopup()
    {
        TutorialStepSelection popup = new TutorialStepSelection();
        popup.Init(OnTypeSelected);


        PopupWindow.Show(new Rect(0, 0, 0, 0), popup);
    }

    private void OnTypeSelected(BaseTutorialStep step)
    {
        Debug.Log(step.ToString());

        ITutorial newStep = (ITutorial)Activator.CreateInstance(step.GetType());
        tutorialData.AddStep(newStep);
    }
}