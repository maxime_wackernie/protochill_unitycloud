﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;
using DG.Tweening;

[System.Serializable]
public struct TutorialHandTarget
{
    public bool isWorldSpace;
    public TileInfo targetTile;
    public string targetTag;
    public Vector3 offset;

    public Transform GetTarget()
    {
        if (string.IsNullOrEmpty(targetTag) == false)
        {
            GameObject target = GameObject.FindGameObjectWithTag(targetTag);
            if (target != null)
            {
                return target.transform;
            }
            else
            {
                Debug.LogError("Tag not found: " + targetTag);
                return null;
            }
        }
        else
        {
            return LevelManager.Instance.GetTile(targetTile).transform;
        }
    }

    public Vector2 GetPosition(RectTransform rectTransform)
    {
        Transform target = GetTarget();
        if (isWorldSpace)
        {
            return TransformUtils.WorldPosToCanvasPos(target, rectTransform, offset);
        }
        else
        {
            return target.position + offset;
        }
    }
}

public class TutorialHand : Singleton<TutorialHand>
{
    [SerializeField]
    private Vector3 offset;

    private RectTransform rectTransform;
    private Transform target;
    private bool isWorldSpace;

    private Tween myTween;

    private void Awake()
    {
        SetInstance(this);

        rectTransform = GetComponent<RectTransform>();
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        ResetValues();
    }

    private void Update()
    {
        if (target != null)
        {
            SetPosition(target, offset, isWorldSpace);
        }
    }

    private void ResetValues()
    {
        target = null;

        if (myTween != null)
        {
            myTween.Kill();
            myTween = null;
        }
    }

    public void SetTarget(TutorialHandTarget to)
    {
        ResetValues();

        SetPosition(to);

        gameObject.SetActive(true);

        this.target = to.GetTarget();
        this.offset = to.offset;
        this.isWorldSpace = to.isWorldSpace;
    }

    public void DoAnimation(TutorialHandTarget from, TutorialHandTarget to, float duration, bool loop, TweenCallback callback = null)
    {
        ResetValues();

        SetPosition(from);

        gameObject.SetActive(true);

        if (to.isWorldSpace)
        {
            myTween = rectTransform.DOAnchorPos(to.GetPosition(rectTransform), duration);
        }
        else
        {
            myTween = rectTransform.DOMove(to.GetPosition(rectTransform), duration);
        }

        myTween.SetLoops(loop ? -1 : 0);

        if (callback != null)
        {
            myTween.onComplete += callback;
        }
    }

    private void SetPosition(TutorialHandTarget target)
    {
        SetPosition(target.GetTarget(), target.offset, target.isWorldSpace);
    }

    private void SetPosition(Transform target, Vector3 offset, bool isWorldSpace)
    {
        if (isWorldSpace)
        {
            rectTransform.anchoredPosition = TransformUtils.WorldPosToCanvasPos(target, rectTransform, offset);
        }
        else
        {
            transform.position = target.position + offset;
        }
    }
}
