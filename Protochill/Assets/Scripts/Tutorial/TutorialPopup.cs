﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;
using TMPro;
using DG.Tweening;

public class TutorialPopup : Singleton<TutorialPopup>
{
    [SerializeField]
    private TextMeshProUGUI popupText = null;

    private Sequence showSequence;
    private Sequence hideSequence;

    private void Awake()
    {
        InitShowSequence();
        InitHideSequence();

        SetInstance(this);

        gameObject.SetActive(false);
    }

    private void InitShowSequence()
    {
        showSequence = DOTween.Sequence();
        showSequence.SetAutoKill(false);
        showSequence.Pause();
        showSequence.OnPlay(() =>
        {
            transform.localScale = Vector3.zero;
            gameObject.SetActive(true);
        });
        showSequence.Append(transform.DOScale(1.1f, 0.2f));
        showSequence.Append(transform.DOScale(1f, 0.1f));
    }

    private void InitHideSequence()
    {
        hideSequence = DOTween.Sequence();
        hideSequence.SetAutoKill(false);
        hideSequence.Pause();
        hideSequence.Append(transform.DOScale(1.1f, 0.1f));
        hideSequence.Append(transform.DOScale(0f, 0.2f));
        hideSequence.OnComplete(() => gameObject.SetActive(false));
    }

    public void SetText(string text)
    {
        if (hideSequence.IsPlaying())
            hideSequence.Complete();

        if (gameObject.activeSelf == false)
        {
            showSequence.Restart();
        }

        popupText.SetText(text);
    }

    public void Hide()
    {
        if (showSequence.IsPlaying())
            showSequence.Complete();

        if (gameObject.activeSelf)
        {
            hideSequence.Restart();
        }
    }
}