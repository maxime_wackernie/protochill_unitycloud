﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TutorialHandler : MonoBehaviour
{
    [SerializeField]
    private TutorialData tutorial = null;
    [SerializeField]
    private bool logTutorial = true;

    private int currentStepId;
    private TutorialData currentTutorial;
    private ITutorial currentStep = null;


    public UnityEvent onTutorialStart;
    public UnityEvent onTutorialComplete;

    private void Update()
    {
        if (currentStep != null)
        {
            currentStep.UpdateStep();
        }
    }

    public void StartTutorial()
    {
        StartTutorial(tutorial);
    }
        
    public void StartTutorial(TutorialData tutorialData)
    {
        if (logTutorial)
            print("StartTutorial: " + tutorialData.name);

        if (onTutorialStart != null)
        {
            onTutorialStart.Invoke();
        }

        currentStepId = 0;

        if (tutorialData.TutorialSteps.Count > 0)
        {
            currentTutorial = tutorialData;

            StartStep(tutorialData.TutorialSteps[0]);
        }
    }

    private void TutorialComplete()
    {
        if (logTutorial)
            print("TutorialComplete: " + currentTutorial.name);

        if (onTutorialComplete != null)
        {
            onTutorialComplete.Invoke();
        }

        currentStep = null;
        currentTutorial = null;
    }

    private void StartStep(ITutorial step)
    {
        if (logTutorial)
            print("StartStep " + currentStepId + ": " + (string.IsNullOrEmpty(step.Description) == false ? step.Description : step.StepName));

        currentStep = step;

        step.StartStep(StepComplete);
    }

    private void StepComplete()
    {
        if (logTutorial)
            print("------> StepComplete: " + (string.IsNullOrEmpty(currentStep.Description) == false ? currentStep.Description : currentStep.StepName));

        if (currentStepId + 1 < currentTutorial.TutorialSteps.Count)
        {
            ++currentStepId;
            StartStep(currentTutorial.TutorialSteps[currentStepId]);
        }
        else
        {
            TutorialComplete();
        }
    }
}
