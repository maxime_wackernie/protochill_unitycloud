﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITutorial
{
    string Description
    {
        get;
    }

    string StepName
    {
        get;
    }

    void StartStep(System.Action completeCallback);

    void UpdateStep();

    void OnStepComplete();
}
