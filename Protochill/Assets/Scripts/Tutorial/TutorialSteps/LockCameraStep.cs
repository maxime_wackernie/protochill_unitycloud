﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LockCameraStep : BaseTutorialStep
{
    [SerializeField]
    private bool lockCamera = true;

    public override string StepName => "Lock / Unlock camera";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        CameraController cameraController = Camera.main.GetComponent<CameraController>();
        if (lockCamera)
        {
            cameraController.FreezeCameraInput();
        }
        else
        {
            cameraController.UnfreezeCameraInput();
        }

        OnStepComplete();
    }
}