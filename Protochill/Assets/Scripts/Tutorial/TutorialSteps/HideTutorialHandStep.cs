﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HideTutorialHandStep : BaseTutorialStep
{
    public override string StepName => "Hide hand";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        TutorialHand.Instance.gameObject.SetActive(false);

        OnStepComplete();
    }
}