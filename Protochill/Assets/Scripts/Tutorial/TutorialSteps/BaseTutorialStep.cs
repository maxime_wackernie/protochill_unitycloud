﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class BaseTutorialStep : ITutorial
{
    [SerializeField]
    private string description = string.Empty;

    public string Description { get { return description; } }

    public abstract string StepName
    {
        get;
    }

    private System.Action completeCallback;

    public virtual void StartStep(System.Action completeCallback)
    {
        this.completeCallback += completeCallback;
    }

    public virtual void UpdateStep()
    {

    }

    public virtual void OnStepComplete()
    {
        if (completeCallback != null)
        {
            completeCallback.Invoke();
        }

        completeCallback = null;
    }
}
