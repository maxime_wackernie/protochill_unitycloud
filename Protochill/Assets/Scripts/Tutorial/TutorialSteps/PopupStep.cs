﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PopupStep : BaseTutorialStep
{
    [SerializeField]
    private string text = string.Empty;

    public override string StepName => "Popup";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        TutorialPopup.Instance.SetText(text);

        OnStepComplete();
    }
}
