﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitClickStep : BaseTutorialStep
{
    public override string StepName => "Wait for click";

    public override void UpdateStep()
    {
        base.UpdateStep();

        if (Input.GetMouseButtonDown(0))
        {
            OnStepComplete();
        }
    }
}
