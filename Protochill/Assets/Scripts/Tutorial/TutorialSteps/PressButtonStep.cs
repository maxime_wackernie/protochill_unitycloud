﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PressButtonStep : BaseTutorialStep
{
    [SerializeField]
    private string targetTag = string.Empty;

    public override string StepName => "Press button";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        Button button = GameObject.FindGameObjectWithTag(targetTag).GetComponent<Button>();
        button.onClick.AddListener(OnStepComplete);
    }

    public override void OnStepComplete()
    {
        base.OnStepComplete();

        Button button = GameObject.FindGameObjectWithTag(targetTag).GetComponent<Button>();
        button.onClick.RemoveListener(OnStepComplete);
    }
}
