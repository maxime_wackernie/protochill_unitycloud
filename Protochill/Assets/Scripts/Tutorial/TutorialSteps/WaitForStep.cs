﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

public class WaitForStep : BaseTutorialStep
{
    [SerializeField]
    private float duration = 0f;

    public override string StepName => "Wair for";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        CoroutineHelper.Instance.ExecuteAfter(duration, OnStepComplete);
    }
}
