﻿using System;
using System.Collections;
using System.Collections.Generic;
using TypeReferences;
using UnityEngine;

[System.Serializable]
public class WaitForPlaceResourceTypeStep : BaseTutorialStep
{
    [SerializeField]
    [ClassExtends(typeof(TileResource))]
    private ClassTypeReference targetType;

    public override string StepName => "Place resource type";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        LevelManager.Instance.resourcePlaceEvent += OnResourcePlaced;
    }

    private void OnResourcePlaced(MyTile tile, TileResource resource)
    {
        if (resource.GetType() == targetType.Type)
        {
            OnStepComplete();
        }
    }

    public override void OnStepComplete()
    {
        base.OnStepComplete();

        LevelManager.Instance.resourcePlaceEvent -= OnResourcePlaced;
    }
}
