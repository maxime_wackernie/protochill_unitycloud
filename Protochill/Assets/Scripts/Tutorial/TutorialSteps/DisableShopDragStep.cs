﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableShopDragStep : BaseTutorialStep
{
    [SerializeField]
    private bool activate = false;

    [SerializeField]
    private int[] shopDragIds = null;

    public override string StepName => "Activate / Deactivate shop drag";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        foreach (int id in shopDragIds)
        {
            ShopUI.Instance.PrimaryItemUIs[id].gameObject.SetActive(activate);
        }

        OnStepComplete();
    }
}