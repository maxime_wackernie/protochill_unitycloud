﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockShopStep : BaseTutorialStep
{
    [SerializeField]
    private bool lockShop = true;

    public override string StepName => "Lock / Unlock shop";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        ShopUI.Instance.LockShop = lockShop;

        OnStepComplete();
    }
}
