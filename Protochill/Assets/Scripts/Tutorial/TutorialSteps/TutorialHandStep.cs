﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TutorialHandStep : BaseTutorialStep
{
    [SerializeField]
    private TutorialHandTarget target = new TutorialHandTarget();

    public override string StepName => "Hand set position";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        TutorialHand.Instance.SetTarget(target);

        OnStepComplete();
    }
}
