﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LockTileStep : BaseTutorialStep
{
    [SerializeField]
    private bool lockTile = true;

    [SerializeField]
    private TileInfo[] targetTiles = null;

    public override string StepName => "Lock / Unlock tile";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        foreach (TileInfo targetTile in targetTiles)
        {
            LevelManager.Instance.GetTile(targetTile).SetIsEditable(!lockTile, false);
        }

        OnStepComplete();
    }
}
