﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TutorialHandMoveStep : BaseTutorialStep
{
    [SerializeField]
    private TutorialHandTarget from = new TutorialHandTarget();

    [SerializeField]
    private TutorialHandTarget to = new TutorialHandTarget();

    [SerializeField]
    private float duration = 2f;

    [SerializeField]
    private bool loop = true;

    public override string StepName => "Hand move animation";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        TutorialHand.Instance.DoAnimation(from, to, duration, loop);

        OnStepComplete();
    }
}
