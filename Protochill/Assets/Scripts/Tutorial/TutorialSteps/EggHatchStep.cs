﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EHatchProgress
{
    START = 0,
    END
}

[System.Serializable]
public class EggHatchStep : BaseTutorialStep
{
    [SerializeField]
    private EHatchProgress hatchProgress = EHatchProgress.START;

    public override string StepName => "Hatching";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        EggHandler eggHandler = GameObject.FindObjectOfType<EggHandler>();
        switch (hatchProgress)
        {
            case EHatchProgress.START:
                eggHandler.startHatchEvent.AddListener(OnStepComplete);
                break;
            case EHatchProgress.END:
                eggHandler.endHatchEvent.AddListener(OnStepComplete);
                break;
        }
    }

    public override void OnStepComplete()
    {
        base.OnStepComplete();

        EggHandler eggHandler = GameObject.FindObjectOfType<EggHandler>();
        switch (hatchProgress)
        {
            case EHatchProgress.START:
                eggHandler.startHatchEvent.RemoveListener(OnStepComplete);
                break;
            case EHatchProgress.END:
                eggHandler.endHatchEvent.RemoveListener(OnStepComplete);
                break;
        }
    }
}
