﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct TileInfo
{
    public int islandId;
    public Vector2Int gridPosition;
}

[System.Serializable]
public class MoveCameraStep : BaseTutorialStep
{
    [SerializeField]
    private float distance = 10f;

    [SerializeField]
    private TileInfo targetTile = new TileInfo();

    [SerializeField]
    private string targetTag = string.Empty;

    [SerializeField]
    private Vector3 offset = Vector3.zero;

    public override string StepName => "Move camera";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        Transform target;
        if (string.IsNullOrEmpty(targetTag) == false)
        {
            target = GameObject.FindGameObjectWithTag(targetTag).transform;
        }
        else
        {
            target = LevelManager.Instance.GetTile(targetTile).transform;
        }

        CameraController cameraController = Camera.main.GetComponent<CameraController>();
        cameraController.CenterCameraOnPosition(target.position + offset, distance);
        cameraController.OnEndCenterCameraOnPosition += OnStepComplete;
    }

    public override void OnStepComplete()
    {
        base.OnStepComplete();

        CameraController cameraController = Camera.main.GetComponent<CameraController>();
        cameraController.OnEndCenterCameraOnPosition -= OnStepComplete;
    }
}