﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class HidePopupStep : BaseTutorialStep
{
    public override string StepName => "Hide popup";


    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        TutorialPopup.Instance.Hide();

        OnStepComplete();
    }
}