﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EShopType
{
    CLOSE = -1,
    HABITAT,
    ADD_ON,
    FOOD,
}

public class SetShopStep : BaseTutorialStep
{
    [SerializeField]
    private EShopType shopType = EShopType.CLOSE;

    public override string StepName => "Set Shop";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        ShopUI.Instance.CurrentId = (int)shopType;

        OnStepComplete();
    }
}
