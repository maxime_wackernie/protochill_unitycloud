﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class LockButtonStep : BaseTutorialStep
{
    [SerializeField]
    private bool multipleTags = false;

    [SerializeField]
    private bool interactable = true;

    [SerializeField]
    string[] buttons = null;

    public override string StepName => "Lock / Unlock button";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        foreach (string buttonTag in buttons)
        {
            if (multipleTags)
            {
                foreach (GameObject go in GameObject.FindGameObjectsWithTag(buttonTag))
                {
                    go.GetComponent<Button>().interactable = interactable;
                }
            }
            else
            {
                GameObject.FindGameObjectWithTag(buttonTag).GetComponent<Button>().interactable = interactable;
            }
        }

        OnStepComplete();
    }
}
