﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockEggStep : BaseTutorialStep
{
    [SerializeField]
    private bool lockEggs = true;

    public override string StepName => "Lock / Unlock eggs";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        LevelManager.enableEggHatch = !lockEggs;

        OnStepComplete();
    }
}
