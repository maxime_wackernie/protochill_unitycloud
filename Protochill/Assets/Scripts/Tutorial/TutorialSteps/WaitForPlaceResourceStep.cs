﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaitForPlaceResourceStep : BaseTutorialStep
{
    [SerializeField]
    private TileResource targetResource = null;

    public override string StepName => "Place resource";

    public override void StartStep(Action completeCallback)
    {
        base.StartStep(completeCallback);

        LevelManager.Instance.resourcePlaceEvent += OnResourcePlaced;
    }

    private void OnResourcePlaced(MyTile tile, TileResource resource)
    {
        if (targetResource == resource)
        {
            OnStepComplete();
        }
    }

    public override void OnStepComplete()
    {
        base.OnStepComplete();

        LevelManager.Instance.resourcePlaceEvent -= OnResourcePlaced;
    }
}
