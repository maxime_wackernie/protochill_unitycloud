﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TileMap<Search, Item> 
    where Search : BaseSearch<Item>, new()
    where Item : IPathFindable, new()
{
    #region Private members
    private int y;

    private PathFinder<Search, Item> pathFinder;
    private Dictionary<Vector2Int, Item> tileMapCollection = new Dictionary<Vector2Int, Item>();
    #endregion

    #region Properties
    public int TileCount { get { return tileMapCollection.Count; } }
    public int Y { get { return y; } }
    #endregion

    #region Constructors
    public TileMap(bool enablePathFinding)
    {
        pathFinder = enablePathFinding ? new PathFinder<Search, Item>() : null;
    }
    #endregion

    #region Public methods
    public void LoadTileMapData(Transform grid)
    {
        y = grid.childCount;

        if (tileMapCollection.Count > 0)
        {
            tileMapCollection.Clear();
        }

        for (int ity = 0; ity < y; ++ity)
        {
            for (int itx = 0; itx < grid.GetChild(ity).childCount; ++itx)
            {
                AddItem(new Vector2Int(itx, ity), 1, ETileMapGenerationType.SCENE, grid);
            }
        }

        /*for (int itx = 0; itx < grid.GetChild(itx).childCount; ++itx)
        {
            for (int ity = 0; ity < y; ++ity)
            {
                AddItem(new Vector2Int(itx, ity), 1, ETileMapGenerationType.SCENE, grid);
            }
        }*/
    }

    public void LoadTileMapData(TileMapData tileMapData, Transform grid)
    {
        y = tileMapData.Y;

        if (tileMapCollection.Count > 0)
        {
            tileMapCollection.Clear();
        }

        Transform parent = tileMapData.GenerationType == ETileMapGenerationType.SCENE ? grid : new GameObject("Grid").transform;
        foreach (Vector2Int gridPosition in tileMapData.GridPositions)
        {
            AddItem(gridPosition, tileMapData.TileSize, tileMapData.GenerationType, parent.transform);
        }
    }

    public void LoadNeighbours()
    {
        Item current;
        Vector2Int[] neighbours;
        foreach (Vector2Int position in tileMapCollection.Keys)
        {
            current = GetTile(position);
            if (current == null)
            {
                Debug.Log(position);
                continue;
            }

            if (TryGetNeighbours(position, out neighbours))
            {
                foreach (Vector2Int neighbour in neighbours)
                {
                    if (current.Neighbours == null)
                    {
                        current.Neighbours = new List<IPathFindable>();
                    }

                    current.Neighbours.Add(GetTile(neighbour));
                }
            }
        }
    }

    #region Neighbours
    private bool TryGetNeighbours(Vector2Int position, out Vector2Int[] neighbours)
    {
        List<Vector2Int> res = new List<Vector2Int>();
        foreach (Vector2Int neighbour in GetNeighbourPositions(position, false))
        {
            if (tileMapCollection.ContainsKey(neighbour))
            {
                res.Add(neighbour);
            }
        }

        neighbours = res.ToArray();

        return res.Count > 0;
    }

    private static Vector2Int[] GetNeighbourPositions(Vector2Int p, bool includeDiagonal)
    {
        Vector2Int[] res = new Vector2Int[includeDiagonal ? 8 : 4];
        res[0] = new Vector2Int(p.x + 1, p.y); // right
        res[1] = new Vector2Int(p.x - 1, p.y); // left

        res[2] = new Vector2Int(p.x, p.y + 1); // up
        res[3] = new Vector2Int(p.x, p.y - 1); // down

        if (includeDiagonal)
        {
            res[4] = new Vector2Int(p.x + 1, p.y + 1); // up - right
            res[5] = new Vector2Int(p.x - 1, p.y + 1); // up - left

            res[6] = new Vector2Int(p.x + 1, p.y - 1); // down - right
            res[7] = new Vector2Int(p.x - 1, p.y - 1); // down - left
        }

        return res;
    }
    #endregion

    public Item[] GetTiles()
    {
        return tileMapCollection.Values.ToArray();
    }

    public Item GetTile(Vector2Int gridPosition)
    {
        return tileMapCollection[gridPosition];
    }

    public bool TryGetTile(Vector2Int gridPosition, out Item item)
    {
        return tileMapCollection.TryGetValue(gridPosition, out item);
    }

    public SearchResult<Item> DoSearch(Vector2Int startPosition, Vector2Int goalPosition)
    {
        Item start, goal;
        if (TryGetTile(startPosition, out start) && TryGetTile(goalPosition, out goal))
        {
            return DoSearch(start, goal);
        }
        else
        {
            SearchResult<Item> res =  new SearchResult<Item>();
            res.succes = false;

            return res;
        }
    }

    public SearchResult<Item> DoSearch(Item start, Item goal)
    {
        return pathFinder.Searcher.DoSearch(start, goal);
    }
    #endregion

    #region Private methods
    private void AddItem(Vector2Int gridPosition, float tileSize, ETileMapGenerationType generationType, Transform parent)
    {
        Item item;
        Vector3 position = new Vector3(gridPosition.x * tileSize, 0f, gridPosition.y * tileSize);

        switch (generationType)
        {
            case ETileMapGenerationType.DEFAULT:
                item = new Item();
                break;
            case ETileMapGenerationType.PREFAB:
                item = new Item();//GameObject.Instantiate(tileMapData.TilePrefab.GetComponent<IPathFindable>().GetObjectToInstantiate(), position, Quaternion.identity) as g .GetComponent<Item>();
                break;
            case ETileMapGenerationType.SCENE:
                item = parent.GetChild(gridPosition.y).GetChild(gridPosition.x).GetComponent<Item>();
                break;
            default: return;
        }

        item.GridPosition = gridPosition;
        item.Position = position;
        tileMapCollection.Add(gridPosition, item);
    }
    #endregion
}
