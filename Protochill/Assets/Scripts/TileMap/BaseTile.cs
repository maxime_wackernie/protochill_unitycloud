﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; 

public abstract class BaseTile : MonoBehaviour, IPathFindable
{
    #region IPathFindable
    protected Vector3 position;
    protected Vector2Int gridPosition;
    protected List<IPathFindable> neighbours;

    public Vector3 Position
    {
        get { return position; }
        set { position = value; }
    }

    public Vector2Int GridPosition
    {
        get { return gridPosition; }
        set { gridPosition = value; }
    }

    public List<IPathFindable> Neighbours
    {
        get { return neighbours; }
        set { neighbours = value; }
    }

    public abstract bool IsAvailable();

    public abstract float GetCost(IPathFindable other);

    public abstract UnityEngine.Object GetObjectToInstantiate();
    #endregion
}
