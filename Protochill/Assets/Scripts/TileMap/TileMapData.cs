﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMapData : ScriptableObject
{
    [SerializeField]
    private int x;
    [SerializeField]
    private int y;
    [SerializeField]
    private float tileSize;

    [SerializeField]
    private ETileMapGenerationType generationType;

    [SerializeField]
    private GameObject tilePrefab;

    [SerializeField]
    private Vector2Int[] gridPositions;

    public int X 
    { 
        get { return x; } 
        set { x = value; } 
    }

    public int Y 
    { 
        get { return y; }
        set { y = value; }
    }

    public float TileSize
    {
        get { return tileSize; }
        set { tileSize = value; }
    }
    public ETileMapGenerationType GenerationType
    {
        get { return generationType; }
        set { generationType = value; }
    }

    public GameObject TilePrefab
    {
        get { return tilePrefab; }
        set { tilePrefab = value; }
    }

    public Vector2Int[] GridPositions 
    {
        get { return gridPositions; } 
        set { gridPositions = value; }
    }
}
