﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseTileMapComponent<Search, Item> : MonoBehaviour
    where Search : BaseSearch<Item>, new()
    where Item : IPathFindable, new()
{
    [SerializeField]
    private bool enablePathFinding = true;
    [SerializeField]
    private bool loadNeighbours = true;

    [SerializeField]
    private TileMapData tileMapData = null;

    private TileMap<Search, Item> tileMap;

    public TileMap<Search, Item> TileMap { get { return tileMap; } }

    private void Awake()
    {
        tileMap = new TileMap<Search, Item>(enablePathFinding);

        if (tileMapData != null)
        {
            tileMap.LoadTileMapData(tileMapData, transform);
        }
        else
        {
            tileMap.LoadTileMapData(transform);
        }

        if (loadNeighbours)
        {
            tileMap.LoadNeighbours();
        }
    }
    public Item GetRandomTile()
    {
        int randY = Random.Range(0, tileMap.Y);
        int randX = Random.Range(0, transform.GetChild(randY).childCount);
        return tileMap.GetTile(new Vector2Int(randX, randY));
    }
}
