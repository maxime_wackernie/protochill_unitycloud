﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder<Search, Item> 
    where Item : IPathFindable, new()
    where Search : BaseSearch<Item>, new()
{
    private Search searcher;

    public Search Searcher { get { return searcher; } }

    public PathFinder()
    {
        searcher = new Search();
    }

    /*private void InitGridNeighbours(TileMap<Search, Item> tileMap)
    {
        List<Vector2Int> listGrid = new List<Vector2Int>(tileMap.GridPositions);

        IPathFindable current;

        Vector2Int[] neighbours;
        foreach (Vector2Int position in tileMap.GridPositions)
        {
            current = tileMap.GetTile(position);
            if (current == null)
            {
                Debug.Log(position);
                continue;
            }

            if (TryGetNeighbours(position, out neighbours, listGrid, false))
            {
                foreach (Vector2Int neighbour in neighbours)
                {
                    current.Neighbours.Add(tileMap.GetTile(neighbour));
                }
            }
        }
    }*/

    public bool TryGetNeighbours(Vector2Int position, out Vector2Int[] neighbours, List<Vector2Int> grid, bool includeDiagonal)
    {
        List<Vector2Int> res = new List<Vector2Int>();
        foreach (Vector2Int neighbour in GetNeighbourPositions(position, includeDiagonal))
        {
            if (grid.Contains(neighbour))
            {
                res.Add(neighbour);
            }
        }

        neighbours = res.ToArray();

        return res.Count > 0;
    }

    // TMP
    private static Vector2Int[] GetNeighbourPositions(Vector2Int p, bool includeDiagonal)
    {
        Vector2Int[] res = new Vector2Int[includeDiagonal ? 8 : 4];
        res[0] = new Vector2Int(p.x + 1, p.y); // right
        res[1] = new Vector2Int(p.x - 1, p.y); // left

        res[2] = new Vector2Int(p.x, p.y + 1); // up
        res[3] = new Vector2Int(p.x, p.y - 1); // down

        if (includeDiagonal)
        {
            res[4] = new Vector2Int(p.x + 1, p.y + 1); // up - right
            res[5] = new Vector2Int(p.x - 1, p.y + 1); // up - left

            res[6] = new Vector2Int(p.x + 1, p.y - 1); // down - right
            res[7] = new Vector2Int(p.x - 1, p.y - 1); // down - left
        }

        return res;
    }
}
