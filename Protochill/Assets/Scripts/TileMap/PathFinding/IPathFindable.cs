﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPathFindable
{
    Vector2Int GridPosition
    {
        get;
        set;
    }

    Vector3 Position
    {
        get;
        set;
    }

    List<IPathFindable> Neighbours
    {
        get;
        set;
    }

    bool IsAvailable();

    float GetCost(IPathFindable other);

    UnityEngine.Object GetObjectToInstantiate();
}
