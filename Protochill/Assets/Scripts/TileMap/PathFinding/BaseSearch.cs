﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseSearch<Item> where Item : IPathFindable
{
    public abstract SearchResult<Item> DoSearch(Item start, Item goal);

    public abstract float Heuristic(Item a, Item b);

    protected void GetOrderPath(ref SearchResult<Item> searchResult, Item start, Item goal)
    {
        Item current = goal;
        while (current.Equals(start) == false)
        {
            searchResult.orderedPath.Add(current);
            current = searchResult.cameFrom[current];
        }

        searchResult.orderedPath.Reverse();
    }
}

public class SearchResult<Item> where Item : IPathFindable
{
    public bool succes;

    public List<Item> orderedPath = new List<Item>();

    public Dictionary<Item, Item> cameFrom = new Dictionary<Item, Item>();
    public Dictionary<Item, float> costSoFar = new Dictionary<Item, float>();
}
