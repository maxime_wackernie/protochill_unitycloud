﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStarSearch<Item> : BaseSearch<Item> where Item : IPathFindable
{
    public override SearchResult<Item> DoSearch(Item start, Item goal)
    {
        SearchResult<Item> searchResult = new SearchResult<Item>();

        PriorityQueue<Item> frontier = new PriorityQueue<Item>();
        frontier.Enqueue(start, 0f);

        searchResult.cameFrom[start] = start;
        searchResult.costSoFar[start] = 0f;

        while (frontier.Count > 0)
        {
            Item current = frontier.Dequeue();

            if (current.Equals(goal))
            {
                searchResult.succes = true;
                break;
            }

            foreach (Item next in current.Neighbours)
            {
                if (next.IsAvailable() == false && next.Equals(goal) == false)
                    continue;

                float newCost = searchResult.costSoFar[current] + next.GetCost(current); 
                if (!searchResult.costSoFar.ContainsKey(next) || newCost < searchResult.costSoFar[next])
                {
                    searchResult.costSoFar[next] = newCost;
                    float priority = newCost + Heuristic(next, goal);
                    frontier.Enqueue(next, priority);
                    searchResult.cameFrom[next] = current;
                }
            }
        }

        if (searchResult.succes)
        {
            GetOrderPath(ref searchResult, start, goal);
        }

        return searchResult;
    }

    public override float Heuristic(Item a, Item b)
    {
        return 0f;// Mathf.Abs(a.GetX() - b.GetX()) + Mathf.Abs(a.GetY() - b.GetY());
    }
}
