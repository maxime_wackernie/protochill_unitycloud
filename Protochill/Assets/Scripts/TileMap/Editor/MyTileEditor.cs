﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(MyTile))]
public class MyTileEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        MyTile myTile = (MyTile)target;
        if (GUILayout.Button("Forest"))
        {
            SetTile(myTile, ETileType.FOREST);
        }

        if (GUILayout.Button("Sand"))
        {
            SetTile(myTile, ETileType.SAND);
        }

        if (GUILayout.Button("Snow"))
        {
            SetTile(myTile, ETileType.SNOW);
        }

        if (GUILayout.Button("None"))
        {
            SetTile(myTile, ETileType.NONE);
        }
    }

    private void SetTile(MyTile tile, ETileType tileType)
    {
        tile.tileType = tileType;
        tile.TileMeshRenderer.enabled = tileType != ETileType.NONE;
        tile.TileMeshRenderer.material = GetMaterial(tileType);

        if (tileType == ETileType.NONE)
            tile.SetIsEditable(false, true);

        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }

    private Material GetMaterial(ETileType tileType)
    {
        TileTypeData tileTypeData = TileTypeData.Load();

        return tileTypeData.GetTileMaterial(tileType, false);
    }
}
