﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TileMapGeneratorData))]
public class TileMapGeneratorDataEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TileMapGeneratorData myTarget = (TileMapGeneratorData)target;

        if (GUILayout.Button("Generate TileMap Data"))
        {
            myTarget.Generate();
        }
    }
}
