﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTileMapComponent : BaseTileMapComponent<AStarSearch<MyTile>, MyTile>
{
    public Vector3 GetRandomPointOnTileMap()
    {
        return GetRandomTile().GetRandomPoint();
    }
}
