﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public enum ETileMapGenerationType
{
    DEFAULT,
    PREFAB,
    SCENE
}

[CreateAssetMenu(fileName = "TileMapGeneratorData", menuName = "ScriptableObjects/TileMap/TileMapGeneratorData", order = 1)]
public class TileMapGeneratorData : ScriptableObject
{
    [Header("Generator")]
    [SerializeField]
    private int x = 0;
    [SerializeField]
    private int y = 0;
    [SerializeField]
    private float tileSize = 1f;
    [SerializeField]
    private bool sizeY = false;
    [SerializeField]
    private ETileMapGenerationType generationType = ETileMapGenerationType.DEFAULT;

    [SerializeField]
    private GameObject tilePrefab = null;

    private void Awake()
    {
        if (tilePrefab != null && tilePrefab.GetComponent<IPathFindable>() == null)
        {
            Debug.LogError(tilePrefab + " does not implement IPathFindable.");
        }
    }

    public void Generate()
    {
        Vector2Int[] gridPositions = new Vector2Int[this.x * this.y];

        int id = 0;
        for (int y = 0; y < this.y; ++y)
        {
            for (int x = 0; x < this.x; ++x)
            {
                gridPositions[id] = new Vector2Int(x, y);
                ++id;
            }
        }

#if UNITY_EDITOR

        if (generationType == ETileMapGenerationType.SCENE)
        {
            GenerateInCurrentScene(gridPositions, tilePrefab, x, y, tileSize);
        }

        TileMapData asset = ScriptableObject.CreateInstance<TileMapData>();
        ProjectWindowUtil.CreateAsset(asset, "Assets/Datas/TileMapDatas/NewTileMapData.asset");

        asset.X = x;
        asset.Y = y;
        asset.GridPositions = gridPositions;
        asset.GenerationType = generationType;
        asset.TilePrefab = tilePrefab;
        asset.TileSize = tileSize;

        EditorUtility.SetDirty(asset);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
#endif
    }

#if UNITY_EDITOR
    private void GenerateInCurrentScene(Vector2Int[] gridPositions, GameObject prefab, float xCount, float yCount, float tileSize)
    {
        Transform parent;
        parent = new GameObject("Grid").transform;
        for (int i = 0; i < yCount; ++i)
        {
            new GameObject("Row_" + i).transform.SetParent(parent);
        }

        GameObject instance;
        Vector3 position;
        IPathFindable pathFindable = GetIPathFindable(prefab);
        foreach (Vector2Int gridPosition in gridPositions)
        {
            position = new Vector3(gridPosition.x * tileSize, 0f, gridPosition.y * tileSize);
            instance = PrefabUtility.InstantiatePrefab(pathFindable.GetObjectToInstantiate(), parent.GetChild(gridPosition.y)) as GameObject;
            instance.name = GetName(gridPosition);
            instance.transform.position = position;

            float scaleY = instance.transform.localScale.y;
            instance.transform.localScale = Vector3.one * tileSize;
            if (sizeY == false)
            {
                Vector3 scale = instance.transform.localScale;
                instance.transform.localScale = new Vector3(scale.x, scaleY, scale.z);
            }
        }
    }
#endif

    private IPathFindable GetIPathFindable(GameObject prefab)
    {
        IPathFindable res = null;
        foreach (MonoBehaviour monoBehaviour in prefab.GetComponents<MonoBehaviour>())
        {
            res = monoBehaviour.GetComponent<IPathFindable>();
            if (res != null)
            {
                return res;
            }
        }

        return res;
    }

    private GameObject Get(string name, ref GameObject[] gameObjects)
    {
        foreach (GameObject gameObject in gameObjects)
        {
            if (gameObject.name == name)
            {
                return gameObject;
            }
        }

        return null;
    }

    public static string GetName(Vector2Int gridPosition)
    {
        return "Tile [" + gridPosition.x + ", " + gridPosition.y + "]";
    }
}