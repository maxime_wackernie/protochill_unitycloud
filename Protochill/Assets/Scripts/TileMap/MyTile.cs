﻿using DG.Tweening;
using GLUnity.IOC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[System.Serializable]
public struct MyTileSave
{
    public SerializableVector2Int gridPosition;
    public BaseResourceComponentSave resourceSave;

    public string tileResource;
    public bool removedDecoration;
}

public class MyTile : BaseTile
{
    private const string decoPath = "Deco_Tile";
    private bool removedDecoration = false;

    public ETileType tileType = ETileType.NONE;

    [SerializeField]
    private bool isEditable;

    [SerializeField]
    private MeshRenderer tileMeshRenderer = null;

    [SerializeField]
    private TileResource startTileRessource = null;

    [HideInInspector]
    public GameObject spawnParticlesHabitat;
    [HideInInspector]
    public float spawnParticlesOffsetY;
    [HideInInspector]
    public GameObject tileResourcePrefab;

    public delegate void EventHandler();
    public event EventHandler OnTileDefaultPosition;

    private TileResource tileResource;

    private Vector3 startPosition;

    private bool isUnlocked;

    public bool IsEditable
    { 
        get { return isEditable && isUnlocked; }
    }

    public void SetIsEditable(bool value, bool setDirty)
    {
        isEditable = value;

#if UNITY_EDITOR
        if (setDirty)
        {
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    public bool IsUnlocked 
    {
        get { return isUnlocked; }
        set
        {
            isUnlocked = value;
        }
    }
    public MeshRenderer TileMeshRenderer { get { return tileMeshRenderer; } }
    public TileResource TileResource
    {
        get { return tileResource; }
        set 
        { 
            if (tileResource != null)
            {
                tileResource.OnRemove(this);
            }

            if (value != null)
            {
                value.OnPlace(this, false);
                BounceTilesOnSpawn();

                if (transform.Find(decoPath))
                {
                    Destroy(transform.Find(decoPath).gameObject);
                    removedDecoration = true;
                }
            }

            tileResource = value;

            // notify neighbours
            foreach (MyTile neighbour in neighbours)
            {
                neighbour.OnNeighbourRessourceTileUpdate(this);
            }
        }
    }

    public float DarkValue
    {
        get { return tileMeshRenderer.material.GetFloat(DragHandler.darkerPropertyId); ; }
        set
        {
            tileMeshRenderer.material.SetFloat(DragHandler.darkerPropertyId, value);
        }
    }

    private void Awake()
    {
        if (tileType == ETileType.NONE || tileMeshRenderer.enabled == false)
        {
            GetComponent<BoxCollider>().enabled = false;
        }
        startPosition = transform.position;
    }

    private void Start()
    {
        if (startTileRessource)
        {
            TileResource = startTileRessource;
        }
    }

    public void StartReset()
    {
        tileResource.StartReset(this);
    }

    public void ResetTile()
    {
        tileResource.OnRemove(this);
        tileResource = null;
    }

    public void Save()
    {
        MyTileSave myTileSave = new MyTileSave();
        myTileSave.gridPosition = new SerializableVector2Int(gridPosition);
        myTileSave.removedDecoration = removedDecoration;

        if (tileResource != null)
        {
            myTileSave.tileResource = tileResource.name;
            myTileSave.resourceSave = tileResource.GetSave(this);
        }

        Island island = GetComponentInParent<Island>();
        IslandSave islandSave = PrototypeProfile.Instance.ProfileData.islandSaves[island.IslandId];
        islandSave.tileSaves[GetTileId(island)] = myTileSave;
    }

    public void Load(Island island,  MyTileSave tileSave)
    {
        removedDecoration = tileSave.removedDecoration;
        if (string.IsNullOrEmpty(tileSave.tileResource) == false)
        {
            tileResource = SaveManager.Instance.GetScriptableObject<TileResource>(tileSave.tileResource);
            tileResource.Load(island, this, tileSave.resourceSave);
              
        }
        if (transform.Find(decoPath) && removedDecoration)
        {
            Destroy(transform.Find(decoPath).gameObject);
        }
    }

    public void EnableDecoration(bool enable)
    {
        if (transform.Find(decoPath))
        {
            transform.Find(decoPath).gameObject.SetActive(enable);
        }
    }

    private int GetTileId(Island island)
    {
        int count = 0;
        for (int i = 0; i < transform.parent.GetSiblingIndex(); ++i)
        {
            count += island.transform.GetChild(i).childCount;
        }

        return count + transform.GetSiblingIndex();
        //return gridPosition.y + (gridPosition.x * island.TileMapComponent.TileMap.Y);
    }

    private void OnNeighbourRessourceTileUpdate(MyTile neighbour)
    {
        if (tileResource != null)
        {
            tileResource.OnNeighbourUpdate(this, neighbour);
        }
    }



    #region IPathfindable
    public override Object GetObjectToInstantiate()
    {
        return gameObject;
    }

    public override bool IsAvailable()
    {
        return true;
    }

    public override float GetCost(IPathFindable other)
    {
        return 1f;
    }
    #endregion

    #region Event systems
    public void OnPointerDown(PointerEventData eventData)
    {
        if (tileResource != null)
        {
            tileResource.OnClick(this);
            BounceTile(TweenValueManager.Instance.clickTileDent, 0, TweenValueManager.Instance.clickTileDuration);
        }

        ResetIsland resetIsland = GetComponent<ResetIsland>();  
        if (resetIsland != null)
        {
            resetIsland?.OnPointerDown();
            BounceTile(TweenValueManager.Instance.clickTileDent, 0, TweenValueManager.Instance.clickTileDuration);
        }
        
    }
    #endregion

    public Vector3 GetRandomPoint()
    {
        Bounds bounds = tileMeshRenderer.bounds;
        return new Vector3(Random.Range(bounds.min.x, bounds.max.x), bounds.center.y, Random.Range(bounds.min.z, bounds.max.z)) + LevelManager.Instance.Offset;
    }

    private void BounceTilesOnSpawn()
    {
        SpawnDustParticle(TweenValueManager.Instance.mainTileDentDelay);

        BounceTile(TweenValueManager.Instance.mainTileDent, TweenValueManager.Instance.mainTileDentDelay);
        float time = TweenValueManager.Instance.heightMoveDuration + TweenValueManager.Instance.neighboursDentTotalDuration / 4f;
        foreach (MyTile neighbour in neighbours)
        {
            neighbour.BounceTile(TweenValueManager.Instance.neighboursDent, time);
            time += TweenValueManager.Instance.neighboursDentTotalDuration / 4f;
        }
    } 

    public void BounceTile(float yOffset, float delay = 0f, float duration = 0.2f, Ease ease = Ease.OutQuad, TweenCallback callback = null)
    {
        StartCoroutine(_BounceTile(yOffset, delay, duration, ease, callback));
    }

    public IEnumerator _BounceTile(float yOffset, float delay, float duration, Ease ease, TweenCallback callback = null)
    {
        yield return new WaitForSeconds(delay);
        float yDown = Random.Range(yOffset - (-yOffset * TweenValueManager.Instance.generalDentRandomness), yOffset );

        transform.DOMoveY(startPosition.y + yDown, duration).SetEase(ease).OnComplete(() => 
        {
            var tween = transform.DOMoveY(startPosition.y, duration);
            tween.SetEase(ease);

            if (callback != null)
                tween.onComplete += callback;

            tween.onComplete += () => {
                OnTileDefaultPosition?.Invoke();
                if (tileResource == null || !TweenValueManager.Instance.heightOnOccupedTiles)
                    return;
                transform.DOMoveY(Position.y + TweenValueManager.Instance.occupedHeight, TweenValueManager.Instance.occupedDuration);
            };
        });
    }

    public void SpawnDustParticle(float delay)
    {
        StartCoroutine(_SpawnParticleHabitat(delay));
    }

    private IEnumerator _SpawnParticleHabitat(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (spawnParticlesHabitat != null)
        {
            Vector3 v = new Vector3(transform.position.x, transform.position.y + spawnParticlesOffsetY, transform.position.z);
            GameObject p = Instantiate(spawnParticlesHabitat, v, spawnParticlesHabitat.transform.rotation, transform);
            p.AddComponent<AutoDestroy>().destroyAfter = 3f;
        }
    }
}
