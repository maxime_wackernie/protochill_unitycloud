﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridLayoutGroup3D : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer meshRenderer = null;

    [SerializeField]
    private Vector3 offsetBounds = Vector3.zero;

    [SerializeField]
    private Vector2 padding = Vector2.zero;

    [SerializeField]
    private Vector2 spacing = Vector2.zero;

    [SerializeField]
    private int columnCount = 5;

    private Bounds bounds;

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        if (meshRenderer != null)
        {
            Vector3 size = meshRenderer.bounds.size + offsetBounds;
            bounds = new Bounds(meshRenderer.bounds.center, size);
            Init(bounds);
        }
    }

    public void Init(Bounds bounds)
    {
        this.bounds = bounds;
    }

    public void Refresh()
    {
        Vector3 startPosition = bounds.min;
        startPosition.x += padding.x;
        startPosition.z += padding.y;

        Vector3 position;
        Transform child;
        int div, modulo;

        for (int i = 0; i < transform.childCount; ++i)
        {
            div = i / columnCount;
            modulo = i % columnCount;

            position.x = startPosition.x + (modulo * spacing.x);
            position.z = startPosition.z + (div * spacing.y);
            position.y = bounds.max.y;

            child = transform.GetChild(i);
            child.position = position;
        }
    }

    [ContextMenu("Refresh")]
    public void DebugRefresh()
    {
        Vector3 size = meshRenderer.bounds.size;

        bounds = new Bounds(meshRenderer.bounds.center, size);

        Refresh();
    }
}
