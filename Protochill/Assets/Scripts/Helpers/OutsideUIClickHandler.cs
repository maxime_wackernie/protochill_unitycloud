﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using GLUnity.IOC;

[IOCExpose]
public class OutsideUIClickHandler : MonoBehaviour
{
    private List<OutsideClickComponent> outsideClickCollection = new List<OutsideClickComponent>();

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            OnClick(Input.mousePosition);
        }
#else
        if (Input.touchCount > 0)
        {
            OnClick(Input.GetTouch(0).position);
        }
#endif
    }

    private void OnClick(Vector3 inputPosition)
    {
        foreach (OutsideClickComponent clickComponent in outsideClickCollection)
        {
            if (clickComponent.gameObject.activeInHierarchy && clickComponent.ProcessingFrame == false)
            {
                if (RectTransformUtility.RectangleContainsScreenPoint(clickComponent.Panel, inputPosition) == false)
                {
                    clickComponent.RaiseCallback();
                }
            }
        }
    }

    public void RegisterToOutsideClick(OutsideClickComponent outsideClickComponent)
    {
        if (outsideClickCollection.Contains(outsideClickComponent) == false)
        {
            outsideClickCollection.Add(outsideClickComponent);
        }
        else
        {
            Debug.LogWarning("OutsideUIClickHandler already contains: " + outsideClickComponent.name);
        }
    }

    public void UnregisterFromOutsideClick(OutsideClickComponent outsideClickComponent)
    {
        if (outsideClickCollection.Remove(outsideClickComponent) == false)
        {
            Debug.LogWarning("OutsideUIClickHandler failed to remove: " + outsideClickComponent.name);

        }
    }
}
