﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

[System.Serializable]
public class AnimalTimerEvent
{
    [SerializeField]
    private Vector2 timerRange = Vector2.one;

    [SerializeField]
    private Vector2Int animalRange = Vector2Int.one;

    [SerializeField]
    private bool loop = true;

    private int timerId;
    public delegate void AnimalTimerDelegate(Animal animal);
    private AnimalTimerDelegate completeCallback;

    private float RandomDuration { get { return Random.Range(timerRange.x, timerRange.y); } }
    private int RandomAnimalCount { get { return Random.Range(animalRange.x, animalRange.y); } }

    public void Init(AnimalTimerDelegate completeCallback)
    {
        if (timerRange.x < 0 && timerRange.y < 0)
            return;

        this.completeCallback = completeCallback;
        timerId = TimerManager.Instance.AddTimer(RandomDuration, true, loop, OnTimerComplete);
    }

    public void DoEvent()
    {
        DoEvent(false);
    }

    private void OnTimerComplete(int id)
    {
        DoEvent(true);
    }

    private void DoEvent(bool resetTimer)
    {
        if (completeCallback != null)
        {
            Animal[] animals;
            if (AnimalManager.Instance.GetRandomAnimals(RandomAnimalCount, out animals))
            {
                foreach (Animal animal in animals)
                {
                    completeCallback.Invoke(animal);
                }
            }
        }

        if (resetTimer)
        {
            TimerManager.Instance.SetTimerDuration(timerId, RandomDuration, true);
        }
    }
}
