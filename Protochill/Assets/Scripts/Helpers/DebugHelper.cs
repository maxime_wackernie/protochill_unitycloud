﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity.IOC;

public class DebugHelper : MonoBehaviour
{
    public KeyCode fakeOffline = KeyCode.E;
    public KeyCode plusIncome = KeyCode.KeypadPlus;
    public KeyCode minusIncome = KeyCode.KeypadMinus;
    public KeyCode tutorialInput = KeyCode.T;

    public OfflineUI offlineUI = null;
    public float income = 100f;
    public float scrollSensitivity = 1f;

    public float offlineTime = 10;

    public TutorialData tutorial = null;

    [Dependency]
    private CameraController cameraController = null;

    private void Update()
    {
        if (Input.GetKeyDown(plusIncome) || Input.GetKeyDown(KeyCode.P))
        {
            PlusIncome();
        }
        else if (Input.GetKeyDown(minusIncome))
        {
            MinusIncome();
        }
        else if (Input.GetKeyDown(tutorialInput))
        {
            if (tutorial != null)
            {
                FindObjectOfType<TutorialHandler>().StartTutorial(tutorial);
            }
        }
        else if (Input.GetKeyDown(fakeOffline))
        {
            FakeOffline();
        }

        float scrollWheel = Input.GetAxis("Mouse ScrollWheel");
        if (scrollWheel != 0f)
        {
            cameraController.AddPerspectiveZoom(-scrollWheel * scrollSensitivity/5f);
        }
    }

    private void FakeOffline()
    {
        SaveManager.Instance.ResetValues();
        SaveManager.offlineSeconds = offlineTime;
        SaveManager.Instance.LoadEggs();
        SaveManager.offlineIncome = offlineTime * IncomeManager.Instance.IncomeAverage;

        offlineUI.Init();
    }

    private void PlusIncome()
    {
        IncomeManager.Instance.PlusIncome(income);
    }

    private void MinusIncome()
    {
        IncomeManager.Instance.MinusIncome(income);
    }
}
