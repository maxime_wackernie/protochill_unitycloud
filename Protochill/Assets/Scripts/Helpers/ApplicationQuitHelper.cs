﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[DefaultExecutionOrder(-1)]
public class ApplicationQuitHelper : MonoBehaviour
{
    private static bool isQuittingApplication;
    public static bool IsQuittingApplication { get { return isQuittingApplication; } }

    private void Awake()
    {
        isQuittingApplication = false;
    }

    private void OnApplicationQuit()
    {
        isQuittingApplication = true;
    }
}
