﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimEventListener : MonoBehaviour
{
    public System.Action animationEnd;

    public void OnAnimationEnd()
    {
        if (animationEnd != null)
        {
            animationEnd.Invoke();
        }
    }
}
