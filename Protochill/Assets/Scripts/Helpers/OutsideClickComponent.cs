﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using GLUnity;
using GLUnity.IOC;

public class OutsideClickComponent : MonoBehaviour
{
    [SerializeField]
    private RectTransform panel = null;

    [SerializeField]
    private UnityEvent callback = null;

    [SerializeField]
    private bool deactivateGameObject = false;

    [Dependency]
    private OutsideUIClickHandler handler = null;

    private bool processingFrame;

    public RectTransform Panel { get { return panel; } }
    public UnityEvent Callback { get { return callback; } }
    public bool ProcessingFrame { get { return processingFrame; } }

    private void Awake()
    {
        if (deactivateGameObject)
        {
            callback.AddListener(DeactivateGameObject);
        }
    }

    private void Start()
    {
        handler.RegisterToOutsideClick(this);
    }

    private void OnEnable()
    {
        processingFrame = true;

        CoroutineHelper.Instance.WaitForEndOfFrame(OnEndFrameEnable);
    }

    private void OnDestroy()
    {
        if (ApplicationQuitHelper.IsQuittingApplication == false)
        {
            handler.UnregisterFromOutsideClick(this);
        }
    }

    private void DeactivateGameObject()
    {
        gameObject.SetActive(false);
    }

    private void OnEndFrameEnable()
    {
        processingFrame = false;
    }

    public void RaiseCallback()
    {
        if (callback != null)
        {
            callback.Invoke();
        }
    }
}
