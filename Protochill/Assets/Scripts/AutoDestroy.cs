﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    public float destroyAfter = 0;
    void Start()
    {
        StartCoroutine(DestroyAfter(destroyAfter));
    }

    private IEnumerator DestroyAfter(float destroyAfter)
    {
        if (destroyAfter == 0)
            yield break;

        yield return new WaitForSeconds(destroyAfter);

        Destroy(this.gameObject);
    }
}
