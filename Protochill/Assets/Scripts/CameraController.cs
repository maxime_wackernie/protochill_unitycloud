﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity.IOC;
using UnityEngine.UI;
using System.CodeDom;
using System.Runtime.Serialization.Json;

[IOCExpose]
public class CameraController : MonoBehaviour
{
    [Header("Bounds")]
    [SerializeField]
    private bool useBounds = false;
    [SerializeField]
    private Vector2 heightRange = Vector2.zero;
    [SerializeField]
    private Vector2 widthRange = Vector2.zero;
    [SerializeField]
    private Vector2 depthRange = Vector2.zero;

    [Header("Drag")]
    [SerializeField]
    private float smoothTimeDrag = 0.08f;
    [SerializeField]
    private float smoothTimeInertia = 0.16f;
    [SerializeField]
    private float deltaMultiplier = 10f;
    [SerializeField]
    private Vector2 magnitudeClampInertia = new Vector2(35f, 200f);

    private float smoothTime;

    [Header("Zoom")]
    [SerializeField]
    private float orthoZoomSpeed = 0.5f;
    [SerializeField]
    private float persZoomSpeed = 5f;
    

    private Camera myCamera;

    private Vector3 target;
    private Vector3 velocity;

    private GraphicRaycaster[] graphicRaycasters;

    private bool isCameraFreeze = false;
    private bool isCameraFreezeInput = false;
    private float durationFreeze = 0f;

    public delegate void EventHandler();
    public event EventHandler OnEndCenterCameraOnPosition;
    private bool centerTheCamera = false;
    private Vector3 centerTheCameraPosition;

    private Vector3 Target
    {
        get { return target; }
        set
        {
            if (!MaxZoom(value.y) && !isCameraFreeze)
            {
                target.x = useBounds ? Mathf.Clamp(value.x, widthRange.x, widthRange.y) : value.x;
                target.y = useBounds ? Mathf.Clamp(value.y, heightRange.x, heightRange.y) : value.y;
                target.z = useBounds ? Mathf.Clamp(value.z, depthRange.x, depthRange.y) : value.z;
            }
        }
    }

    private void Awake()
    {
        smoothTime = smoothTimeDrag;
        myCamera = GetComponent<Camera>();
        graphicRaycasters = FindObjectsOfType<GraphicRaycaster>();
        target = myCamera.transform.position;
    }

    private void Update()
    {
        myCamera.transform.position = Vector3.SmoothDamp(myCamera.transform.position, target, ref velocity, smoothTime);
        
        ManageFreezeCamera();

        if (centerTheCamera == true && Vector3.Distance(myCamera.transform.position, target) < 0.001f)
        {
            OnEndCenterCameraOnPosition?.Invoke();
            centerTheCamera = false;
        }
    }

    private bool MaxZoom(float currentZoom)
    {
        if (currentZoom < heightRange.x || currentZoom > heightRange.y)
            return true;
        return false;
    }


    public void CenterCameraOnPosition(Vector3 position, float distance, float durationFreeze = 0f)
    {
        smoothTime = smoothTimeInertia;
        Target = position - myCamera.transform.forward * distance;
        FreezeCamera(durationFreeze);

        centerTheCamera = true;
        centerTheCameraPosition = Target;
    }

    public void FreezeCamera(float durationFreeze)
    {
        if (durationFreeze <= 0f)
        {
            isCameraFreeze = false;
        }

        this.durationFreeze = durationFreeze;
    }

    public void FreezeCameraInput()
    {
        isCameraFreezeInput = true;
    }

    public void UnfreezeCameraInput()
    {
        isCameraFreezeInput = false;
    }

    private void ManageFreezeCamera()
    {
        if (durationFreeze > 0f)
        {
            durationFreeze -= Time.deltaTime;
            if (durationFreeze > 0f)
                isCameraFreeze = true;
            if (durationFreeze <= 0f)
            {
                isCameraFreeze = false;
                durationFreeze = 0f;
            }
        }
    }

    public void AddTranslate(Vector3 position)
    {
        if (isCameraFreezeInput)
            return;

        Target = myCamera.transform.position + position;
        centerTheCamera = false;
    }

    public void StopMove()
    {
        Target = myCamera.transform.position;
        smoothTime = smoothTimeDrag;
    }

    public void DoInertia(Vector2 delta)
    {
//#if !UNITY_EDITOR
        if ((delta.x < 7f && delta.x > -7f && delta.y < 7f && delta.y > -7f))
            return;
//#endif
        smoothTime = smoothTimeInertia;
        Vector3 v = new Vector3(0, myCamera.transform.rotation.eulerAngles.y, 0);

        Target += velocity.normalized * deltaMultiplier * Mathf.Clamp(delta.magnitude, magnitudeClampInertia.x, magnitudeClampInertia.y);
    }

    public void AddOrthoGraphicZoom(float value)
    {
        if (isCameraFreezeInput)
            return;
        myCamera.orthographicSize += value * orthoZoomSpeed * Time.deltaTime;
    }

    public void AddPerspectiveZoom(float value)
    {
        if (isCameraFreezeInput)
            return;
        Target = myCamera.transform.position + (-transform.forward * value) * Time.deltaTime * persZoomSpeed;
    }

    public void EnableGraphicRaycasters(bool enable)
    {
        foreach (GraphicRaycaster graphicRaycaster in graphicRaycasters)
        {
            graphicRaycaster.enabled = enable;
        }
    }
}
