﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    [SerializeField]
    private int maxLevel = -1;
    [SerializeField]
    private string key = string.Empty;
    [SerializeField]
    private IncrementalData incrementalData = null;
    [SerializeField]
    private ProductionData productionData = null;

    [SerializeField]
    private ProductionData productionStep = null;
    [SerializeField]
    private bool initLevel = false;

    private int level;
    private int stepLevel;

    public System.Action levelUpdateEvent;
    public System.Action stepLevelUpdateEvent;

    public IncrementalData IncrementalData 
    {
        get { return incrementalData; } 
        set { incrementalData = value; }
    }
    public ProductionData ProductionData 
    {
        get { return productionData; } 
        set { productionData = value; }
    }
    public ProductionData ProductionStep { get { return productionStep; } }
    public float Cost { get { return incrementalData ? incrementalData.GetCost(level) : -1f; } }
    public float StepCost { get { return incrementalData ? incrementalData.GetCost(StepLevel) : -1f; } }
    public string Key { get { return key; } }
    public bool IsMaxLevel { get { return level == maxLevel; } }
    public int MaxLevel { get { return maxLevel; } }
    public int StepLevel 
    {
        get { return stepLevel; }
        set 
        {
            stepLevel = value;

            if (stepLevelUpdateEvent != null)
            {
                stepLevelUpdateEvent.Invoke();
            }

            if (string.IsNullOrEmpty(key) == false)
            {
                SaveManager.Instance.SaveGenerator(key, this);
            }
        }
    }
    public int Level
    {
        get { return level; }
        set
        {
            if (maxLevel > 0 && value > maxLevel)
                return;

            level = value;

            if (levelUpdateEvent != null)
            {
                levelUpdateEvent.Invoke();
            }

            if (string.IsNullOrEmpty(key) == false)
            {
                SaveManager.Instance.SaveGenerator(key, this);
            }
        }
    }

    private void Start()
    {
        if (initLevel)
        {
            if (string.IsNullOrEmpty(key) == false)
            {
                Load(key);
            }
            else
            {
                Level = 0;
            }
        }
    }

    public void Init(IncrementalData incrementalData, ProductionData productionData, ProductionData productionStep, int level)
    {
        this.incrementalData = incrementalData;
        this.productionData = productionData;
        this.productionStep = productionStep;

        Level = level;
    }

    public void Load(string key)
    {
        GeneratorSaveData data = SaveManager.Instance.LoadGenerator(key);
        Level = data.level;
        StepLevel = data.stepLevel;
    }

    public float GetCostAt(int level)
    {
        return incrementalData.GetCost(level);
    }

    public float GetProduction()
    {
        return productionData.GetProduction(level + 1);
    }
}
