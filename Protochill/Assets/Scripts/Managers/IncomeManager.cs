﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

[DefaultExecutionOrder(-1)]
public class IncomeManager : Singleton<IncomeManager>
{
    [Header("Parameters")]
    [SerializeField]
    private float startIncome = 1000f;

    [Header("Display income")]
    [SerializeField]
    private int displayDigitCount = 3;
    [SerializeField]
    private int displayDecimalCount = 1;
    [SerializeField]
    private string[] incomeSuffixes = null;

    private float income;
    private float incomeAverage;
    private float incomeMultiplier;

    public System.Action<float> incomeUpdateEvent;
    public System.Action<float> incomeAverageUpdateEvent;
    public System.Action<float> incomeMultiplierUpdateEvent;

    private int incomeTimerId;

    public int IncomeTimerId { get { return incomeTimerId; } }
    public float Income
    {
        get { return income; }
        private set
        {
            income = value;

            if (income < 0f)
            {
                income = 0f;
            }

            if (incomeUpdateEvent!= null)
            {
                incomeUpdateEvent.Invoke(income);
            }
        }
    }

    public float IncomeAverage
    {
        get { return incomeAverage; }
        set
        {
            incomeAverage = value;

            if (incomeAverageUpdateEvent != null)
            {
                incomeAverageUpdateEvent.Invoke(value);
            }
        }
    }

    public float IncomeMultiplier
    {
        get { return incomeMultiplier; }
        set
        {
            incomeMultiplier = value;

            if (incomeMultiplierUpdateEvent != null)
            {
                incomeMultiplierUpdateEvent.Invoke(value);
            }
        }
    }

    public float TotalIncomeAverage
    {
        get 
        {
            float perc = 1f + (incomeMultiplier / 100f);
            return incomeAverage * perc;
        }
    }

    private void Start()
    {
        incomeTimerId = TimerManager.Instance.AddTimer(1f, true, true, OnIncomeTimerComplete);
    }

    public void Load()
    {
        if (PrototypeProfile.Instance.ProfileData.hasSave)
        {
            Income = PrototypeProfile.Instance.ProfileData.income;
        }
        else
        {
            Income = startIncome;
        }

        if (AnimalManager.Instance.Animals.Count == 0)
        {
            IncomeAverage = 0f;
        }
    }

    public bool CanBuy(float value)
    {
        return value <= income;
    }

    public void PlusIncome(float value)
    {
        Income += value;
    }

    public void MinusIncome(float value)
    {
        Income -= value;
    }

    public void IncomeToString(out string res)
    {
        IncomeToString(income, displayDigitCount, displayDecimalCount, out res);
    }
    public void IncomeToString(float value, out string res)
    {
        IncomeToString(value, displayDigitCount, displayDecimalCount, out res);
    }

    public void IncomeToString(float value, int digitCount, int decimalCount, out string res)
    {
        if (Mathf.Approximately(value, 0f) || value < 0f)
        {
            res = "0";
            return;
        }

        int length = (int)(Mathf.Log10(value) + 1);
        int modulo = length % digitCount;
        int id = (length / (digitCount)) - (modulo == 0 ? 1 : 0);
        if (id < 0)
            id = 0;

        float div;
        if (id < incomeSuffixes.Length)
        {
            div = value / Mathf.Pow(10, digitCount * id);
        }
        else
        {
            div = value;
        }

        if (decimalCount > 0)
        {
            float decimalFactor = Mathf.Pow(10, decimalCount);
            div = Mathf.Round(div * decimalFactor) / decimalFactor;
        }
        else
        {
            div = Mathf.Round(div);
        }

        //print("length: " + length);
        //print("modulo: " + modulo);
        //print("id: " + id);
        //print("div: " + div);
        //print("div v2: " + div);

        res = div + (id < incomeSuffixes.Length ? incomeSuffixes[id] : incomeSuffixes[incomeSuffixes.Length - 1]);
    }

    private void OnIncomeTimerComplete(int id)
    {
        if (TotalIncomeAverage != 0)
        {
            Income += TotalIncomeAverage;
        }
    }
}