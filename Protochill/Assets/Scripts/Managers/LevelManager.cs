﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GLUnity;
using UnityEngine.UI;

public struct GetTileCondition
{
    public GetTileCondition(LevelManager.TileConditionDelegate condition, params object[] parameters)
    {
        this.condition = condition;
        this.parameters = parameters;
    }

    public bool IsCondition(MyTile tile)
    {
        return condition.Invoke(tile, parameters);
    }

    public LevelManager.TileConditionDelegate condition;
    public object[] parameters;
}

public class LevelManager : Singleton<LevelManager>
{
    [SerializeField]
    private Transform islandContent = null;
    [SerializeField]
    private Generator buyIslandGenerator = null;

    [SerializeField]
    private RarityData rarityData = null;
    [SerializeField]
    private TileTypeData tileTypeData = null;
    [SerializeField]
    private Vector3 offset = Vector3.zero;
    [SerializeField]
    private LayerMask tileLayer = 0;

    [SerializeField]
    private TileMapData tileMapData = null;
    [SerializeField]
    private GraphicRaycaster[] graphicRaycasters = null;

    [Header("Edition")]
    [SerializeField]
    private bool darkerOnlyUnlockedTiles = true;

    private Island[] islands;

    private Camera myCamera;
    private EventSystem eventSystem = null;

    private List<Farm> farms = new List<Farm>();

    public System.Action<MyTile, TileResource> resourcePlaceEvent;

    public Transform IslandContent { get { return islandContent; } }
    public Generator BuyIslandGenerator { get { return buyIslandGenerator; } }
    public RarityData RarityData { get { return rarityData; } }
    public TileMapData TileMapData { get { return tileMapData; } }
    public Vector3 Offset { get { return offset; } }
    public List<Farm> Farms { get { return farms; } }

    private MyTile lastClicked;
    public delegate bool TileConditionDelegate(MyTile tile, params object[] parameters);

    public static bool enableEggHatch;

    public MyTile LastClicked
    { 
        get { return lastClicked; } 
        set { lastClicked = value; }
    }

    private void Awake()
    {
        enableEggHatch = true;

        islands = FindObjectsOfType<Island>();

        myCamera = Camera.main;

        eventSystem = FindObjectOfType<EventSystem>();
    }

    public void OnMouseButtonDown()
    {
        lastClicked = DoRaycast(Input.mousePosition);
    }

    public void OnMouseButtonUp()
    {
        MyTile myTile = DoRaycast(Input.mousePosition);
        if (myTile != null && lastClicked != null && myTile == lastClicked)
        {
            lastClicked = null;
            myTile.OnPointerDown(null);            
        }
    }

    private MyTile DoRaycast(Vector3 position)
    {
        PointerEventData m_PointerEventData = new PointerEventData(eventSystem);
        m_PointerEventData.position = position;

        List<RaycastResult> results = new List<RaycastResult>();
        foreach (GraphicRaycaster graphicRaycaster in graphicRaycasters)
        {
            graphicRaycaster.Raycast(m_PointerEventData, results);
            if (results.Count != 0)
            {
                return null;
            }
        }

        RaycastHit hit;
        Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100f, tileLayer))
        {
            return hit.collider.GetComponent<MyTile>();
        }

        return null;
    }

    public void GetUnlockedTiles(out List<MyTile> tiles)
    {
        tiles = new List<MyTile>();

        foreach (Island island in islands)
        {
            if (island.IsUnlocked)
            {
                tiles.AddRange(island.TileMapComponent.TileMap.GetTiles());
            }
        }
    }

    public void GetTiles(out List<MyTile> tiles)
    {
        tiles = new List<MyTile>();

        foreach (Island island in islands)
        {
            tiles.AddRange(island.TileMapComponent.TileMap.GetTiles());
        }
    }

    public void GetTileCondition(out List<MyTile> tiles, params GetTileCondition[] conditions)
    {
        tiles = new List<MyTile>();

        List<MyTile> unlockedTiles;
        if (darkerOnlyUnlockedTiles)
        {
            GetUnlockedTiles(out unlockedTiles);
        }
        else
        {
            GetTiles(out unlockedTiles);
        }

        foreach (MyTile tile in unlockedTiles)
        {
            if (HasOneCondition(tile, conditions))
            {
                if (/*checkIfContains && */tiles.Contains(tile))
                {
                    continue;
                }

                tiles.Add(tile);
            }
        }
    }

    private bool HasOneCondition(MyTile tile, GetTileCondition[] conditions)
    {
        foreach (GetTileCondition condition in conditions)
        {
            if (condition.IsCondition(tile))
            {
                return true;
            }    
        }
        return false;
    }

    public Island GetRandomUnlockedIsland()
    {
        List<Island> res = new List<Island>();
        foreach (Island island in islands)
        {
            if (island.IsUnlocked)
            {
                res.Add(island);
            }
        }

        return res[Random.Range(0, res.Count)];
    }

    public Island GetIsland(int id)
    {
        if (id < islandContent.childCount)
        {
            return islandContent.GetChild(id).GetComponent<Island>();
        }

        return null;
    }

    public Material GetTileMaterial(ETileType tileType, bool on)
    {
        return tileTypeData.GetTileMaterial(tileType, on);
    }

    public void RaiseResourcePlaced(MyTile tile, TileResource resource)
    {
        if (resourcePlaceEvent != null)
        {
            resourcePlaceEvent.Invoke(tile, resource);
        }
    }

    public MyTile GetTile(TileInfo tileInfo)
    {
        return GetIsland(tileInfo.islandId).TileMapComponent.TileMap.GetTile(tileInfo.gridPosition);
    }
}
