﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

[System.Serializable]
public struct PrefabHolderData
{
    public string key;
    public GameObject prefab;
    public Transform parent;
}

public struct PrefabInstanceData
{
    public PrefabInstanceData(GameObject prefab, Transform parent)
    {
        this.prefab = prefab;
        this.parent = parent;
    }

    public GameObject prefab;
    public Transform parent;
}

public class PrefabHolder : Singleton<PrefabHolder>
{
    [SerializeField]
    private PrefabHolderData[] prefabs = null;

    private Dictionary<string, PrefabInstanceData> prefabDict;

    private void Awake()
    {
        prefabDict = new Dictionary<string, PrefabInstanceData>(prefabs.Length);
        foreach (PrefabHolderData data in prefabs)
        {
            prefabDict.Add(data.key, new PrefabInstanceData(data.prefab, data.parent));
        }
    }

    public GameObject GetPrefab(string key)
    {
        if (prefabDict.ContainsKey(key))
        {
            return prefabDict[key].prefab;
        }

        Debug.LogError("Key not found");
        return null;
    }

    public T GetPrefab<T>(string key) where T : Component
    {
        if (prefabDict.ContainsKey(key))
        {
            return prefabDict[key].prefab.GetComponent<T>();
        }

        Debug.LogError("Key not found");
        return null;
    }

    public T InstantiatePrefab<T>(string key, Transform parent = null) where T : MonoBehaviour
    {
        if (prefabDict.ContainsKey(key))
        {
            PrefabInstanceData data = prefabDict[key];

            return ExtensionMethods.Instantiate(data.prefab, data.parent).GetComponent<T>();
        }

        Debug.LogError("Key not found");
        return null;
    }

}
