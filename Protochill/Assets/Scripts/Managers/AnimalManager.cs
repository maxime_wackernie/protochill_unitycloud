﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;
using System;
using Random = UnityEngine.Random;

public class AnimalManager : Singleton<AnimalManager>
{
    [Header("Animals")]
    [SerializeField]
    private bool enableAnimalShadows = false;
    [SerializeField]
    private AnimalTimerEvent smileyEvent = new AnimalTimerEvent();
    [SerializeField]
    private AnimalTimerEvent coinEvent = new AnimalTimerEvent();
    [SerializeField]
    private AnimalTimerEvent moveEvent = new AnimalTimerEvent();

    [Header("Parameters")]
    [SerializeField]
    private Transform animalContent = null;
    [SerializeField]
    private CollectionUI collectionUI = null;
    [SerializeField]
    public AnimalData[] animalDatas = null;

    private List<Animal> animals = new List<Animal>();

    public System.Action<Animal> animalCountUpdateEvent;

    public Dictionary<string, int> animalsUnlocked = new Dictionary<string, int>();

    public List<Animal> Animals { get { return animals; } }
    public bool EnableAnimalShadows { get { return enableAnimalShadows; } }

    private void Start()
    {
        smileyEvent.Init(Animal.SpawnSmiley);
        coinEvent.Init(Animal.SpawnCoin);
        moveEvent.Init(Animal.MoveToRandomPoint);

        LoadAnimalsUnlocked();
    }

    #region Save
    public void Save()
    {
        if (animals.Count > 0)
        {
            List<AnimalSave> animalSaves = new List<AnimalSave>();
            //AnimalSave[] animalSaves = new AnimalSave[animals.Count];
            for (int i = 0; i < animals.Count; ++i)
            {
                if (animals[i].isActiveAndEnabled)
                    animalSaves.Add(new AnimalSave(animals[i]));
            }

            PrototypeProfile.Instance.ProfileData.animalSaves = animalSaves.ToArray();
        }
    }

    public void Load()
    {
        if (PrototypeProfile.Instance.ProfileData.animalSaves != null)
        {
            Animal instance;
            AnimalData animalData;
            Habitat habitat;
            foreach (AnimalSave animalSave in PrototypeProfile.Instance.ProfileData.animalSaves)
            {
                habitat = LevelManager.Instance.GetIsland(animalSave.islandId).
                    TileMapComponent.TileMap.GetTile(animalSave.habitatGridPosition.ToVector2Int())
                    .GetComponent<Habitat>();

                animalData = GetAnimalData(animalSave.animalData);
                instance = SpawnAnimal(animalData, animalSave.position.ToVector3());
                instance.Init(animalData, habitat, animalSave.happiness);
            }

            moveEvent.DoEvent();
        }
    }
    #endregion

    public Animal SpawnAnimal(AnimalData animalData, Vector3 position)
    {
        Animal instance = GameObject.Instantiate(animalData.Prefab, animalContent);
        instance.transform.position = position;

        return instance;
    }

    public void AddAnimal(Animal animal)
    {
        animals.Add(animal);

        IncomeManager.Instance.IncomeAverage += animal.AnimalData.AnimalIncome.BaseProduction;

        if (animalCountUpdateEvent != null)
        {
            animalCountUpdateEvent.Invoke(animal);
        }
    }

    public void RemoveAnimal(Animal animal)
    {
        int i = 0; bool b = false;
        b = animalsUnlocked.TryGetValue(animal.AnimalData.name, out i);
        if (i != 0 && b)
        {
            animalsUnlocked[animal.AnimalData.name] = i - 1;
            animals.Remove(animal);
            animalCountUpdateEvent.Invoke(animal);
            IncomeManager.Instance.IncomeAverage -= animal.AnimalData.AnimalIncome.BaseProduction;
        }        
        SaveAnimalsUnlocked();
    }

    public bool GetRandomAnimals(int count, out Animal[] randAnimals)
    {
        randAnimals = null;

        if (count <= 0 || animals.Count == 0)
            return false;

        count = Mathf.Clamp(count, 0, animals.Count);
        randAnimals = new Animal[count];

        List<int> excludeIds = new List<int>();

        int id;
        int it = 0;
        while (it < count)
        {
            id = Random.Range(0, animals.Count);
            if (excludeIds.Contains(id))
                continue;

            excludeIds.Add(id);
            randAnimals[it] = animals[id];
            ++it;
        }

        return true;
    }

    public AnimalData GetAnimalData(string assetName)
    {
        foreach(AnimalData data in animalDatas)
        {
            if (data.name == assetName)
                return data;
        }
        return null;
    }

    public bool HasUnlocked(AnimalData animalData)
    {
        return HasUnlocked(animalData.name);
    }

    public bool HasUnlocked(string animalDataName)
    {
        return animalsUnlocked.ContainsKey(animalDataName);
    }

    public int CountAnimal(AnimalData animalData)
    {
        return CountAnimal(animalData.name);
    }

    public int CountAnimal(string animalDataName)
    {
        animalsUnlocked.TryGetValue(animalDataName, out int count);
        return count;
    }

    private void LoadAnimalsUnlocked()
    {
        if (PrototypeProfile.Instance.ProfileData.animalsUnlockedSaveDatas == null)
            return;
        //int totalCount = 0;
        AnimalsUnlockedSaveData[] save = PrototypeProfile.Instance.ProfileData.animalsUnlockedSaveDatas;
        foreach (AnimalsUnlockedSaveData data in save)
        {
            animalsUnlocked.Add(data.name, data.count);
            collectionUI.Load(GetAnimalData(data.name), data.count);
            //totalCount += data.count;
        }
    }

    public void FillAnimalsUnlocked(AnimalData animalData)
    {
        if (animalsUnlocked.ContainsKey(animalData.name))
        {
            int count = 0;
            animalsUnlocked.TryGetValue(animalData.name, out count);
            animalsUnlocked[animalData.name] = count+1;
        }
        else
        {
            animalsUnlocked.Add(animalData.name, 1);
        }

        SaveAnimalsUnlocked();
    }

    private void SaveAnimalsUnlocked()
    {
        AnimalsUnlockedSaveData[] save = new AnimalsUnlockedSaveData[animalsUnlocked.Count];
        int i = 0;
        foreach (KeyValuePair<string, int> kvp in animalsUnlocked)
        {
            AnimalsUnlockedSaveData data = new AnimalsUnlockedSaveData(kvp.Key, kvp.Value);
            save[i] = data;
            i++;
        }
        PrototypeProfile.Instance.ProfileData.animalsUnlockedSaveDatas = save;
        PrototypeProfile.Instance.Save();
    }

    public int GetAnimalId(AnimalData animalData)
    {
        for (int i = 0; i < animalDatas.Length; ++i)
        {
            if (animalData == animalDatas[i])
            {
                return i;
            }
        }

        return -1;
    }
}
