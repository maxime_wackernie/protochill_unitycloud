﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

public enum ECompare
{
    EQUAL,
    INFERIOR,
}

[System.Serializable]
public struct HappinessStep
{
    [Range(0f, 1f)]
    public float step;

    public Sprite emoji;
    public Color color;
    public float perc;
    public ECompare compare;
}

public class HappinessManager : Singleton<HappinessManager>
{
    [Header("Decrease")]
    [SerializeField]
    private Vector2 decreaseTimerRange = new Vector2(5f, 10f);
    [SerializeField]
    private float decreaseAmount = 2f;
    [SerializeField]
    private float upgradeHabitatAmount = 4f;

    [Header("Parameters")]
    [SerializeField]
    private float maxHappiness = 10f;
    [SerializeField]
    private float startHappiness = 5f;

    [SerializeField]
    private HappinessStep[] happinessSteps = null;

    private float averageHappiness;

    public System.Action<float> averageHappinessUpdateEvent;
    public System.Action<HappinessStep> happinessStepUpdate;

    public HappinessStep[] HappinessSteps { get { return happinessSteps; } }
    public Vector2 DecreaseTimerRange { get { return decreaseTimerRange; } }
    public float DecreaseAmount { get { return decreaseAmount; } }
    public float UpgradeHabitatAmount { get { return upgradeHabitatAmount; } }
    public float MaxHappiness { get { return maxHappiness; } }
    public float StartHappiness { get { return startHappiness; } }
    public float HappinessRatio { get { return averageHappiness / maxHappiness; } }
    public float AverageHappiness
    { 
        get { return averageHappiness; }
        set 
        {
            averageHappiness = Mathf.Clamp(value, 0, maxHappiness);

            HappinessStep step;
            if (float.IsNaN(HappinessRatio))
                return;
            else
                step = GetHappinessStep(HappinessRatio);

            IncomeManager.Instance.IncomeMultiplier = step.perc;

            if (happinessStepUpdate != null)
            {
                happinessStepUpdate.Invoke(step);
            }

            if (averageHappinessUpdateEvent != null)
            {
                averageHappinessUpdateEvent.Invoke(averageHappiness);
            }
        }
    }

    public HappinessStep CurrentHappinessStep { get { return GetHappinessStep(HappinessRatio); } }

    public void UpdateAverageHappiness()
    {
        float value = 0f;
        foreach (Animal animal in AnimalManager.Instance.Animals)
        {
            value += animal.Happiness;
        }

        AverageHappiness = value / AnimalManager.Instance.Animals.Count;
    }

    public HappinessStep GetHappinessStep(float step)
    {
        float current = 0f;
        foreach (HappinessStep happinessStep in happinessSteps)
        {
            current = happinessStep.step;

            switch (happinessStep.compare)
            {
                case ECompare.EQUAL:
                    if (Mathf.Approximately(current, step))
                    {
                        return happinessStep;
                    }
                    break;
                case ECompare.INFERIOR:
                    if (current > step)
                    {
                        return happinessStep;
                    }
                    break;
            }
        }

        Debug.LogError("Happiness step not found: " + step);
        return new HappinessStep();
    }
}

