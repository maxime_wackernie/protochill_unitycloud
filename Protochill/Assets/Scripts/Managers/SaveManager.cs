﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

[System.Serializable]
public struct SerializableVector2Int
{
    public SerializableVector2Int(Vector2Int vector2Int)
    {
        this.x = vector2Int.x;
        this.y = vector2Int.y;
    }

    public int x, y;

    public Vector2Int ToVector2Int()
    {
        return new Vector2Int(x, y);
    }
}

[System.Serializable]
public struct SerializableVector3
{
    public SerializableVector3(Vector3 vector3)
    {
        this.x = vector3.x;
        this.y = vector3.y;
        this.z = vector3.z;
    }

    public float x, y, z;

    public Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }
}

[DefaultExecutionOrder(-2)]
public class SaveManager : Singleton<SaveManager>
{
    [SerializeField]
    private bool tutorialOnStart = true;
    [SerializeField]
    private bool saveIncomeWithBonus = false;

    [SerializeField]
    private int maxOfflineMinutes = 120;
    [SerializeField]
    private int minSecondsForPopup = 30;

    [SerializeField]
    private ScriptableObject[] scriptableObjects = null;

    private bool isInitialized = false;

    private OfflineUI offlineUI;

    private Dictionary<string, GeneratorSaveData> generatorSave = new Dictionary<string, GeneratorSaveData>();
    private Dictionary<string, ScriptableObject> scriptableObjectDict = new Dictionary<string, ScriptableObject>();

    public static float offlineSeconds;
    public static float offlineIncome;
    public static Dictionary<ETileType, int> offlineReadyEggs;

    public System.Action loadCompleteEvent;

    public int MaxOfflineMinutes { get { return maxOfflineMinutes; } }
    public int MaxOfflineHours { get { return maxOfflineMinutes / 60; } }

    private void Awake()
    {
        offlineUI = FindObjectOfType<OfflineUI>();
        offlineUI.gameObject.SetActive(false);

        offlineSeconds = 0f;
        offlineIncome = 0f;
        offlineReadyEggs = new Dictionary<ETileType, int>
        {
            { ETileType.FOREST, 0 },
            { ETileType.SAND, 0 },
            { ETileType.SNOW, 0 }
        };
    }

    private void Start()
    {
        InitScriptableObjectDictionary();

        LoadIncome();
        LoadOffline();
        LoadGenerators();
        LoadIslands();
        LoadAnimals();

        if (loadCompleteEvent != null)
        {
            loadCompleteEvent.Invoke();
        }

        if (PrototypeProfile.Instance.ProfileData.hasSave)
        {
            offlineUI.Init();
        }
        else
        {
            if (tutorialOnStart)
            {
                FindObjectOfType<TutorialHandler>().StartTutorial();
            }

            PrototypeProfile.Instance.ProfileData.hasSave = true;
        }

        isInitialized = true;
    }

    private void OnApplicationQuit()
    {
        Save();
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            Save();
        }

        if (isInitialized && pause == false)
        {
            LoadFocusOn();
        }
    }

    private void LoadFocusOn()
    {
        ResetValues();

        LoadOffline();
        LoadEggs();

        if (offlineSeconds < minSecondsForPopup)
        {
            IncomeManager.Instance.PlusIncome(SaveManager.offlineIncome);
        }
        else
        {
            offlineUI.Init();
        }
    }

    public void ResetValues()
    {
        offlineReadyEggs[ETileType.FOREST] = 0;
        offlineReadyEggs[ETileType.SAND] = 0;
        offlineReadyEggs[ETileType.SNOW] = 0;

        offlineIncome = 0f;
        offlineSeconds = 0;
    }

    public void InitScriptableObjectDictionary()
    {
        foreach (ScriptableObject scriptableObject in scriptableObjects)
        {
            scriptableObjectDict.Add(scriptableObject.name, scriptableObject);
        }

        // free memory
        scriptableObjects = null;
    }

    public T GetScriptableObject<T>(string name) where T : ScriptableObject
    {
        if (scriptableObjectDict.ContainsKey(name))
        {
            return scriptableObjectDict[name] as T;
        }

        return null;
    }

    public void SaveGenerator(string key, Generator generator)
    {
        GeneratorSaveData data = new GeneratorSaveData(key, generator.Level, generator.StepLevel);
        SaveGenerator(key, data);
    }

    public void SaveGenerator(string key, GeneratorSaveData data)
    {
        if (generatorSave.ContainsKey(key) == false)
        {
            generatorSave.Add(key, data);
        }

        generatorSave[key] = data;
    }

    public GeneratorSaveData LoadGenerator(string key)
    {
        if (generatorSave.ContainsKey(key) == false)
        {
            SaveGenerator(key, new GeneratorSaveData(key, 0, 0));
        }

        return generatorSave[key];
    }

    #region Save
    private void Save()
    {
        SaveIslands();
        SaveAnimals();
        SaveGenerators();

        PrototypeProfile.Instance.ProfileData.lastDateTime = System.DateTime.Now;
        PrototypeProfile.Instance.ProfileData.income = IncomeManager.Instance.Income;
        PrototypeProfile.Instance.ProfileData.averageIncome = saveIncomeWithBonus ? IncomeManager.Instance.TotalIncomeAverage : IncomeManager.Instance.IncomeAverage;
        PrototypeProfile.Instance.ProfileData.hasSave = true;
        PrototypeProfile.Instance.Save();
    }

    private void SaveGenerators()
    {
        GeneratorSaveData[] save = new GeneratorSaveData[generatorSave.Count];

        int i = 0;
        foreach (KeyValuePair<string, GeneratorSaveData> it in generatorSave)
        {
            save[i] = new GeneratorSaveData(it.Value);
            ++i;
        }

        PrototypeProfile.Instance.ProfileData.generatorSaveDatas = save;
    }

    private void SaveIslands()
    {
        foreach (Island island in LevelManager.Instance.IslandContent.GetComponentsInChildren<Island>())
        {
            island.Save();
        }
    }

    private void SaveAnimals()
    {
        AnimalManager.Instance.Save();
    }
    #endregion

    #region Load
    private void LoadIncome()
    {
        IncomeManager.Instance.Load();
    }

    private void LoadOffline()
    {
        if (PrototypeProfile.Instance.ProfileData.hasSave)
        {
            System.DateTime now = System.DateTime.Now;
            System.TimeSpan diff = now - PrototypeProfile.Instance.ProfileData.lastDateTime;

            if (diff.TotalMinutes > maxOfflineMinutes)
            {
                diff = new System.TimeSpan(0, 0, maxOfflineMinutes, 0);
            }

            SaveManager.offlineIncome = (int)((float)diff.TotalSeconds * PrototypeProfile.Instance.ProfileData.averageIncome);
            SaveManager.offlineSeconds = (float)diff.TotalSeconds;
        }
    }

    private void LoadGenerators()
    {
        if (PrototypeProfile.Instance.ProfileData.generatorSaveDatas != null)
        {
            foreach (GeneratorSaveData data in PrototypeProfile.Instance.ProfileData.generatorSaveDatas)
            {
                generatorSave.Add(data.key, new GeneratorSaveData(data));
            }
        }
    }

    private void LoadIslands()
    {
        bool load = true;
        if (PrototypeProfile.Instance.ProfileData.islandSaves == null)
        {
            load = false;
            PrototypeProfile.Instance.ProfileData.islandSaves = new IslandSave[LevelManager.Instance.IslandContent.childCount];
        }

        foreach (Island island in LevelManager.Instance.IslandContent.GetComponentsInChildren<Island>())
        {
            if (load)
            {
                island.Load();
            }
            else
            {
                island.Init();
            }
        }
    }

    private void LoadAnimals()
    {
        AnimalManager.Instance.Load();
    }

    public void LoadEggs()
    {
        foreach (Habitat habitat in FindObjectsOfType<Habitat>())
        {
            habitat.LoadEggs();
        }
    }
    #endregion
}
