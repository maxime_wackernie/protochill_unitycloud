﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GLUnity.IOC;
using MoreMountains.NiceVibrations;

public class ClickableEgg : MonoBehaviour, IPointerClickHandler
{
    private bool isHatching;
    private Animator animator;

    private static int revealId = Animator.StringToHash("EggReveal");

    private EggHatchData eggHatchData;
    private AnimalData resAnimalData;
    private ERarity resRarity;

    [Dependency]
    private EggHandler eggHandler = null;
    [Dependency]
    private CameraController cameraController = null;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Init(EggHatchData eggHatchData, ETileType tileType)
    {
        this.Inject();

        this.eggHatchData = eggHatchData;

        transform.position = eggHatchData.habitat.transform.position + eggHandler.EggOffset;

        GetComponentInChildren<MeshRenderer>().material = eggHandler.GetEggMaterial(tileType, eggHatchData.eggRarity);

        MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (LevelManager.enableEggHatch && isHatching == false && eggHandler.IsEggHatching == false)
        {
            Hatch();
        }
    }

    private void Hatch()
    {
        resAnimalData = eggHatchData.eggData.GetRandomAnimalData(eggHatchData.eggRarity, out resRarity);

        eggHandler.StartHatch(eggHatchData, resAnimalData);

        cameraController.CenterCameraOnPosition(transform.position + (eggHandler.CameraOffset), eggHandler.CameraDistance, eggHandler.FreezeDuration);

        animator.Play(revealId);

        isHatching = true;
    }

    public void OnRevealComplete()
    {
        eggHandler.SpawnEgg(resAnimalData, resRarity, eggHatchData.habitat, transform.GetChild(0).position);

        eggHatchData.habitat.RemoveReadyEgg(eggHatchData);
        Destroy(gameObject);
    }
}
