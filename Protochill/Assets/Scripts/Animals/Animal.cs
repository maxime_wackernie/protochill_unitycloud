﻿using GLUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

[System.Serializable]
public struct AnimalSave
{
    public AnimalSave(Animal animal)
    {
        this.islandId = animal.Habitat.GetComponentInParent<Island>().IslandId;
        this.habitatGridPosition = new SerializableVector2Int(animal.Habitat.MyTile.GridPosition);
        this.position = new SerializableVector3(animal.transform.position);
        this.animalData = animal.AnimalData.name;
        this.happiness = animal.Happiness;
    }

    public int islandId;
    public SerializableVector2Int habitatGridPosition;
    public SerializableVector3 position;
    public string animalData;
    public float happiness;
}
    
public class Animal : MonoBehaviour
{
    [SerializeField]
    private Need[] needs = null; 
    [SerializeField]
    private AnimalUI animalUIPrefab = null;

    private float happiness = 10f;
    private int happinessDecreaseTimerId;
    private bool isInitialized;

    private AnimalData animalData;
    private AnimalUI animalUI;
    private NavMeshAgent navMeshAgent;
    private Habitat habitat;
    
    private NeedBlackboard needBlackboard = new NeedBlackboard();

    private Need currentNeed;
    public Need CurrentNeed
    {
        get { return currentNeed; }
        set
        {
            currentNeed = value;

            if (value != null)
            {
                value.NeedBehaviour.StartNeed(this);
            }
        }
    }

    public System.Action<float> happinessUpdateEvent;

    public Habitat Habitat { get { return habitat; } }
    public NeedBlackboard NeedBlackboard { get { return needBlackboard; } }
    public NavMeshAgent NavMeshAgent { get { return navMeshAgent; } }
    public AnimalData AnimalData { get { return animalData; } }
    public AnimalUI AnimalUI { get { return animalUI; } }
    public float Happiness
    { 
        get { return happiness; }
        set 
        {
            happiness = Mathf.Clamp(value, 0, HappinessManager.Instance.MaxHappiness);
            HappinessManager.Instance.UpdateAverageHappiness();

            if (happinessUpdateEvent != null)
            {
                happinessUpdateEvent.Invoke(happiness);
            }
        }
    }

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

 /*   private void Start()
    {
        if (AnimalManager.Instance.EnableAnimalShadows == false)
        {
            transform.GetChild(0).GetComponentInChildren<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }
    }*/

    private void OnDestroy()
    {
        if (ApplicationQuitHelper.IsQuittingApplication)
        {
            animalUI?.SetAvailable();
        }
    }

    public void Destroy()
    {
        AnimalManager.Instance.RemoveAnimal(this);
        gameObject.SetActive(false);
        isInitialized = false;
        animalUI?.SetAvailable();
    }

    private void Update()
    {
        if (currentNeed != null)
        {
            currentNeed.UpdateNeed(this);
        }
    }

    public void ResetBlackBoard()
    {
        needBlackboard.farm = null;
        needBlackboard.isEating = false;
    }

    public void UpdateHappiness()
    {
        float value = 0f;
        foreach (Need need in needs)
        {
            value += need.CurrentNeedValue;
        }

        Happiness = value / needs.Length;
    }

    public void InitFromEgg(AnimalData animalData, Habitat habitat, float stayDuration, float moveDuration, System.Action animEndCallback)
    {
        this.animalData = animalData;
        this.habitat = habitat;

        AddAnimal();

        float scale = transform.localScale.x;
        CoroutineHelper.Instance.ExecuteAfter(stayDuration, () => 
        {
            MoveToGround(habitat.MyTile.GetRandomPoint(), scale, moveDuration, animEndCallback); 
        });
    }

    public void Init(AnimalData animalData, Habitat habitat, float happiness)
    {
        this.animalData = animalData;
        this.habitat = habitat;

        AddAnimal();
        Init(happiness);
        UpdateNeeds(false, false);
    }

    private void AddAnimal()
    {
        habitat.Animals.Add(this);
        AnimalManager.Instance.AddAnimal(this);
    }

    private void Init(float happiness)
    {
        isInitialized = true;

        if (navMeshAgent.Warp(transform.position) == false)
        {
            navMeshAgent.Warp(habitat.transform.position);
        }

        navMeshAgent.enabled = true;

        InitNeeds(happiness);

        animalUI = ObjectPool.Instance.GetAvailable(animalUIPrefab) as AnimalUI;
        animalUI.Init(this);

        Vector2 range = HappinessManager.Instance.DecreaseTimerRange;
        happinessDecreaseTimerId = TimerManager.Instance.AddTimer(Random.Range(range.x, range.y), true, true, OnHappinessDecreaseTimerComplete);

        UpdateHappiness();
    }

    private void InitNeeds(float happiness)
    {
        foreach (Need need in needs)
        {
            need.Init(happiness);
        }
    }

    private void MoveToGround(Vector3 targetPos, float targetScale, float moveDuration, System.Action callback)
    {
        transform.DOScale(targetScale, moveDuration);
        transform.DORotateQuaternion(Quaternion.identity, moveDuration);
        transform.DOMove(targetPos, moveDuration).onComplete += () =>
        {
            if (callback != null)
            {
                callback.Invoke();
            }
            Init(HappinessManager.Instance.StartHappiness);
        };
    }

    public void SpawnCoin()
    {
        if (isInitialized)
        {
            PoolableObject coin = ObjectPool.Instance.GetAvailable<PoolableObject>("coin", lifeTime: 1.25f);
            coin.transform.position = transform.position;
            coin.GetComponentInChildren<Animator>().Play("CoinFeedback");
        }
    }

    public void SpawnSmiley()
    {
        if (isInitialized)
        {
            Sprite sprite;
            if (currentNeed != null)
            {
                return;
                //sprite = currentNeed.NeedBehaviour.EmoteSprite;
            }
            else
            {
                float step = happiness / HappinessManager.Instance.MaxHappiness;
                sprite = HappinessManager.Instance.GetHappinessStep(step).emoji;
            }

            SpawnEmote(sprite);
        }
    }

    public void SpawnEmote(Sprite sprite)
    {
        if (isInitialized)
        {
            animalUI.SpawnEmote(sprite, Random.Range(1f, 3f));
        }
    }

    private void OnHappinessDecreaseTimerComplete(int id)
    {
        UpdateNeeds(true, true);
    }

    private void UpdateNeeds(bool updateHappiness, bool decrease)
    {
        if (isInitialized == false)
            return;

        float value = 0f;
        foreach (Need need in needs)
        {
            value += need.CurrentNeedValue;

            if (decrease)
            {
                need.Decrease();
            }

            if (currentNeed == null && need.HasToFulfill)
            {
                CurrentNeed = need;
            }
        }

        if (updateHappiness)
        {
            Happiness = value / needs.Length;
        }
    }

    public void MoveToRandomPoint()
    {
        if (currentNeed != null || isInitialized == false)
            return;

        Vector3 randomPoint = LevelManager.Instance.GetRandomUnlockedIsland().TileMapComponent.GetRandomPointOnTileMap();
        navMeshAgent.destination = randomPoint;
    }

    #region Static methods

    public static void SpawnCoin(Animal animal)
    {
        animal.SpawnCoin();
    }

    public static void SpawnSmiley(Animal animal)
    {
        animal.SpawnSmiley();
    }

    public static void MoveToRandomPoint(Animal animal)
    {
        animal.MoveToRandomPoint();
    }
    #endregion
}
