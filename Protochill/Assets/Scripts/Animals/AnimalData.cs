﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AnimalData", menuName = "Data/Animal/AnimalData", order = 1)]
[System.Serializable]
public class AnimalData : ScriptableObject
{
    [SerializeField]
    private string animalName = null;
    
    [SerializeField]
    private Sprite animalSprite = null;

    [SerializeField]
    private Animal prefab = null;
    
    [SerializeField]
    private ProductionData animalIncome = null;

    [SerializeField]
    public ERarity rarity;

    [SerializeField]
    public EType type;

    public string AnimalName { get { return animalName; } }
    public Sprite AnimalSprite { get { return animalSprite; } }
    public Animal Prefab { get { return prefab; } }
    public ProductionData AnimalIncome { get { return animalIncome; } }
}
