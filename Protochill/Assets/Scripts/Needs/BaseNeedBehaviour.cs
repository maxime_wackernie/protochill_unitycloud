﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseNeedBehaviour : ScriptableObject
{
    [SerializeField]
    private Sprite emoteSprite = null;

    public Sprite EmoteSprite { get { return emoteSprite; } }

    public virtual void StartNeed(Animal animal)
    {
        animal.SpawnEmote(emoteSprite);
    }

    public abstract void UpdateNeed(Animal animal);

    public virtual void CompleteNeed(Animal animal)
    {
        animal.CurrentNeed.CompleteNeed(animal);
        animal.SpawnSmiley();
    }

    public virtual void NeedFailed(Animal animal)
    {
        animal.AnimalUI.OnNeedFailed();
        animal.ResetBlackBoard();

        animal.CurrentNeed = null;
    }
}
