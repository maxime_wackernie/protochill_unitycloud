﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(fileName = "FoodNeed", menuName = "Data/Need/FoodNeed", order = 1)]
public class FoodNeed : BaseNeedBehaviour
{
    public override void StartNeed(Animal animal)
    {
        base.StartNeed(animal);

        if (animal.NavMeshAgent.isOnNavMesh)
        {
            Farm farm = GetClosestAvailableFarm(animal);
            if (farm)
            {
                Transform slot;
                if (farm.EnterQueue(animal, out slot))
                {
                    animal.NavMeshAgent.destination = slot.transform.position;
                    return;
                }
            }
        }

       NeedFailed(animal);
    }

    public override void UpdateNeed(Animal animal)
    {
        if (animal.NeedBlackboard.isEating || animal.NavMeshAgent.pathPending)
            return;

        if (animal.NavMeshAgent.pathStatus == NavMeshPathStatus.PathInvalid)
        {
            NeedFailed(animal);
            return;
        }

        float dist = animal.NavMeshAgent.remainingDistance;
        if (dist != Mathf.Infinity && animal.NavMeshAgent.pathStatus == NavMeshPathStatus.PathComplete && Mathf.Approximately(animal.NavMeshAgent.remainingDistance, 0))
        {
            animal.NeedBlackboard.farm.DoEat(animal, CompleteNeed);
        }
    }

    public override void CompleteNeed(Animal animal)
    {
        base.CompleteNeed(animal);

        animal.ResetBlackBoard();

        animal.MoveToRandomPoint();
    }

    private Farm GetClosestAvailableFarm(Animal animal)
    {
        Farm result = null;

        List<Farm> availableFarms = new List<Farm>();
        foreach (Farm farm in LevelManager.Instance.Farms)
        {
            if (farm.HasAvailableSlot)
            {
                availableFarms.Add(farm);
            }
        }

        float distance;
        float closest = float.MaxValue;
        foreach (Farm availableFarm in availableFarms)
        {
            distance = Vector3.Distance(animal.transform.position, availableFarm.transform.position);
            if (distance < closest)
            {
                result = availableFarm;
                closest = distance;
            }
        }

        return result;
    }
}
