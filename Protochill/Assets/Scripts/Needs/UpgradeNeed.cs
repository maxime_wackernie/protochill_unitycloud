﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeNeed : BaseNeedBehaviour
{
    public override void StartNeed(Animal animal)
    {
    }

    public override void UpdateNeed(Animal animal)
    {
    }

    public override void CompleteNeed(Animal animal)
    {
    }

    public override void NeedFailed(Animal animal)
    {
    }
}
