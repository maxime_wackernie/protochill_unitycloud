﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedBlackboard
{
    public Farm farm;
    public bool isEating;
}

[System.Serializable]
public class Need
{
    [SerializeField]
    private BaseNeedBehaviour needBehaviour = null;

    [SerializeField]
    private float fulfillAtRatio = 0.5f;

    [SerializeField]
    private float valueDecreaseRatio = 0.1f;

    private float maxNeedValue;
    private float currentNeedValue;


    public float CurrentNeedValue
    {
        get { return currentNeedValue; }
        set
        {
            currentNeedValue = Mathf.Clamp(value, 0, maxNeedValue);
        }
    }
    public BaseNeedBehaviour NeedBehaviour { get { return needBehaviour; } }
    public bool HasToFulfill { get { return currentNeedValue <= maxNeedValue * fulfillAtRatio; } }

    public void Init(float needValue)
    {
        this.maxNeedValue = HappinessManager.Instance.MaxHappiness;
        this.currentNeedValue = needValue;
    }

    public void UpdateNeed(Animal animal)
    {
        needBehaviour.UpdateNeed(animal);
    }

    public void CompleteNeed(Animal animal)
    {
        currentNeedValue = maxNeedValue;

        animal.CurrentNeed = null;
        animal.UpdateHappiness();
    }

    public void Decrease()
    {
        CurrentNeedValue -= maxNeedValue * valueDecreaseRatio;
    }
}
