﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;
using System;

namespace GLUnity
{
    public class CoroutineHelper : Singleton<CoroutineHelper>
    {

        public delegate void CoroutineDelegate();
        public delegate bool UntilDelegate();

        private IEnumerator WaitForEndOfFrameCoroutine(CoroutineDelegate callback)
        {
            yield return new WaitForEndOfFrame();
            callback();
        }

        private IEnumerator WaitForSecsCoroutine(float secs, CoroutineDelegate callback)
        {
            yield return new WaitForSeconds(secs);
            callback();
        }

        private IEnumerator WaitForSecsRealtimeCoroutine(float secs, CoroutineDelegate callback)
        {
            yield return new WaitForSecondsRealtime(secs);
            callback();
        }

        private IEnumerator WaitUntilCoroutine(System.Func<bool> until, CoroutineDelegate callback)
        {
            yield return new WaitUntil(until);
            callback();
        }

        private IEnumerator LerpValueCoroutine(float startValue, float endValue, float duration, bool unscaledDeltaTime, Action<float> updateCallback = null, Action endCallback = null)
        {
            float time = 0;
            do
            {
                float timeCoef = time / duration;

                float currentValue = Mathf.Lerp(startValue, endValue, timeCoef);
                if (updateCallback != null)
                {
                    updateCallback(currentValue);
                }
                yield return new WaitForEndOfFrame();
                if (unscaledDeltaTime)
                    time += Time.unscaledDeltaTime;
                else
                    time += Time.deltaTime;
            }
            while (time < duration);
            if (updateCallback != null)
            {
                updateCallback(endValue);
            }
            if (endCallback != null)
            {
                endCallback();
            }
        }


        public IEnumerator ExecuteAfter(float secs, CoroutineDelegate callback, bool realTime = false)
        {
            IEnumerator coroutine;
            if (realTime)
            {
                coroutine = WaitForSecsRealtimeCoroutine(secs, callback);
            }
            else
            {
                coroutine = WaitForSecsCoroutine(secs, callback);
            }

            StartCoroutine(coroutine);
            return coroutine;
        }

        public IEnumerator WaitUntil(System.Func<bool> until, CoroutineDelegate callback)
        {
            IEnumerator coroutine = WaitUntilCoroutine(until, callback);
            StartCoroutine(coroutine);
            return coroutine;
        }

        public IEnumerator LerpValue(float startValue, float endValue, float duration, bool unscaledDeltaTime = false, Action<float> updateCallback = null, Action endCallback = null)
        {
            IEnumerator coroutine = LerpValueCoroutine(startValue, endValue, duration, unscaledDeltaTime, updateCallback, endCallback);
            StartCoroutine(coroutine);
            return coroutine;
        }

        public IEnumerator WaitForEndOfFrame(CoroutineDelegate callback)
        {
            IEnumerator coroutine = WaitForEndOfFrameCoroutine(callback);
            StartCoroutine(coroutine);
            return coroutine;
        }
    }
}