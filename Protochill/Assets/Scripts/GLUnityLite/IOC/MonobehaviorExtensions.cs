﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GLUnity.IOC
{
    public static class MonobehaviorExtensions
    {

        private static IOCContainer iocContainer = null;

        public static void SetIOCContainer(IOCContainer value)
        {
            iocContainer = value;
        }

        public static void Inject<T>(this T mb) where T : MonoBehaviour
        {
            if (iocContainer != null)
            {
                iocContainer.Inject(mb);
            }
        }

        public static void Inject(this MonoBehaviour mb, Type type)
        {
            if (iocContainer != null)
            {
                iocContainer.Inject(type, mb);
            }
        }

        public static void AutBind<T>(this T mb) where T : MonoBehaviour
        {
            if (iocContainer != null)
            {
                iocContainer.AutoBind(typeof(T), mb);
            }
        }

    }
}
