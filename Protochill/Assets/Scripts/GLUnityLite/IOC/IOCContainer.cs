﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Reflection;
namespace GLUnity.IOC
{
    public class IOCContainer
    {

        private class TypeDesc
        {
            public object Instance { get; set; }
            public List<KeyValuePair<Dependency, PropertyInfo>> Properties { get; private set; }
            public List<KeyValuePair<Dependency, FieldInfo>> Fields { get; private set; }

            private TypeDesc()
            {
                this.Properties = new List<KeyValuePair<Dependency, PropertyInfo>>();
                this.Fields = new List<KeyValuePair<Dependency, FieldInfo>>();
            }

            public static TypeDesc Create(Type type, object instance = null)
            {
                var typeData = new TypeDesc { Instance = instance };
                FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
                foreach (var field in fields)
                {
                    var dependency = (Dependency)field.GetCustomAttributes(typeof(Dependency), true).FirstOrDefault();
                    if (dependency == null)
                    {
                        continue;
                    }
                    typeData.Fields.Add(new KeyValuePair<Dependency, FieldInfo>(dependency, field));
                }

                foreach (var property in type.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy))
                {
                    var dependency = (Dependency)property.GetCustomAttributes(typeof(Dependency), true).FirstOrDefault();
                    if (dependency == null)
                    {
                        continue;
                    }
                    typeData.Properties.Add(new KeyValuePair<Dependency, PropertyInfo>(dependency, property));
                }
                return typeData;
            }


        }

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private Dictionary<Type, TypeDesc> typeDescs = new Dictionary<Type, TypeDesc>();
        private Dictionary<Type, List<object>> pendingDependencies = new Dictionary<Type, List<object>>();

        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public void AutoBind(Type type, object instance)
        {
            var iocExpose = type.GetCustomAttributes(typeof(IOCExpose), true).FirstOrDefault() as IOCExpose;
            if(iocExpose != null)
            {
                Bind(type, instance);
            }
        }

        public void Bind<T>(T instance)
        {
            Type type = typeof(T);
            Bind(type, instance);
        }

        public void Bind(Type type, object instance)
        {
            if (typeDescs.ContainsKey(type))
            {
                return;
            }
            typeDescs[type] = TypeDesc.Create(type, instance);

            List<object> objs;
            if (pendingDependencies.TryGetValue(type, out objs))
            {
                foreach(object obj in objs)
                {
                    Inject(obj.GetType(), obj);
                }
                pendingDependencies.Remove(type);
            }
        }

        public object Resolve(Type type, object obj)
        {
            if (!typeDescs.ContainsKey(type))
            {
                List<object> objs;
                if (pendingDependencies.TryGetValue(type, out objs))
                {
                    if (!objs.Contains(obj))
                        objs.Add(obj);
                }
                else
                {
                    objs = new List<object>();
                    objs.Add(obj);
                    pendingDependencies[type] = objs;
                }
                return null;
            }
            return typeDescs[type].Instance;
        }

        public void Inject<T>(T injectTo)
        {
            Type type = injectTo.GetType();
            Inject(type, injectTo);
        }

        public void Inject(Type type, object injectTo)
        {
            TypeDesc typeDesc = null;
            if (!typeDescs.ContainsKey(type))
            {
                typeDesc = TypeDesc.Create(type);
                if(typeDesc == null)
                {
                    return;
                }
                Bind(type, null);
            }
            typeDesc = typeDescs[type];
            foreach (var pair in typeDesc.Fields)
            {
                pair.Value.SetValue(injectTo, Resolve(pair.Value.FieldType, injectTo));
            }
            foreach (var pair in typeDesc.Properties)
            {
                pair.Value.SetValue(injectTo, Resolve(pair.Value.PropertyType, injectTo), null);
            }
        }

        #endregion
    }
}