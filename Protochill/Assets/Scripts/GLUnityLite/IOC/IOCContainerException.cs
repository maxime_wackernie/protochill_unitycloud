﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GLUnity.IOC
{

    public class IOCContainerException : Exception
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        public IOCContainerException(string message) : base(message)
        {

        }
        #endregion

        #region private methods
        #endregion

        #region protected methods
        #endregion

        #region public methods
        #endregion
    }
}
