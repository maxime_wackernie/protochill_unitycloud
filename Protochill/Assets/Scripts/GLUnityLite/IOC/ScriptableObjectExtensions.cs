﻿using System.Collections.Generic;
using UnityEngine;
namespace GLUnity.IOC
{
    public static class ScriptableObjectExtensions
    {

        private static IOCContainer iocContainer = null;

        public static void SetIOCContainer(IOCContainer value)
        {
            iocContainer = value;
        }

        public static void InjectToScriptable<T>(this T mb) where T : ScriptableObject
        {
            if (iocContainer != null)
            {
                iocContainer.Inject(mb);
            }
        }


    }
}
