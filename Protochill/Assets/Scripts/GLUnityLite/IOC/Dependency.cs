﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace GLUnity.IOC
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class Dependency : Attribute
    {

        public Dependency()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class IOCExpose : Attribute
    {
        public IOCExpose()
        {
        }
    }
}
