﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GLUnity.IOC
{
    public static class Bootstrapper
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        static IOCContainer container;
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void FeedDependencies()
        {
            container = new IOCContainer();
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        }

        private static void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            container.Bind<IOCContainer>(container);
            MonobehaviorExtensions.SetIOCContainer(container);
            ScriptableObjectExtensions.SetIOCContainer(container);
            DoAutoBind(container, arg0);
            DoInject(container, arg0);

        }

        private static void DoAutoBind(IOCContainer container, Scene scene)
        {
            var roots = scene.GetRootGameObjects();
            foreach (var root in roots)
            {
                var behaviors = root.GetComponentsInChildren<MonoBehaviour>();
                foreach (var behavior in behaviors)
                {
                    if (behavior != null)
                    {
                        var type = behavior.GetType();
                        container.AutoBind(type, behavior);
                    }
                }

            }
        }

        private static void DoInject(IOCContainer container, Scene scene)
        {
            var roots = scene.GetRootGameObjects();
            foreach (var root in roots)
            {
                var behaviors = root.GetComponentsInChildren<MonoBehaviour>();
                foreach (var behavior in behaviors)
                {
                    if (behavior != null && behavior.gameObject.activeInHierarchy)
                    {
                        var type = behavior.GetType();
                        container.Inject(type, behavior);
                    }
                }

            }
        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        #endregion
    }

}
