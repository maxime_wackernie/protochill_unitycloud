﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GLUnity
{
    public static class TransformUtils
    {

        public static void AddPosition(this Transform tr, Vector3 position)
        {
            tr.position = tr.position + position;
        }

        public static void AddPosition(this Transform tr, float x, float y, float z)
        {
            AddPosition(tr, new Vector3(x, y, z));
        }

        public static void AddPositionX(this Transform tr, float x)
        {
            AddPosition(tr, x, 0.0f, 0.0f);
        }

        public static void AddPositionY(this Transform tr, float y)
        {
            AddPosition(tr, 0.0f, y, 0.0f);
        }

        public static void AddPositionZ(this Transform tr, float z)
        {
            AddPosition(tr, 0.0f, 0.0f, z);
        }

        public static void AddLocalPosition(this Transform tr, Vector3 position)
        {
            tr.localPosition = tr.localPosition + position;
        }

        public static void AddLocalPosition(this Transform tr, float x, float y, float z)
        {
            AddLocalPosition(tr, new Vector3(x, y, z));
        }

        public static void AddLocalPositionX(this Transform tr, float x)
        {
            AddLocalPosition(tr, x, 0.0f, 0.0f);
        }

        public static void AddLocalPositionY(this Transform tr, float y)
        {
            AddLocalPosition(tr, 0.0f, y, 0.0f);
        }

        public static void AddLocalPositionZ(this Transform tr, float z)
        {
            AddLocalPosition(tr, 0.0f, 0.0f, z);
        }

        public static void SetPosition(this Transform tr, float x, float y, float z)
        {
            tr.position = new Vector3(x, y, z);
        }

        public static void SetPositionX(this Transform tr, float x)
        {
            tr.position = new Vector3(x, tr.position.y, tr.position.z);
        }

        public static void SetPositionY(this Transform tr, float y)
        {
            tr.position = new Vector3(tr.position.x, y, tr.position.z);
        }

        public static void SetPositionZ(this Transform tr, float z)
        {
            tr.position = new Vector3(tr.position.x, tr.position.y, z);
        }

        public static void SetLocalPosition(this Transform tr, float x, float y, float z)
        {
            tr.localPosition = new Vector3(x, y, z);
        }

        public static void SetLocalPositionX(this Transform tr, float x)
        {
            tr.localPosition = new Vector3(x, tr.localPosition.y, tr.localPosition.z);
        }

        public static void SetLocalPositionY(this Transform tr, float y)
        {
            tr.localPosition = new Vector3(tr.localPosition.x, y, tr.localPosition.z);
        }

        public static void SetLocalPositionZ(this Transform tr, float z)
        {
            tr.localPosition = new Vector3(tr.localPosition.x, tr.localPosition.y, z);
        }

        public static void SetLocalScale(this Transform tr, Vector3 scale)
        {
            tr.localScale = scale;
        }

        public static void SetLocalScale(this Transform tr, float x, float y, float z)
        {
            tr.localScale = new Vector3(x, y, z);
        }

        public static void SetLocalScaleX(this Transform tr, float x)
        {
            tr.localScale = new Vector3(x, tr.localScale.y, tr.localScale.z);
        }

        public static void SetLocalScaleY(this Transform tr, float y)
        {
            tr.localScale = new Vector3(tr.localScale.x, y, tr.localScale.z);
        }

        public static void SetLocalScaleZ(this Transform tr, float z)
        {
            tr.localScale = new Vector3(tr.localScale.x, tr.localScale.y, z);
        }

        public static void SetGlobalScale(this Transform transform, Vector3 globalScale)
        {
            transform.localScale = Vector3.one;
            transform.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, globalScale.z / transform.lossyScale.z);
        }

        public static void SetLocalRotationX(this Transform tr, float x)
        {
            tr.localRotation = Quaternion.Euler(x, tr.localEulerAngles.y, tr.localEulerAngles.z);
        }

        public static void SetLocalRotationY(this Transform tr, float y)
        {
            tr.localRotation = Quaternion.Euler(tr.localEulerAngles.x, y, tr.localEulerAngles.z);
        }

        public static void SetLocalRotationZ(this Transform tr, float z)
        {
            tr.localRotation = Quaternion.Euler(tr.localEulerAngles.x, tr.localEulerAngles.y, z);
        }

        public static Vector2 WorldPosToCanvasPos(Transform worldTransform, RectTransform rectTransform)
        {
            return WorldPosToCanvasPos(worldTransform, rectTransform, Vector3.zero);
        }

        public static Vector2 WorldPosToCanvasPos(Transform worldTransform, RectTransform rectTransform, Vector3 offset)
        {
            Vector2 viewportPosition = Camera.main.WorldToViewportPoint(worldTransform.position + offset);
            Canvas canvas = rectTransform.GetComponentInParent<Canvas>();
            RectTransform canvasRect = canvas.GetComponent<RectTransform>();

            Vector2 proportionalPosition = new Vector2(viewportPosition.x * canvasRect.sizeDelta.x, viewportPosition.y * canvasRect.sizeDelta.y);
            Vector2 uiOffset = new Vector2(canvasRect.sizeDelta.x * 0.5f, canvasRect.sizeDelta.y * 0.5f);
            return proportionalPosition - uiOffset;
        }

        public static Vector2 WorldPosToCanvasPos(Vector3 position, Vector3 offset, Camera camera, RectTransform canvasRect)
        {
            Vector2 viewportPosition = camera.WorldToViewportPoint(position + offset);
            Vector2 proportionalPosition = new Vector2(viewportPosition.x * canvasRect.sizeDelta.x, viewportPosition.y * canvasRect.sizeDelta.y);
            Vector2 uiOffset = new Vector2(canvasRect.sizeDelta.x * 0.5f, canvasRect.sizeDelta.y * 0.5f);

            return proportionalPosition - uiOffset;
        }

        public static bool CanBeSeen(Collider collider)
        {
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
            if (GeometryUtility.TestPlanesAABB(planes, collider.bounds))
            {
                return true;
            }
            return false;
        }
    }

}
