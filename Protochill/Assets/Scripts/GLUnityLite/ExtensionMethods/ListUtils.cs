﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListUtils {
    public static T Pop<T>(this List<T> list)
    {
        if (list.Count == 0)
        {
            throw new System.Exception("can't pop since list is empty");
        }
        var item = list[0];
        list.RemoveAt(0);
        return item;
    }
}
