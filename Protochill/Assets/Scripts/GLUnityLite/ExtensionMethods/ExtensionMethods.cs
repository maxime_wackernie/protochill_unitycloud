﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using GLUnity.IOC;

namespace GLUnity
{
    public static class ExtensionMethods
    {

        // Récupère la position du touch par rapport au ratio de la width de l'écran
        public static Vector2 GetTouchPosition(this Touch touch)
        {
            return touch.position / Screen.height;
        }

        // Récupère le delta position du touch par rapport au ratio de la width de l'écran
        public static Vector2 GetTouchDeltaPosition(this Touch touch)
        {
            return touch.deltaPosition / Screen.height;
        }

        // Rotation 2D en float
        public static float GetRotation2D(this Transform t)
        {
            return t.rotation.eulerAngles.z;
        }

        public static void SetRotation2D(this Transform t, float _z)
        {
            Vector3 rot = t.rotation.eulerAngles;
            rot.z = _z;
            t.rotation = Quaternion.Euler(rot);
            return;
        }

        public static void DrawPoint(Vector3 position, float size, Color color, float _duration)
        {
            Debug.DrawLine(position + Vector3.right * size / 2, position - Vector3.right * size / 2, color, _duration);
            Debug.DrawLine(position + Vector3.up * size / 2, position - Vector3.up * size / 2, color, _duration);
            Debug.DrawLine(position + Vector3.forward * size / 2, position - Vector3.forward * size / 2, color, _duration);
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {                
                int k = Random.Range(0, n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static T RandomItem<T>(this IList<T> list)
        {
            if(list.Count == 0)
            {
                return default(T);
            }
            return list[Random.Range(0, list.Count)];
        }

        public static List<T> RandomItems<T>(this List<T> list, int count)
        {
            List<T> l = new List<T>();
            while(l.Count < count && l.Count < list.Count)
            {
                T v = list.RandomItem<T>();
                if (!l.Contains(v))
                {
                    l.Add(v);
                }
            }
            return l;
        }

        public static T Instantiate<T>(this T original, Transform parent) where T : Object
        {
            var instantiated = Object.Instantiate(original, parent);
            var mono = instantiated as MonoBehaviour;
            if(mono != null)
            {
                mono.Inject();
                var children = mono.GetComponentsInChildren<MonoBehaviour>();
                foreach(var child in children)
                {
                    child.Inject(child.GetType());
                }
            }
            return instantiated;
        }

        public static T Instantiate<T>(this T original) where T : Object
        {
            return Instantiate(original, null);
        }

    }

    public static class DrawArrow
    {
        public static void ForGizmo(Vector3 pos, Vector3 direction, float arrowLength = 1, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
        {
            direction *= arrowLength;
            Gizmos.DrawRay(pos, direction);


            Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Vector3 right2 = Quaternion.LookRotation(direction) * Quaternion.Euler(180 + arrowHeadAngle, 0, 0) * new Vector3(0, 0, 1);
            Vector3 left2 = Quaternion.LookRotation(direction) * Quaternion.Euler(180 - arrowHeadAngle, 0, 0) * new Vector3(0, 0, 1);
            Gizmos.DrawRay(pos + direction, right * arrowHeadLength);
            Gizmos.DrawRay(pos + direction, left * arrowHeadLength);
            Gizmos.DrawRay(pos + direction, right2 * arrowHeadLength);
            Gizmos.DrawRay(pos + direction, left2 * arrowHeadLength);

        }

        public static void ForGizmo(Vector3 pos, Vector3 direction, Color color, float arrowLength = 1, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
        {
            direction *= arrowLength;
            Gizmos.color = color;
            Gizmos.DrawRay(pos, direction);

            Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Vector3 right2 = Quaternion.LookRotation(direction) * Quaternion.Euler(180 + arrowHeadAngle, 0, 0) * new Vector3(0, 0, 1);
            Vector3 left2 = Quaternion.LookRotation(direction) * Quaternion.Euler(180 - arrowHeadAngle, 0, 0) * new Vector3(0, 0, 1);
            Gizmos.DrawRay(pos + direction, right * arrowHeadLength);
            Gizmos.DrawRay(pos + direction, left * arrowHeadLength);
            Gizmos.DrawRay(pos + direction, right2 * arrowHeadLength);
            Gizmos.DrawRay(pos + direction, left2 * arrowHeadLength);
        }

        public static void ForDebug(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
        {
            Debug.DrawRay(pos, direction);

            Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Debug.DrawRay(pos + direction, right * arrowHeadLength);
            Debug.DrawRay(pos + direction, left * arrowHeadLength);
        }
        public static void ForDebug(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
        {
            Debug.DrawRay(pos, direction, color);

            Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Debug.DrawRay(pos + direction, right * arrowHeadLength, color);
            Debug.DrawRay(pos + direction, left * arrowHeadLength, color);
        }
    }
}

