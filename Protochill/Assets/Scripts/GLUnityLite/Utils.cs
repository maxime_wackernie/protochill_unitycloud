﻿using UnityEngine;
using System.Collections;

namespace GLUnity
{
    public class Utils
    {
        public static void DrawGizmoString(string text, Vector3 worldPos, Color? colour = null)
        {
#if UNITY_EDITOR
        UnityEditor.Handles.BeginGUI();
        var restoreColor = GUI.color;
        if (colour.HasValue) GUI.color = colour.Value;
        UnityEditor.SceneView view = UnityEditor.SceneView.currentDrawingSceneView;
        if (view == null)
        {
            return;
        }
        Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);
        Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
        GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y), text);
        GUI.color = restoreColor;
        UnityEditor.Handles.EndGUI();
#endif
        }

        public static T CopyComponent<T>(T original, GameObject destination) where T : Component
        {
            System.Type type = original.GetType();
            var dst = destination.AddComponent(type) as T;
            var fields = type.GetFields();
            foreach (var field in fields)
            {
                if (field.IsStatic)
                {
                    continue;
                }
                field.SetValue(dst, field.GetValue(original));
            }
            var props = type.GetProperties();
            foreach (var prop in props)
            {
                if (!prop.CanWrite || !prop.CanWrite || prop.Name == "name")
                {
                    continue;
                }
                prop.SetValue(dst, prop.GetValue(original, null), null);
            }
            return dst as T;
        }
    }
}

