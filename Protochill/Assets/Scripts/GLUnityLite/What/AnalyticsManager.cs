﻿
using System.Collections.Generic;


using UnityEngine;
using UnityEngine.Analytics;


namespace GLUnity.What
{
    public class AnalyticsManager : MonoBehaviour
    {
        //EVENTS
        protected const string END_GAME_EVENT = "end_game";
        protected const string PAUSE_CLICKED_EVENT = "pause_clicked";
        protected const string RESUME_CLICKED_EVENT = "resume_clicked";


        //PARAMS
        protected const string SCORE_PARAM = "score";
        protected const string AVG_FPS_PARAM = "avg_fps";
        protected const string SESSION_DURATION_PARAM = "session_duration";
        protected const string VERSION_PARAM = "version";


        private float startTime = -1;

        [SerializeField]
        private string version = "1.0.0";

        public void StartGameSession()
        {
            startTime = Time.time;
            Debug.Log("Analytics manager start session startTime " + Time.time.ToString());
        }

        public void EndGameSession(double score, float avgFps)
        {
            if (startTime < 0)
            {
                throw new System.Exception("Can't end session since StartGameSession hasn't been called");
            }
            var sessionTime = Time.time - startTime;
            IDictionary <string, object> dict = new Dictionary<string, object>
            {
                { SESSION_DURATION_PARAM, sessionTime},
                { SCORE_PARAM, score },
                { AVG_FPS_PARAM, avgFps }
            };
            LogEvent(END_GAME_EVENT, dict);
            startTime = -1;
        }

        public void LogPause()
        {
            IDictionary<string, object> dict = new Dictionary<string, object>
            {
            };
            LogEvent(PAUSE_CLICKED_EVENT, dict);
        }

        public void LogResume()
        {
            IDictionary<string, object> dict = new Dictionary<string, object>
            {
            };
            LogEvent(RESUME_CLICKED_EVENT, dict);
        }

        protected void LogEvent(string eventName, IDictionary<string, object> values)
        {
            values.Add(VersionKeyValue());
            Analytics.CustomEvent(eventName, values);
            string debugStr = "event : " + eventName + "\n";
            foreach (KeyValuePair<string, object> kvp in values)
            {
                debugStr += string.Format("[{0}] = {1}\n", kvp.Key, kvp.Value);
            }
            Debug.Log(debugStr);
        }

        private KeyValuePair<string, object> VersionKeyValue()
        {
            return new KeyValuePair<string, object>(VERSION_PARAM, version);
        }

    }
}
