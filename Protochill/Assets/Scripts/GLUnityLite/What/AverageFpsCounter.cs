﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace GLUnity.What
{
    public class AverageFpsCounter : MonoBehaviour
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        [SerializeField]
        private int maxFpsCount = 30;
        #endregion

        #region private variables
        private float totalTime = 0;
        private int framesCount = 0;
        private float avgFps;
        private bool started = false;
        #endregion

        #region protected variables
        #endregion

        #region properties
        public float AvgFPS
        {
            get
            {
                return avgFps;
            }
        }
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private void Update()
        {
            if (!started)
            {
                return;
            }
            ++framesCount;
            totalTime += Time.deltaTime;
            avgFps = 1.0f / (float)(totalTime / framesCount);
            if (framesCount >= maxFpsCount)
            {
                framesCount = 0;
                totalTime = 0;
            }
        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public void DoStart()
        {
            started = true;
        }

        public void DoStop()
        {
            started = false;
        }
        #endregion
    }
}
