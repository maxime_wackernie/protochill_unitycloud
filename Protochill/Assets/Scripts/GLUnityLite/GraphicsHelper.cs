﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
namespace GLUnity
{
    public static class GraphicsHelper
    {

        public static bool IsPointerOverUIObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        public static Vector3 CanvasToWorldPoint(Transform tr)
        {
            var worldPos = Vector3.zero;
            var canvas = tr.GetComponentInParent<Canvas>();
            if (canvas == null)
            {
                return Vector3.zero;
            }
            var rectT = canvas.GetComponent<RectTransform>();
            var point = RectTransformUtility.PixelAdjustPoint(tr.position, tr, canvas);
            RectTransformUtility.ScreenPointToWorldPointInRectangle(rectT, point, Camera.main, out worldPos);
            return worldPos;
        }

        public static Vector3 WorldToCanvasPoint(Transform tr)
        {
            return WorldToCanvasPoint(tr.position);
        }

        public static Vector3 WorldToCanvasPoint(Vector3 point)
        {
            return RectTransformUtility.WorldToScreenPoint(Camera.main, point);
        }

        public static int GetLayerMask(string layerName)
        {
            return 1 << LayerMask.NameToLayer(layerName);
        }
    }
}
