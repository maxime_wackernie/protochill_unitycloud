﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GLUnity
{
    public class Cooldown
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private float elapsedTime;
        private float time;
        private float prevTime;
        private bool isStarted = false;
        private bool isPaused = false;
        private bool reachedCooldown = false;
        #endregion

        #region protected variables
        #endregion

        #region properties
        public float Coef
        {
            get
            {
                if (IsAvailable())
                {
                    return 1.0f;
                }
                return elapsedTime / time;
            }
        }

        public float TotalTime
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }

        #endregion

        #region events
        public delegate void CooldownDelegate(Cooldown c);
        public event CooldownDelegate OnCooldown;
        #endregion

        #region constructor
        public Cooldown(float time)
        {
            this.time = time;
            elapsedTime = 0.0f;
        }
        #endregion

        #region private methods
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public void Start()
        {
            elapsedTime = 0.0f;
            prevTime = Time.time;
            isStarted = true;
            isPaused = false;
            reachedCooldown = false;
        }

        public void Pause()
        {
            if (isPaused) return;
            isPaused = true;
        }

        public void Unpause()
        {
            if (!isPaused) return;
            isPaused = false;
        }

        public void Stop()
        {
            elapsedTime = 0.0f;
            prevTime = Time.time;
            isStarted = false;
            isPaused = false;
            reachedCooldown = false;
        }

        public void Update()
        {
            if (isPaused)
            {
                return;
            }
            if(!isStarted)
            {
                return;
            }
            float deltaTime = Time.time - prevTime;
            elapsedTime += deltaTime;
            prevTime = Time.time;
            if (IsAvailable())
            {
                isStarted = false;
                if(!reachedCooldown)
                {
                    if (OnCooldown != null)
                    {
                        OnCooldown(this);
                    }
                    reachedCooldown = true;
                }
            }
        }

        public bool IsAvailable()
        {
            if (!isStarted)
            {
                return true;
            }
            return elapsedTime > time;
        }
        #endregion
    }

}
