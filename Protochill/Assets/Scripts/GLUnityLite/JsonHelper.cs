﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
namespace GLUnity
{
    public class JsonHelper
    {

        public static T[] GetJsonArray<T>(string json)
        {
            string newJson = "{ \"array\": " + json + "}";
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
            return wrapper.array;
        }

        static public T GetJsonObject<T>(string jsonStr)
        {
            if (jsonStr == "Null")
            {
                return default(T);
            }
            T json = JsonUtility.FromJson<T>(jsonStr);
            return json;
        }

        [System.Serializable]
        private class Wrapper<T>
        {
            public T[] array = null;
        }
    }
}