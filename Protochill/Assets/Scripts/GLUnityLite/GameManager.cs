﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity.IOC;


namespace GLUnity
{

    [DefaultExecutionOrder(10000)]
    [IOCExpose]
    public class GameManager : MonoBehaviour
    {
        public enum GAME_STATE
        {
            INTRO,
            START_GAME,
            PLAYING,
            GAME_OVER,
            GAME_OVER_WON
        }

        public delegate void StateDelegate(GAME_STATE state);
        public event StateDelegate stateChangedEvent = null;
        public event StateDelegate stateLeftEvent = null;

        
        [SerializeField]
        private GAME_STATE state = GAME_STATE.INTRO;

        public GAME_STATE State
        {
            get
            {
                return state;
            }
            set
            {
                if (value == state)
                {
                    return;
                }
                stateLeftEvent?.Invoke(state);
                state = value;
                stateChangedEvent?.Invoke(state);
            }
        }

        public int Order
        {
            get
            {
                return 0;
            }
        }

        public void PauseGame()
        {
            Time.timeScale = 0f;
        }

        public void ResumeGame()
        {
            Time.timeScale = 1f;
        }

        protected virtual void Awake()
        {
            Application.targetFrameRate = 60;
        }

        protected virtual void Start()
        {
            stateChangedEvent?.Invoke(state);
        }


        [ContextMenu("StartGame")]
        private void DebugStartGame()
        {
            State = GAME_STATE.START_GAME;
        }

        [ContextMenu("Playing")]
        private void DebugPlaying()
        {
            State = GAME_STATE.PLAYING;
        }

        [ContextMenu("Lose")]
        private void DebugLose()
        {
            State = GAME_STATE.GAME_OVER;
        }

        [ContextMenu("Win")]
        private void DebugWin()
        {
            State = GAME_STATE.GAME_OVER_WON;
        }

        [ContextMenu("Intro")]
        private void DebugIntro()
        {
            State = GAME_STATE.INTRO;
        }
    }
}