﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity
{
    public class SoundManager : Singleton<SoundManager>
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        [SerializeField]
        private List<AudioSource> soundChannels = new List<AudioSource>();
        [SerializeField]
        private AudioSource musicChannel = null;

        #endregion

        #region private variables
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private void Awake()
        {
            if(soundChannels.Count == 0)
            {
                AudioSource aS = gameObject.AddComponent<AudioSource>();
                soundChannels.Add(aS);
            }
            if(musicChannel == null)
            {
                musicChannel = gameObject.AddComponent<AudioSource>();
            }
        }

        private void CheckChannel(int channel)
        {
            if (channel < 0 || channel >= soundChannels.Count)
            {
                throw new System.Exception($"channel not found {channel}");
            }
        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public void PlaySound(AudioClip clip, int channel = 0, bool loop = false, float pitch = 1.0f)
        {
            if(clip == null)
            {
                throw new System.Exception("missing clip");
            }
            CheckChannel(channel);
            AudioSource source = soundChannels[channel];
            source.clip = clip;
            source.loop = loop;
            source.pitch = pitch;
            source.Play();
        }

        public void PlayMusic(AudioClip clip, float pitch = 1.0f)
        {
            musicChannel.clip = clip;
            musicChannel.loop = true;
            musicChannel.pitch = pitch;
            musicChannel.Play();
        }

        public void StopAllSoundChannels()
        {
            foreach(AudioSource a in soundChannels)
            {
                a.Stop();
            }
        }

        public void StopSoundChannel(int channel = 0)
        {
            CheckChannel(channel);
            soundChannels[channel].Stop();
        }

        public void StopMusic()
        {
            musicChannel.Stop();
        }

        public void SetSoundActive(bool active)
        {
            foreach (AudioSource a in soundChannels)
            {
                a.volume = active ? 1.0f : 0.0f;
            }
        }

        public void SetMusicActive(bool active)
        {
            musicChannel.volume = active ? 1.0f : 0.0f;
        }

        public T AddMusicFilter<T>() where T : Behaviour
        {
            return musicChannel.gameObject.AddComponent<T>();
        }

        public void RemoveMusicFilter<T>() where T : Behaviour
        {
            var filter = musicChannel.gameObject.GetComponent<T>();
            if (filter != null)
            {
                Destroy(filter);
            }
        }

        public T AddSoundFilterToChannel<T>(int channel = 0) where T : Behaviour
        {
            CheckChannel(channel);
            return soundChannels[channel].gameObject.AddComponent<T>();
        }

        public void RemoveSoundFilterAtChannel<T>(int channel = 0) where T : Behaviour
        {
            CheckChannel(channel);
            var filter = soundChannels[channel].gameObject.GetComponent<T>();
            if(filter != null)
            {
                Destroy(filter);
            }
        }

        public void RemoveFilterFromAllChannels<T>() where T : Behaviour
        {
            for(int i = 0; i < soundChannels.Count; ++i)
            {
                RemoveSoundFilterAtChannel<T>(i);
            }
        }

        public void AddChannel()
        {
            AudioSource aS = gameObject.AddComponent<AudioSource>();
            soundChannels.Add(aS);
        }
        #endregion
    }

}

