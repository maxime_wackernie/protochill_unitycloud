﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GLUnity.Tweens
{
    public class ColorComponent
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private Color matColor;
        
        private Renderer renderer;
        private SpriteRenderer spriteRenderer;
        #endregion

        #region protected variables
        protected GameObject toAlpha;
        protected Color destColor;
        protected Color startColor;
        protected EaseFunction.EaseFunc easeFunc;
        #endregion

        #region events
        #endregion

        #region constructor
        public ColorComponent(EaseFunction.EaseFunc easeFunc, GameObject toAlpha, Color destColor)
        {
            this.toAlpha = toAlpha;
            this.destColor = destColor;
            this.easeFunc = easeFunc;
            renderer = toAlpha.GetComponent<MeshRenderer>();
            spriteRenderer = toAlpha.GetComponent<SpriteRenderer>();

            UpdateMatColor();

        }

        private void UpdateMatColor()
        {
            if (renderer != null && renderer.material.HasProperty("_Color"))
            {
                startColor = renderer.material.color;
            }
            if (spriteRenderer != null)
            {
                startColor = spriteRenderer.color;
            }
            matColor = startColor;
        }
        #endregion

        #region private methods
        #endregion

        #region public methods
        public virtual void SetColorCoef(float coef)
        {
            matColor.a = easeFunc(startColor.a, destColor.a, coef);
            matColor.r = easeFunc(startColor.r, destColor.r, coef);
            matColor.g = easeFunc(startColor.g, destColor.g, coef);
            matColor.b = easeFunc(startColor.b, destColor.b, coef);
            if(renderer != null)
            {
                renderer.material.color = matColor;
            }
            if(spriteRenderer != null)
            {
                spriteRenderer.color = matColor;
            }

        }

        public virtual void SetColor(Color color)
        {
            matColor = color;
            destColor = color;
            if(renderer != null)
            {
                renderer.material.color = matColor;
            }
            if(spriteRenderer != null)
            {
                spriteRenderer.color = matColor;
            }
        }

        public virtual bool IsValid()
        {
            return renderer != null || spriteRenderer != null;
        }
        #endregion
    }
}
