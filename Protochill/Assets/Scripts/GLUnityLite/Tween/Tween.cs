﻿using UnityEngine;
using System.Collections;

namespace GLUnity.Tweens
{
    public abstract class Tween
    {
        #region private variables
        private bool isFinished = false;
        private bool isStarted = false;
        protected float delay;
        private float tweenCoef = 0.0f;
        private float tweenTime = 0.0f;
        #endregion

        #region protected variables
        protected readonly GameObject toTween;
        protected readonly float duration;
        protected readonly EaseFunction.EaseFunc easeFunc;
        #endregion

        #region events
        public delegate void TweenFinishedDelegate(Tween t);
        public event TweenFinishedDelegate OnTweenFinished;

        public delegate void TweenStartedDelegate(Tween t);
        public event TweenStartedDelegate OnTweenStarted;
        #endregion

        #region properties
        public GameObject ToTween
        {
            get
            {
                return toTween;
            }
        }

        protected float TweenCoef
        {
            get
            {
                return tweenCoef;
            }
        }

        public float Duration
        {
            get
            {
                return duration;
            }
        }

        public float Delay
        {
            get
            {
                return delay;
            }
        }
        #endregion

        #region CTor/Dtor
        public Tween(EaseFunction.EaseFunc easeFunc, GameObject toTween, float duration, float delay)
        {
            this.toTween = toTween;
            this.duration = duration;
            this.delay = delay;
            this.easeFunc = easeFunc;
            tweenCoef = 0.0f;
            tweenTime = 0.0f;
        }
        #endregion

        #region protected methods

        protected void UpdateTweenCoef()
        {
            tweenTime += Time.deltaTime;
            tweenCoef = Mathf.Clamp01(tweenTime / duration);
        }

        protected bool HasTweenFinished()
        {
            return tweenCoef >= 1.0f;
        }

        protected abstract void DoUpdate();

        protected abstract void DoStart();
        #endregion

        #region public methods
        public void Update()
        {
            //if(toTween == null)
            //{
            //    FinishTween();
            //    return;
            //}
            delay -= Time.deltaTime;
            if (delay <= 0.0f)
            {
                if (!isStarted)
                {
                    DoStart();
                    if (OnTweenStarted != null)
                    {
                        OnTweenStarted(this);
                    }
                    isStarted = true;
                }
                DoUpdate();
            }
        }

        public bool IsFinished()
        {
            return isFinished;
        }

        public void FinishTween()
        {
            if (isFinished)
            {
                return;
            }
            if (OnTweenFinished != null)
            {
                OnTweenFinished(this);
            }

            isFinished = true;
        }

        #endregion

    }
}
