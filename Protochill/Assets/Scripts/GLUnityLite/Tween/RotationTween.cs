﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace GLUnity.Tweens
{
    public class RotationTween : Tween
    {
        #region private variables
        private Quaternion start;
        private Quaternion destination;
        private bool localRotation;
        #endregion

        #region CTOR/DTOR

        public RotationTween(EaseFunction.EaseFunc easeFunc, GameObject toTween, Quaternion destination, float duration, float delay = 0f, bool localRotation = false): base(easeFunc, toTween, duration, delay)
        {
            this.localRotation = localRotation;
            this.destination = destination;
        }
        #endregion

        #region public methods
        #endregion

        #region protected methods
        protected override void DoStart()
        {
            start = localRotation ? toTween.transform.localRotation : toTween.transform.rotation;
        }

        protected override void DoUpdate()
        {
            if (IsFinished())
            {
                return;
            }

            Quaternion result = new Quaternion();
            result = Quaternion.Lerp(start, destination, TweenCoef);
            //result.x = easeFunc(start.x, destination.x, TweenCoef);
            //result.y = easeFunc(start.y, destination.y, TweenCoef);
            //result.z = easeFunc(start.z, destination.z, TweenCoef);
            //result.w = easeFunc(start.z, destination.z, TweenCoef);
            if (localRotation)
            {
                toTween.transform.localRotation = result;
            }
            else
            {
                toTween.transform.rotation = result;
            }
            UpdateTweenCoef();
            if (HasTweenFinished())
            {
                if (localRotation)
                {
                    toTween.transform.localRotation = destination;
                }
                else
                {
                    toTween.transform.rotation = destination;
                }
                FinishTween();
            }
        }
        #endregion
    }
}
