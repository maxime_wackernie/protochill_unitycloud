﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.Tweens
{
    public class AlphaTween : Tween
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private AlphaComponent alphaCmp;
        private List<AlphaTween> childrenTweens = new List<AlphaTween>();
        private float destAlpha;
        #endregion

        #region events
        #endregion

        #region constructor
        public AlphaTween(EaseFunction.EaseFunc easeFunc, GameObject toTween, float destAlpha, float duration, float delay = 0f) : base(easeFunc, toTween, duration, delay)
        {
            this.destAlpha = destAlpha;
            alphaCmp = new AlphaComponent(easeFunc, toTween, destAlpha);
            if (!alphaCmp.IsValid())
            {
                alphaCmp = new UIAlphaComponent(easeFunc, toTween, destAlpha);
            }
            for (int i = 0; i < toTween.transform.childCount; ++i)
            {
                childrenTweens.Add(new AlphaTween(easeFunc, toTween.transform.GetChild(i).gameObject, destAlpha, duration, delay));
            }

            if (duration == 0.0f)
            {
                if (alphaCmp.IsValid())
                {
                    alphaCmp.SetAlpha(destAlpha);
                }
                foreach (AlphaTween alphaTween in childrenTweens)
                {
                    alphaTween.alphaCmp.SetAlpha(destAlpha);
                }
            }

        }
        #endregion

        #region private methods
        #endregion

        #region public methods

        #endregion

        #region protected methods
        protected override void DoStart()
        {

        }

        protected override void DoUpdate()
        {
            if (!alphaCmp.IsValid() && childrenTweens.Count == 0)
            {
                FinishTween();
                return;
            }
            foreach (AlphaTween alphaTween in childrenTweens)
            {
                alphaTween.DoUpdate();
            }
            if(alphaCmp.IsValid())
            {
                alphaCmp.SetAlphaCoef(TweenCoef);
            }
            UpdateTweenCoef();
            if (HasTweenFinished())
            {
                foreach (AlphaTween alphaTween in childrenTweens)
                {
                    alphaTween.FinishTween();
                }
                if (alphaCmp.IsValid())
                {
                    alphaCmp.SetAlphaCoef(1.0f);
                }
                FinishTween();
            }
        }
        #endregion

    }
}
