﻿using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.Tweens
{
    public class ColorTween : Tween
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private ColorComponent colorCmp;
        private List<ColorTween> childrenTweens = new List<ColorTween>();
        private Color destColor;
        #endregion

        #region events
        #endregion

        #region constructor
        public ColorTween(EaseFunction.EaseFunc easeFunc, GameObject toTween, Color destColor, float duration, float delay = 0f) : base(easeFunc, toTween, duration, delay)
        {
            this.destColor = destColor;

            colorCmp = new ColorComponent(easeFunc, toTween, destColor);
            if (!colorCmp.IsValid())
            {
                colorCmp = new UIColorComponent(easeFunc, toTween, destColor);
            }
            for (int i = 0; i < toTween.transform.childCount; ++i)
            {
                childrenTweens.Add(new ColorTween(easeFunc, toTween.transform.GetChild(i).gameObject, destColor, duration, delay));
            }

            if (duration == 0.0f)
            {
                if (colorCmp.IsValid())
                {
                    colorCmp.SetColor(destColor);
                }
                foreach (ColorTween colorTween in childrenTweens)
                {
                    colorTween.colorCmp.SetColor(destColor);
                }
            }
        }
        #endregion

        #region private methods
        #endregion

        #region public methods

        #endregion

        #region protected methods
        protected override void DoStart()
        {

        }

        protected override void DoUpdate()
        {
            if (!colorCmp.IsValid() && childrenTweens.Count == 0)
            {
                FinishTween();
                return;
            }
            foreach (ColorTween colorTween in childrenTweens)
            {
                colorTween.DoUpdate();
            }
            if (colorCmp.IsValid())
            {
                colorCmp.SetColorCoef(TweenCoef);
            }
            UpdateTweenCoef();
            if (HasTweenFinished())
            {
                foreach (ColorTween colorTween in childrenTweens)
                {
                    colorTween.FinishTween();
                }
                if (colorCmp.IsValid())
                {
                    colorCmp.SetColorCoef(1.0f);
                }
                FinishTween();
            }
        }
        #endregion

    }
}
