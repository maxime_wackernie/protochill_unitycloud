﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GLUnity.Tweens
{
    public class AlphaComponent
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private Color matColor;

        private Renderer renderer;
        private SpriteRenderer spriteRenderer;
        private bool isValid;
        #endregion

        #region protected variables
        protected GameObject toAlpha;
        protected float destAlpha;
        protected float startAlpha;
        protected EaseFunction.EaseFunc easeFunc;
        #endregion

        #region events
        #endregion

        #region constructor
        public AlphaComponent(EaseFunction.EaseFunc easeFunc, GameObject toAlpha, float destAlpha)
        {
            this.toAlpha = toAlpha;
            this.destAlpha = destAlpha;
            this.easeFunc = easeFunc;
            renderer = toAlpha.GetComponent<MeshRenderer>();
            spriteRenderer = toAlpha.GetComponent<SpriteRenderer>();
            UpdateMatColor();
        }
        #endregion

        #region private methods
        private void UpdateMatColor()
        {
            if (renderer != null && renderer.material.HasProperty("_Color"))
            {
                matColor = renderer.material.color;
                startAlpha = matColor.a;
                isValid = true;
            }
            if (spriteRenderer != null)
            {
                matColor = spriteRenderer.color;
                startAlpha = matColor.a;
                isValid = true;
            }
        }
        #endregion

        #region public methods
        public virtual void SetAlphaCoef(float coef)
        {
            UpdateMatColor();
            matColor.a = easeFunc(startAlpha, destAlpha, coef);
            if (renderer != null)
            {
                renderer.material.color = matColor;
            }
            if (spriteRenderer != null)
            {
                spriteRenderer.color = matColor;
            }
        }

        public virtual void SetAlpha(float alpha)
        {
            matColor.a = alpha;
            destAlpha = alpha;
            UpdateMatColor();
            if (renderer != null)
            {
                renderer.material.color = matColor;
            }
            if (spriteRenderer != null)
            {
                spriteRenderer.color = matColor;
            }
        }

        public virtual bool IsValid()
        {
            return isValid;
        }
        #endregion
    }
}

