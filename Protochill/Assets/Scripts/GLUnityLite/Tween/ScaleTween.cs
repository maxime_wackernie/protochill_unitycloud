﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GLUnity.Tweens
{
    public class ScaleTween : Tween
    {
        private Vector3 startScale;
        private Vector3 destScale;

        public ScaleTween(EaseFunction.EaseFunc easeFunc, GameObject toTween, Vector3 destScale, float duration, float delay = 0f) : base(easeFunc, toTween, duration, delay)
        {
            
            this.destScale = destScale;
        }

        protected override void DoStart()
        {
            this.startScale = toTween.transform.localScale;
        }

        protected override void DoUpdate()
        {
            if (IsFinished())
            {
                return;
            }
            Vector3 scaling = new Vector3();
            scaling.x = easeFunc(startScale.x, destScale.x, TweenCoef);
            scaling.y = easeFunc(startScale.y, destScale.y, TweenCoef);
            scaling.z = easeFunc(startScale.z, destScale.z, TweenCoef);
            toTween.transform.localScale = scaling;
            UpdateTweenCoef();
            if (HasTweenFinished())
            {
                toTween.transform.localScale = destScale;
                FinishTween();
            }
        }
    }
}
