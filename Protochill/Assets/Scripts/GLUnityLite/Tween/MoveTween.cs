﻿using UnityEngine;
using System.Collections;
using System;

namespace GLUnity.Tweens
{
    public class MoveTween : Tween
    {
        #region private variables
        private Vector3 start;
        private Vector3 destination;
        private Transform trDestination;
        private bool localPosition;
        #endregion

        #region CTOR/DTOR

        public MoveTween(EaseFunction.EaseFunc easeFunc, GameObject toTween, Transform destination, float duration, float delay = 0f, bool localPosition = false): base(easeFunc, toTween, duration, delay)
        {
            this.localPosition = localPosition;
            this.trDestination = destination;
            this.destination = new Vector3();
        }

        public MoveTween(EaseFunction.EaseFunc easeFunc, GameObject toTween, Vector3 destination, float duration, float delay = 0f, bool localPosition = false) : base(easeFunc, toTween, duration, delay)
        {
            this.localPosition = localPosition;
            this.destination = destination;
        }
        #endregion

        #region public methods
        #endregion

        #region protected methods
        protected override void DoStart()
        {
            start = localPosition ? toTween.transform.localPosition : toTween.transform.position;
        }

        protected override void DoUpdate()
        {
            if (IsFinished())
            {
                return;
            }
            Vector3 moving = new Vector3();
            if(trDestination != null)
            {
                this.destination = trDestination.position;
            }
            moving.x = easeFunc(start.x, destination.x, TweenCoef);
            moving.y = easeFunc(start.y, destination.y, TweenCoef);
            moving.z = easeFunc(start.z, destination.z, TweenCoef);
            if(localPosition)
            {
                toTween.transform.localPosition = moving;
            }
            else
            {
                toTween.transform.position = moving;
            }
            UpdateTweenCoef();
            if (HasTweenFinished())
            {
                if (localPosition)
                {
                    toTween.transform.localPosition = destination;
                }
                else
                {
                    toTween.transform.position = destination;
                }
                FinishTween();
            }
        }
        #endregion
    }
}
