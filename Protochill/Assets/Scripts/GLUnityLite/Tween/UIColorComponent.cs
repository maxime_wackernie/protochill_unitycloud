﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GLUnity.Tweens
{
    public class UIColorComponent : ColorComponent
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private CanvasRenderer renderer;
        #endregion

        #region events
        #endregion

        #region constructor
        public UIColorComponent(EaseFunction.EaseFunc easeFunc, GameObject toAlpha, Color destColor) : base(easeFunc, toAlpha, destColor)
        {
            renderer = toAlpha.GetComponent<CanvasRenderer>();
        }
        #endregion

        #region private methods
        #endregion

        #region public methods
        public override void SetColorCoef(float coef)
        {
            if (renderer != null)
            {
                var color = renderer.GetColor();
                color.a = easeFunc(startColor.a, destColor.a, coef);
                color.r = easeFunc(startColor.r, destColor.r, coef);
                color.g = easeFunc(startColor.g, destColor.g, coef);
                color.b = easeFunc(startColor.b, destColor.b, coef);
                renderer.SetColor(color);
            }

        }

        public override void SetColor(Color color)
        {
            if (renderer != null)
            {
                renderer.SetColor(color);
            }

        }

        public override bool IsValid()
        {
            return renderer != null;
        }
        #endregion
    }
}
