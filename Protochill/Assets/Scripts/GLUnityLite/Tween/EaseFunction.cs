﻿using UnityEngine;

namespace GLUnity.Tweens
{
    public static class EaseFunction
    {

        public delegate float EaseFunc(float start, float end, float coef);

        public static float Linear(float start, float end, float coef)
        {
            return Mathf.Lerp(start, end, coef);
        }

        public static float QuadIn(float start, float end, float coef)
        {
            return (end - start)* coef * coef + start;
        }

        public static float QuadOut(float start, float end, float coef)
        {
            return (-end + start)* coef * (coef - 2) + start;
        }

        public static float QuadInOut(float start, float end, float coef)
        {
            coef *= 2.0f;
            if (coef < 1) return (end - start) / 2 * coef * coef + start;
            coef--;
            return (-end + start) / 2 * (coef * (coef - 2) - 1) + start;
        }

        public static float CubicIn(float start, float end, float coef)
        {
            return (end - start) * coef * coef * coef + start;
        }

        public static float CubicOut(float start, float end, float coef)
        {
            coef--;
            return (end - start) * (coef * coef * coef + 1) + start;
        }

        public static float CubicInOut(float start, float end, float coef)
        {
            coef *= 2.0f;

            if (coef < 1) return (end -start) / 2 * coef * coef * coef + start;
            coef -= 2;
            return (end - start) / 2 * (coef * coef * coef + 2) + start;
        }

        public static float QuarticIn(float start, float end, float coef)
        {
            return (end - start) * coef * coef * coef * coef + start;
        }

        public static float QuarticOut(float start, float end, float coef)
        {
            coef--;
            return (-end + start) * (coef * coef * coef * coef - 1) + start;
        }

        public static float QuarticInOut(float start, float end, float coef)
        {
            coef *= 2.0f;
            if (coef < 1) return (end - start) / 2 * coef * coef * coef * coef + start;
            coef -= 2;
            return (-end + start) / 2 * (coef * coef * coef * coef - 2) + start;
        }

        public static float QuintIn(float start, float end, float coef)
        {
            return (end - start) * coef * coef * coef * coef * coef + start;
        }

        public static float QuintOut(float start, float end, float coef)
        {
            coef--;
            return (end - start) * (coef * coef * coef * coef * coef + 1) + start;
        }

        public static float QuintInOut(float start, float end, float coef)
        {
            coef *= 2.0f;
            if (coef < 1) return (end - start) / 2 * coef * coef * coef * coef * coef + start;
            coef -= 2;
            return (end - start) / 2 * (coef * coef * coef * coef * coef + 2) + start;
        }

        public static float SinIn(float start, float end, float coef)
        {
            return (-end + start) * Mathf.Cos(coef * (Mathf.PI / 2)) + (end - start) + start;
        }

        public static float SinOut(float start, float end, float coef)
        {
            return (end - start) * Mathf.Sin(coef * (Mathf.PI / 2)) + start;
        }

        public static float SinInOut(float start, float end, float coef)
        {
            return (-end + start) / 2 * (Mathf.Cos(Mathf.PI * coef) - 1) + start;
        }

        public static float ExpIn(float start, float end, float coef)
        {
            return (end - start) * Mathf.Pow(2, 10 * (coef - 1)) + start;
        }

        public static float ExpOut(float start, float end, float coef)
        {
            return (end - start) * (-Mathf.Pow(2, -10 * coef) + 1) + start;
        }

        public static float ExpInOut(float start, float end, float coef)
        {
            coef *= 2.0f;
            if (coef < 1) return (end - start) / 2 * Mathf.Pow(2, 10 * (coef - 1)) + start;
            coef--;
            return (end - start) / 2 * (-Mathf.Pow(2, -10 * coef) + 2) + start;
        }

        public static float CircIn(float start, float end, float coef)
        {
            return (-end + start) * (Mathf.Sqrt(1 - coef * coef) - 1) + start;
        }

        public static float CircOut(float start, float end, float coef)
        {
            coef--;
            return (end - start) * Mathf.Sqrt(1 - coef * coef) + start;
        }

        public static float CircIntOut(float start, float end, float coef)
        {
            coef *= 2.0f;
            if (coef < 1) return (-end + start) / 2 * (Mathf.Sqrt(1 - coef * coef) - 1) + start;
            coef -= 2;
            return (end - start) / 2 * (Mathf.Sqrt(1 - coef * coef) + 1) + start;
        }

        public static float BounceOut(float start, float end, float coef)
        {
            if (coef < (1 / 2.75f))
            {
                return (end - start) * (7.5625f * coef * coef) + start;
            }
            else if (coef < (2 / 2.75f))
            {
                coef -= (1.5f / 2.75f);
                return (end - start) * (7.5625f * coef * coef + .75f) + start;
            }
            else if (coef < (2.5 / 2.75))
            {
                coef -= (2.25f / 2.75f);
                return (end - start) * (7.5625f * coef * coef + .9375f) + start;
            }
            else
            {
                coef -= (2.625f / 2.75f);
                return (end - start) * (7.5625f * coef * coef + .984375f) + start;
            }
        }

        public static float BounceIn(float start, float end, float coef)
        {
            return (end - start) - BounceOut(0, (end - start), 1.0f - coef) + start;
        }

        public static float BounceInOut(float start, float end, float coef)
        {
            if (coef < 0.5f) return BounceIn(0, (end - start), coef * 2.0f) * 0.5f + start;
            else return BounceOut(0, (end - start), coef * 2.0f - 1.0f) * 0.5f + (end - start) * .5f + start;
        }
    }
}
