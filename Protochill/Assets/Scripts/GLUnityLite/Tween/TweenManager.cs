﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.Tweens
{
    public class TweenManager : Singleton<TweenManager>
    {

        #region private variables
        private Dictionary<object, List<Tween>> tweens = new Dictionary<object, List<Tween>>();
        #endregion

        #region private methods
        private void Update()
        {
            foreach (KeyValuePair<object, List<Tween>> entry in tweens)
            {
                List<Tween> tweensEntry = entry.Value;
                for (int i = tweensEntry.Count - 1; i >= 0; --i)
                {
                    Tween tween = tweensEntry[i];
                    if (tween.IsFinished())
                    {
                        tweensEntry.Remove(tween);
                    }
                    else
                    {
                        tween.Update();
                    }
                }

            }


        }

        private void AddTween(Tween tween, object obj)
        {
            if (!tweens.ContainsKey(obj))
            {
                tweens[obj] = new List<Tween>();
            }
            tweens[obj].Add(tween);
        }

        private IEnumerator SequenceTweensCoroutine(Tween[] tweens)
        {
            for(int i = 0; i < tweens.Length; ++i)
            {
                AddTween(tweens[i], tweens[i].ToTween);
                yield return new WaitForSeconds(tweens[i].Duration + tweens[i].Delay);
            }
        }
        #endregion

        #region public methods
        //Legacy
        public Tween DoLinearMoveTween(GameObject toMove, Vector3 to, float time, float delay = 0.0f, bool localPosition = false)
        {
            Tween tween = new MoveTween(EaseFunction.Linear, toMove, to, time, delay, localPosition);
            AddTween(tween, toMove);
            return tween;
        }

        //Legacy
        public Tween DoLinearAlphaTween(GameObject toTween, float alpha, float time, float delay = 0.0f)
        {
            Tween tween = new AlphaTween(EaseFunction.Linear, toTween, alpha, time, delay);
            AddTween(tween, toTween);
            return tween;
        }

        public Tween DoMoveTween(EaseFunction.EaseFunc easeFunc, GameObject toMove, Vector3 to, float time, float delay = 0.0f, bool localPosition = false)
        {
            Tween tween = new MoveTween(easeFunc, toMove, to, time, delay, localPosition);
            AddTween(tween, toMove);
            return tween;
        }

        public Tween DoMoveTween(EaseFunction.EaseFunc easeFunc, GameObject toMove, Transform to, float time, float delay = 0.0f, bool localPosition = false)
        {
            Tween tween = new MoveTween(easeFunc, toMove, to, time, delay, localPosition);
            AddTween(tween, toMove);
            return tween;
        }

        public Tween DoAlphaTween(EaseFunction.EaseFunc easeFunc, GameObject toTween, float alpha, float time, float delay = 0.0f)
        {
            Tween tween = new AlphaTween(easeFunc, toTween, alpha, time, delay);
            AddTween(tween, toTween);
            return tween;
        }

        public Tween DoScaleTween(EaseFunction.EaseFunc easeFunc, GameObject toTween, Vector3 to, float time, float delay = 0.0f)
        {
            Tween tween = new ScaleTween(easeFunc, toTween, to, time, delay);
            AddTween(tween, toTween);
            return tween;
        }


        public Tween DoColorTween(EaseFunction.EaseFunc easeFunc, GameObject toTween, Color to, float time, float delay = 0.0f)
        {
            Tween tween = new ColorTween(easeFunc, toTween, to, time, delay);
            AddTween(tween, toTween);
            return tween;
        }

        public Tween DoRotationTween(EaseFunction.EaseFunc easeFunc, GameObject toTween, Quaternion destination, float time, float delay = 0.0f, bool localRotation = false)
        {
            Tween tween = new RotationTween(easeFunc, toTween, destination, time, delay, localRotation);
            AddTween(tween, toTween);
            return tween;
        }

        public Tween DoFloatTween(EaseFunction.EaseFunc easeFunc, object obj, string propName, float dest, float time, float delay = 0.0f)
        {
            Tween tween = new FloatTween(easeFunc, obj, propName, dest, time, delay);
            AddTween(tween, obj);
            return tween;
        }

        public void SequenceTweens(params Tween [] tweens)
        {
            StartCoroutine(SequenceTweensCoroutine(tweens));
        }

        public void ClearTweens(object toTween)
        {
            if (tweens.ContainsKey(toTween))
            {
                foreach(var t in tweens[toTween])
                {
                    t.FinishTween();
                }
                tweens[toTween].Clear();
            }
        }
        #endregion

    }
}
