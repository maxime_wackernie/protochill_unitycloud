﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GLUnity.Tweens
{
    public class UIAlphaComponent : AlphaComponent
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private CanvasRenderer renderer;
        private Graphic graphic;
        private bool isTextMesh;
        #endregion

        #region events
        #endregion

        #region constructor
        public UIAlphaComponent(EaseFunction.EaseFunc easeFunc, GameObject toAlpha, float destAlpha) : base(easeFunc, toAlpha, destAlpha)
        {
            renderer = toAlpha.GetComponent<CanvasRenderer>();
            graphic = toAlpha.GetComponent<Graphic>();
            isTextMesh = graphic != null && graphic.ToString().Contains("TextMeshPro");
            
        }
        #endregion

        #region private methods
        #endregion

        #region public methods
        public override void SetAlphaCoef(float coef)
        {

            if (renderer != null && !isTextMesh)
            {
                renderer.SetAlpha(easeFunc(startAlpha, destAlpha, coef));
            }
            else if (graphic != null)
            {
                var color = graphic.color;
                color.a = easeFunc(startAlpha, destAlpha, coef);
                graphic.color = color;

            }

        }

        public override void SetAlpha(float alpha)
        {
            if(renderer != null && !isTextMesh)
            {
                renderer.SetAlpha(alpha);
            }
            else if (graphic != null)
            {
                var color = graphic.color;
                color.a = alpha;
                graphic.color = color;
            }
        }

        public override bool IsValid()
        {
            return renderer != null || graphic != null;
        }
        #endregion
    }
}
