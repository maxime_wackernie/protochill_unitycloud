﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace GLUnity.Tweens
{
    public class FloatTween : Tween
    {
        private PropertyInfo propInfo;
        private FieldInfo fieldInfo;

        private float start;
        private float dest;
        private object obj;

        public FloatTween(EaseFunction.EaseFunc easeFunc, object obj, string propName, float dest, float duration, float delay): base(easeFunc, null, duration, delay)
        {
            propInfo = obj.GetType().GetProperty(propName);
            fieldInfo = obj.GetType().GetField(propName, BindingFlags.Public |
                                              BindingFlags.NonPublic |
                                              BindingFlags.Instance);
            if (propInfo == null && fieldInfo == null)
            {
                throw new Exception("can't find property " + propName + " on " + obj);
            }
            this.obj = obj;
            this.dest = dest;
        }

        protected override void DoStart()
        {
            
            if (propInfo != null)
            {
                start = (float)propInfo.GetValue(obj, null);
            }
            else if(fieldInfo != null)
            {
                start = (float)fieldInfo.GetValue(obj);
            }
        }

        protected override void DoUpdate()
        {
            if (IsFinished())
            {
                return;
            }
            var newVal = easeFunc(start, dest, TweenCoef);
            if (propInfo != null)
            {
                propInfo.SetValue(obj, newVal, null);
            }
            else if (fieldInfo != null)
            {
                fieldInfo.SetValue(obj, newVal);
            }
            UpdateTweenCoef();
            if (HasTweenFinished())
            {
                FinishTween();
            }
        }
    }
}
