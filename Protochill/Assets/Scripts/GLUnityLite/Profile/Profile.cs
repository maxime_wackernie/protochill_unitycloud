﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace GLUnity.Profile
{


    public class Profile<PData> where PData : new()
    {
        public delegate void ProfileDelegate();
        public event ProfileDelegate savedEvent = null;
        public event ProfileDelegate loadedEvent = null;

        #region constants
        private const string FILE_DATA = "data.bin";
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private string fullPath;

        private static Profile<PData> instance = null;
        #endregion

        #region protected variables
        protected PData profileData;
        #endregion

        #region properties
        public PData ProfileData
        {
            get
            {
                return profileData;
            }
            set
            {
                profileData = value;
            }
        }

        public static Profile<PData> Instance {
            get
            {
                if(instance == null)
                {
                    instance = new Profile<PData>();
                }
                return instance;
            }
        }
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        public Profile()
        {
            fullPath = Path.Combine(Application.persistentDataPath, FILE_DATA);
            profileData = new PData();
            Load();

        }
        #endregion

        #region private methods
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public void Load()
        {

            try
            {
                if(File.Exists(fullPath))
                {
                    using (FileStream fs = new FileStream(fullPath, FileMode.Open))
                    {
                        BinaryFormatter formater = new BinaryFormatter();
                        profileData = (PData)formater.Deserialize(fs);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            loadedEvent?.Invoke();
        }

        public void Save()
        {
            try
            {
                using (FileStream fs = new FileStream(fullPath, FileMode.OpenOrCreate))
                {
                    BinaryFormatter formater = new BinaryFormatter();
                    formater.Serialize(fs, profileData);
                }
            }
            catch (Exception e)
            {
                Debug.Log("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            savedEvent?.Invoke();

        }
        #endregion
    }

}
