﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {

    #region constants
    #endregion

    #region enums
    #endregion

    #region exposed variables
    [Header("Binding")]
    [SerializeField]
    private Transform toFollow = null;
    [Header("Parameters")]
    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private bool lerp = true;
    [SerializeField]
    private bool x = true;
    [SerializeField]
    private bool y = true;
    [SerializeField]
    private bool z = true;

    #endregion

    #region private variables
    #endregion

    #region protected variables
    #endregion

    #region properties
    public bool ActivateX
    {
        set
        {
            x = value;
        }
    }

    public bool ActivateY
    {
        set
        {
            y = value;
        }
    }

    public bool ActivateZ
    {
        set
        {
            z = value;
        }
    }

    public Transform ToFollow
    {
        set
        {
            toFollow = value;
        }
    }

    #endregion

    #region events
    #endregion

    #region Constructor / Destructor
    #endregion

    #region private methods
    private void LateUpdate()
    {
        var toFollowPos = toFollow.position;
        if(!x)
        {
            toFollowPos.x = transform.position.x;
        }
        if (!y)
        {
            toFollowPos.y = transform.position.y;
        }
        if (!z)
        {
            toFollowPos.z = transform.position.z;
        }
        if(lerp)
        {
            transform.position = Vector3.Lerp(transform.position, toFollowPos, Time.deltaTime * speed);
        }
        else
        {
            transform.position = toFollowPos;
        }
    }
    #endregion

    #region protected methods
    #endregion

    #region public methods
    #endregion
}
