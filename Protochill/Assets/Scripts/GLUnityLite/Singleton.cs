﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace GLUnity
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        private static object _lock = new object();

        public static T Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = (T)FindObjectOfType(typeof(T));

                        if (_instance == null)
                        {
                            GameObject singleton = new GameObject();
                            _instance = singleton.AddComponent<T>();
                            singleton.name = "(singleton) " + typeof(T).ToString();

                            MethodInfo method = _instance.GetType().GetMethod("Awake");
                            if(method != null)
                            {
                                method.Invoke(_instance, new object[] { });
                            }                            
                        }
                    }

                    return _instance;
                }
            }
        }

        protected static void SetInstance(T instance)
        {
            _instance = instance;
        }

    }
}


