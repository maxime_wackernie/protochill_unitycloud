﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;
namespace GLUnity
{
    public class ObjectPool : Singleton<ObjectPool>
    {
        [System.Serializable]
        public struct PoolDescriptor
        {
            public PoolableObject prefab;
            public string specialKey;
            public int count;

            public Transform poolParent;
        }
        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        [SerializeField]
        private PoolDescriptor[] objectsPrefabs = new PoolDescriptor[0];
        [SerializeField]
        private Transform defaultPoolParent = null;
        #endregion

        #region private variables
        private Dictionary<string, List<PoolableObject>> objectsPerType = new Dictionary<string, List<PoolableObject>>();
        private Dictionary<string, List<PoolableObject>> objectsPerPrefab = new Dictionary<string, List<PoolableObject>>();
        #endregion

        #region protected variables
        #endregion

        #region properties
        public PoolDescriptor[] ObjectsPrefabs
        {
            get
            {
                return objectsPrefabs;
            }
        }
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private void Awake()
        {
            Transform poolObjectParent = null;
            foreach (var poolableObjectPrefab in objectsPrefabs)
            {
                var poolablePrefab = poolableObjectPrefab.prefab;
                if(poolablePrefab == null)
                {
                    continue;
                }
                string hashKey = GetHashKey(poolablePrefab.GetType().GetHashCode(), poolableObjectPrefab.specialKey);
                string prefabName = GetPrefabName(poolablePrefab, poolableObjectPrefab.specialKey);
                var pool = new List<PoolableObject>();
                if (!objectsPerType.ContainsKey(hashKey))
                {
                    objectsPerType[hashKey] = pool;
                }    
                if(!objectsPerPrefab.ContainsKey(prefabName))
                {
                    objectsPerPrefab[prefabName] = pool;
                }
                for (int i = 0; i < poolableObjectPrefab.count; ++i)
                {
                    poolObjectParent = poolableObjectPrefab.poolParent != null ? poolableObjectPrefab.poolParent : defaultPoolParent;
                    var poolable = InstantiatePoolable(poolablePrefab, poolObjectParent, i, poolableObjectPrefab.specialKey);
                    if(poolable != null)
                    {
                        pool.Add(poolable);
                    }                    
                }
            }
        }

        private Transform GetPrefabPoolParent(PoolableObject prefab)
        {
            foreach (PoolDescriptor poolDescriptor in objectsPrefabs)
            {
                if (poolDescriptor.prefab == prefab)
                {
                    return poolDescriptor.poolParent != null ? poolDescriptor.poolParent : defaultPoolParent;
                }
            }

            return defaultPoolParent;
        }

        private PoolableObject InstantiatePoolable(PoolableObject poolablePrefab, Transform poolParent, int index, string specialKey = "")
        {
            var poolable = ExtensionMethods.Instantiate(poolablePrefab, poolParent);
            if(poolable != null)
            {
                poolable.name = poolablePrefab.gameObject.name + "_" +  specialKey + "_" + index.ToString();
                poolable.SetAvailable();
            }
            return poolable;
        }

        private static string GetHashKey(int hascode, string specialKey)
        {
            return hascode.ToString() + (specialKey == string.Empty ? "" : "_" + specialKey);
        }

        private static string GetPrefabName(PoolableObject prefab, string specialKey)
        {
            return prefab.gameObject.name + (specialKey == string.Empty ? "" : "_" + specialKey);
        }

        #endregion

        #region protected methods
        #endregion

        #region public methods
        public T GetAvailable<T>(string specialKey = "", bool add = true, float lifeTime = -1f) where T : PoolableObject
        {
            PoolableObject pO = GetAvailable(typeof(T), specialKey, add, lifeTime);
            if (pO != null)
            {
                return pO as T;
            }
            return null;
        }

        public void Shuffle<T>(string specialKey = "") where T : PoolableObject
        {
            int hashCode = typeof(T).GetHashCode();
            List<PoolableObject> objects;
            if (objectsPerType.TryGetValue(GetHashKey(hashCode, specialKey), out objects))
            {
                objects.Shuffle();
            }
        }

        public PoolableObject GetAvailable(System.Type type, string specialKey = "", bool add = true, float lifeTime = -1f)
        {
            int hashCode = type.GetHashCode();
            List<PoolableObject> objects;
            if (objectsPerType.TryGetValue(GetHashKey(hashCode, specialKey), out objects))
            {
                foreach (PoolableObject pO in objects)
                {
                    if (pO.IsAvailable())
                    {
                        pO.Spawn(lifeTime);
                        return pO;
                    }
                }
            }
            if(!add)
            {
                return null;
            }
            var toSpawn = AddAvailable(type, specialKey);
            if(toSpawn != null)
            {
                toSpawn.Spawn(lifeTime);
            }
            return toSpawn;
        }

        public PoolableObject GetAvailable(PoolableObject prefab, string specialKey = "",bool add = true, float lifeTime = -1f)
        {
            List<PoolableObject> objects;
            string prefabName = GetPrefabName(prefab, specialKey);
            if (objectsPerPrefab.TryGetValue(prefabName, out objects))
            {
                foreach (PoolableObject pO in objects)
                {
                    if (pO.IsAvailable())
                    {
                        pO.Spawn(lifeTime);

                        return pO;
                    }
                }
            }
            if (!add)
            {
                return null;
            }
            var toSpawn = AddAvailable(prefab, GetPrefabPoolParent(prefab));
            if (toSpawn != null)
            {
                toSpawn.Spawn(lifeTime);
            }
            return toSpawn;
        }

        public void SetAllAvailable<T>(string specialKey = "") where T : PoolableObject
        {
            int hashCode = typeof(T).GetHashCode();
            List<PoolableObject> objects;
            if (objectsPerType.TryGetValue(GetHashKey(hashCode, specialKey), out objects))
            {
                foreach(var obj in objects)
                {
                    if(!obj.IsAvailable())
                    {
                        obj.SetAvailable();
                    }
                }
            }
        }

        public void SetAllAvailable(PoolableObject prefab, string specialKey = "")
        {
            List<PoolableObject> objects;
            string prefabName = GetPrefabName(prefab, specialKey);
            if(objectsPerPrefab.TryGetValue(prefabName, out objects))
            {
                foreach (var obj in objects)
                {
                    if (!obj.IsAvailable())
                    {
                        obj.SetAvailable();
                    }
                }
            }
        }

        public PoolableObject AddAvailable(System.Type type, string specialKey = "")
        {
            int hashCode = type.GetHashCode();
            string hashKey = GetHashKey(hashCode, specialKey);
            foreach (var poolableObjectPrefab in objectsPrefabs)
            {
                var poolablePrefab = poolableObjectPrefab.prefab;
                if (poolablePrefab == null)
                {
                    continue;
                }
                string prefabHashKey = GetHashKey(poolablePrefab.GetType().GetHashCode(), poolableObjectPrefab.specialKey);
                if (prefabHashKey == hashKey)
                {
                    if (!objectsPerType.ContainsKey(hashKey))
                    {
                        objectsPerType[hashKey] = new List<PoolableObject>();
                    }
                    Transform poolParent = poolableObjectPrefab.poolParent ? poolableObjectPrefab.poolParent : defaultPoolParent;
                    return AddAvailable(poolablePrefab, poolParent, specialKey);
                }
            }
            return null;
        }

        public PoolableObject AddAvailable(PoolableObject prefab, Transform parent, string specialKey = "")
        {
            List<PoolableObject> objects;
            string hashKey = GetHashKey(prefab.GetType().GetHashCode(), specialKey);
            string prefabName = GetPrefabName(prefab, specialKey);
            objectsPerPrefab.TryGetValue(prefabName, out objects);

            if (objects == null)
            {
                objects = new List<PoolableObject>();
            }

            var poolableObject = InstantiatePoolable(prefab, parent, objects.Count);
            objects.Add(poolableObject);
            objectsPerPrefab[prefabName] = objects;
            objectsPerType[hashKey] = objects;
            return poolableObject;
        }
        #endregion
    }

}
