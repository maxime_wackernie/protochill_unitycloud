﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

#if UNITY_IPHONE
using UnityEngine.iOS;
#endif

namespace GLUnity
{
    public static class DeviceHelper
    {

        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = MD5.Create();  //or use SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static string GetDeviceGUID()
        {
#if UNITY_IPHONE
        return GetHashString(Device.advertisingIdentifier);
#else

            string macAddr = null;
            NetworkInterface[] adapters1 = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface currentInterface in adapters1)
            {
                var type = currentInterface.NetworkInterfaceType;


                if (type == System.Net.NetworkInformation.NetworkInterfaceType.Ethernet
                    || type == System.Net.NetworkInformation.NetworkInterfaceType.GigabitEthernet
                    || type == System.Net.NetworkInformation.NetworkInterfaceType.FastEthernetFx
                    || type == System.Net.NetworkInformation.NetworkInterfaceType.Wireless80211)
                {

                    macAddr = currentInterface.GetPhysicalAddress().ToString();

                }

            }

            if (macAddr == null)
            {
                return GetHashString("000000000000");
            }
            else
            {
                return GetHashString(macAddr);
            }
#endif
        }
    }
}
