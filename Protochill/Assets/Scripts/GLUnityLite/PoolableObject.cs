﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace GLUnity
{
    public class PoolableObject : MonoBehaviour
    {
        IEnumerator LifetimeCoroutine;

        public virtual bool IsAvailable()
        {
            return !gameObject.activeSelf;
        }

        public virtual void SetAvailable()
        {
            gameObject.SetActive(false);

            if (LifetimeCoroutine != null)
            {
                CoroutineHelper.Instance.StopCoroutine(LifetimeCoroutine);
                LifetimeCoroutine = null;
            }
        }

        public virtual void Spawn(float lifeTime)
        {
            gameObject.SetActive(true);

            if (lifeTime > 0f)
                LifetimeCoroutine = CoroutineHelper.Instance.ExecuteAfter(lifeTime, SetAvailable);
        }

        public virtual void InitPosition(Vector3 position)
        {
            transform.position = position;
        }
    }
}

