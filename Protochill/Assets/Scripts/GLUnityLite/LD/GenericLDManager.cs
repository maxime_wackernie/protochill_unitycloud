﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GLUnity;

namespace GLUnity.LD
{

    public class GenericLDManager<T> : MonoBehaviour where T : LDDescriptor
    {
        public delegate void ChunkDelegate(LDChunk chunk);
        public delegate void LevelManagerDelegate();
        public event ChunkDelegate chunkCompletedEvent = null;
        public event ChunkDelegate chunkAddedEvent = null;
        public event LevelManagerDelegate levelUpdateEvent = null;
        public event LevelManagerDelegate initEvent = null;

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        [Header("Binding")]
        [SerializeField]
        private T[] levels = new T[0];
        [SerializeField]
        private Transform startTransform = null;
        [Header("Parameters")]
        [SerializeField]
        private int startChunkCount = 3;
        [SerializeField]
        private int countBeforeRespawn = 2;

        [SerializeField]
        private Vector3 expandDirection = Vector3.forward;
        [SerializeField]
        private float expandSpacing = 0f;
        [SerializeField]
        private bool autoStart = false;
        #endregion

        #region private variables
        private Vector3 startPosition = Vector3.zero;
        private int currentLevel = 0;
        private float positionAccum = 0;
        protected List<LDChunk> chunks = new List<LDChunk>();
        private List<LDChunk> pendingRespawn = new List<LDChunk>();
        #endregion

        #region protected variables
        #endregion

        #region properties
        public int CurrentLevel
        {
            get
            {
                return currentLevel;
            }
            set
            {
                if(value == currentLevel)
                {
                    return;
                }
                currentLevel = value;
                currentLevel = Mathf.Clamp(currentLevel, 0, levels.Length - 1);
                levelUpdateEvent?.Invoke();
            }
        }

        public int LevelsCount
        {
            get
            {
                return levels.Length;
            }
        }

        public T CurrentLevelDescriptor
        {
            get
            {
                return levels[currentLevel];
            }
        }

        public List<LDChunk> Chunks
        {
            get
            {
                return chunks;
            }
        }

        public LDChunk LastChunk
        {
            get
            {
                if (chunks.Count == 0)
                {
                    return null;
                }
                return chunks[chunks.Count - 1];
            }
        }

        public LDChunk CurrentChunk
        {
            get
            {
                if (chunks.Count == 0)
                {
                    return null;
                }
                return chunks[0];
            }
        }

        public float ZBound
        {
            get
            {
                return positionAccum;
            }
        }

        public float ExpandSpacing
        {
            get
            {
                return expandSpacing;
            }
        }

        protected T[] Levels
        {
            get
            {
                return levels;
            }
        }
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private void Start()
        {
            if (autoStart)
            {
                Init();
            }
        }

        private LDDescriptor LevelDescriptorByLevelIndex(int levelIndex)
        {
            if (levelIndex < 0 || levelIndex >= levels.Length)
            {
                throw new System.Exception($"wrong level index should be between 0 and {levels.Length}");
            }

            return levels[levelIndex];
        }

        private void Chunk_LevelExitEvent(LDChunk chunk)
        {
            var firstChunk = chunks.Pop();
            pendingRespawn.Add(firstChunk);

            if (pendingRespawn.Count >= countBeforeRespawn)
            {
                var toRespawn = pendingRespawn.Pop();
                toRespawn.SetAvailable();

            }
            chunk.levelExitEvent -= Chunk_LevelExitEvent;
            AddChunk();
            chunkCompletedEvent?.Invoke(chunk);
        }


        #endregion

        #region protected methods
        protected virtual void AddChunk()
        {
            var levelDesc = LevelDescriptorByLevelIndex(currentLevel);
            var chunkPrefab = levelDesc.GetRandomChunkPrefab();
            AddChunk(chunkPrefab);
        }

        protected void AddChunk(LDChunk chunkPrefab)
        {

            var chunk = ObjectPool.Instance.GetAvailable(chunkPrefab) as LDChunk;
            chunk.levelExitEvent += Chunk_LevelExitEvent;
            chunk.transform.position = startPosition + expandDirection * positionAccum;
            var expandBounds = new Vector3(chunk.Bounds.size.x * expandDirection.x,
                                           chunk.Bounds.size.y * expandDirection.y,
                                           chunk.Bounds.size.z * expandDirection.z);
            chunks.Add(chunk);
            positionAccum += expandBounds.magnitude + expandSpacing;
            chunkAddedEvent?.Invoke(chunk);
        }

        #endregion

        #region public methods
        public virtual void Reset()
        {
            currentLevel = 0;
            positionAccum = 0;
            foreach (var chunk in chunks)
            {
                chunk.SetAvailable();
                chunk.levelExitEvent -= Chunk_LevelExitEvent;
            }
            chunks.Clear();
            foreach (var chunk in pendingRespawn)
            {
                chunk.SetAvailable();
                chunk.levelExitEvent -= Chunk_LevelExitEvent;
            }
            pendingRespawn.Clear();
        }

        public virtual void Init()
        {
            if(startTransform != null)
            {
                startPosition = startTransform.position;
            }
            for(int i = 0; i < startChunkCount; ++i)
            {
                AddChunk();
            }
            initEvent?.Invoke();
        }
        
        #endregion
    }

}
