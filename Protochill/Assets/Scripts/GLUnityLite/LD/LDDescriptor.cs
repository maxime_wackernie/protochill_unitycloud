﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.LD
{
    public class LDDescriptor : ScriptableObject
    {
        public LDChunk[] chunkPrefabs = new LDChunk[0];

        public LDChunk GetRandomChunkPrefab()
        {
            if (chunkPrefabs.Length == 0)
            {
                return null;
            }
            return chunkPrefabs[Random.Range(0, chunkPrefabs.Length)];
        }
    }

}
