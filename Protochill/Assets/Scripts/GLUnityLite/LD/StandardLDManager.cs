﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GLUnity.LD
{

    public class StandardLDManager : GenericLDManager<LDDescriptor> {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
#if UNITY_EDITOR
        [MenuItem("Assets/Create/Data/LevelDescriptor")]
        private static void CreateLDDescriptor()
        {
            var asset = ScriptableObject.CreateInstance<LDDescriptor>();
            ProjectWindowUtil.CreateAsset(asset, "Assets/Data/LevelDecs/NewLDDescriptor.asset");
        }
#endif
#endregion

        #region protected methods
        #endregion

        #region public methods
        #endregion
    }
}
