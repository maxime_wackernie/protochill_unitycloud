﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

namespace GLUnity.LD
{
    [RequireComponent(typeof(Collider))]
    public class LDChunk : PoolableObject
    {
        public delegate void LevelChunkDelegate(LDChunk chunk);

        public LevelChunkDelegate levelExitEvent = null;

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private Collider chunkCollider = null;
        #endregion

        #region protected variables
        #endregion

        #region properties
        public Bounds Bounds
        {
            get
            {
                return chunkCollider.bounds;
            }
        }
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        protected virtual void Awake()
        {
            chunkCollider = GetComponent<Collider>();
        }


        #endregion

        #region protected methods
        #endregion

        #region public methods
        #endregion
    }
}
