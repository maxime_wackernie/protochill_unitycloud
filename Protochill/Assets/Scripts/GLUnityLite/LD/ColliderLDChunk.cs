﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GLUnity.LD
{
    public class ColliderLDChunk : LDChunk {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(Tags.PLAYER_TAG))
            {
                levelExitEvent?.Invoke(this);
            }
        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        #endregion
    }

}
