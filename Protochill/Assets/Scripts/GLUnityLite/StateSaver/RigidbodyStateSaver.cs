﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.StateSaver
{

    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodyStateSaver : AStateSaver
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private Rigidbody rb = null;
        private Vector3 initialVelocity;
        private Vector3 intialAngularVelocity;
        private float initialAngularDrag;
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public override void ResetState()
        {
            rb.velocity = initialVelocity;
            rb.angularVelocity = intialAngularVelocity;
            rb.angularDrag = initialAngularDrag;
        }

        public override void SaveState()
        {
            initialVelocity = rb.velocity;
            intialAngularVelocity = rb.angularVelocity;
            initialAngularDrag = rb.angularDrag;
        }

        #endregion
    }
}
