﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.StateSaver
{
    [DefaultExecutionOrder(-100)]
    public abstract class AStateSaver : MonoBehaviour
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private bool saved = false;
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        #endregion

        #region protected methods
        protected virtual void OnEnable()
        {
            if(!saved)
            {
                SaveState();
                saved = true;
            }            
        }

        #endregion

        #region public methods
        public abstract void SaveState();

        public abstract void ResetState();

        #endregion
    }
}
