﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.StateSaver
{

    public class TransformStateSaver : AStateSaver
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        [SerializeField]
        private bool local = false;
        #endregion

        #region private variables

        private Vector3 initialPos;
        private Vector3 initialScale;
        private Quaternion initialRotation;
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public override void ResetState()
        {
            if (local)
            {
                transform.localPosition = initialPos;
                transform.localRotation = initialRotation;
            }
            else
            {
                transform.position = initialPos;
                transform.rotation = initialRotation;
            }
            transform.localScale = initialScale;
        }

        public override void SaveState()
        {
            initialPos = local ? transform.localPosition : transform.position;
            initialScale = transform.localScale;
            initialRotation = local ? transform.localRotation : transform.rotation;
        }
        #endregion
    }
}
