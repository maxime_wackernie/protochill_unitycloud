﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tags {

    public const string PLAYER_TAG = "Player";

    public static int LayerNameToMask(string layerName)
    {
        return 1 << LayerMask.NameToLayer(layerName);
    }
}
