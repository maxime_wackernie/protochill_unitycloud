﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.Effects
{
    [System.Serializable]
    public class EffectsList
    {

        public Vector3 Position
        {
            set
            {
                foreach(var effect in effects)
                {
                    effect.Position = value;
                }
            }
        }

        public Effect[] effects = new Effect[0];

        public void Trigger()
        {
            foreach (var effect in effects)
            {
                if (effect.gameObject.activeInHierarchy)
                    effect.Trigger();
            }
        }

        
    }
}
