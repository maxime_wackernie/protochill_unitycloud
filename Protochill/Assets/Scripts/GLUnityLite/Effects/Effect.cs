﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GLUnity.Effects
{
    public abstract class Effect : MonoBehaviour
    {
        public abstract void Trigger();

        public virtual Vector3 Position
        {
            set
            {

            }
        }
    }
}
