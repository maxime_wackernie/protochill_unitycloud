﻿using GLUnity;
using GLUnity.Tweens;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.Effects
{
    public class TimescaleEffect : Effect
    {
        [SerializeField]
        private float timeScale = 1f;
        [SerializeField]
        private float duration = 1f;
        [SerializeField]
        private float delay = 0f;
        [SerializeField]
        private float tweenDuration = 0.2f;

        private IEnumerator effectCoroutine = null;

        private float currentTimeScale = 1f;

        private IEnumerator TweenTimeScale(float destination)
        {
            Tween t = TweenManager.Instance.DoFloatTween(EaseFunction.Linear, this, "currentTimeScale", destination, tweenDuration);
            while (!t.IsFinished())
            {
                Time.timeScale = currentTimeScale;
                yield return null;
            }

        }

        private IEnumerator EffectCorountine()
        {
            yield return new WaitForSeconds(delay);
            currentTimeScale = 1f;
            yield return StartCoroutine(TweenTimeScale(timeScale));

            Time.timeScale = timeScale;
            yield return new WaitForSeconds(duration);
            yield return StartCoroutine(TweenTimeScale(1f));

            Time.timeScale = 1f;
            effectCoroutine = null;
        }

        public override void Trigger()
        {
            if (effectCoroutine != null)
            {
                StopCoroutine(effectCoroutine);
                TweenManager.Instance.ClearTweens(this);
                timeScale = 1f;
            }
            effectCoroutine = EffectCorountine();
            StartCoroutine(effectCoroutine);
        }

    }
}
