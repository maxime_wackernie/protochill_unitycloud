﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

namespace GLUnity.Effects
{
    public class PrefabEffectActivator : Effect
    {
        [SerializeField]
        protected PoolableObject prefabToTrigger = null;
        [SerializeField]
        protected bool attachToTransform = true;

        [SerializeField]
        private float duration = 1f;
        [SerializeField]
        private bool setAvailableIfActive = true;

        private PoolableObject pooled = null;
        private IEnumerator executeAfterEnumerator;

        public override Vector3 Position
        {
            set
            {
                if (pooled != null)
                {
                    pooled.transform.position = value;
                }
            }
        }


        public override void Trigger()
        {
            if (pooled != null)
            {
                if (setAvailableIfActive)
                {
                    if (executeAfterEnumerator != null)
                    {
                        CoroutineHelper.Instance.StopCoroutine(executeAfterEnumerator);
                        executeAfterEnumerator = null;
                    }

                    pooled.SetAvailable();
                    pooled = null;
                }
                else
                {
                    return;
                }
            }
            pooled = ObjectPool.Instance.GetAvailable(prefabToTrigger);
            if (attachToTransform)
            {
                pooled.transform.SetParent(transform, false);
            }
            else
            {
                pooled.transform.position = transform.position;
            }

            executeAfterEnumerator = CoroutineHelper.Instance.ExecuteAfter(GetDuration(pooled), () =>
            {
                if (pooled != null)
                {
                    pooled.SetAvailable();
                    pooled = null;
                }
            });
        }

        protected virtual float GetDuration(PoolableObject poolableObject)
        {
            return duration;
        }
    }
}
