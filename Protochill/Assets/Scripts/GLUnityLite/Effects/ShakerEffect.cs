﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GLUnity.IOC;
using GLUnity;


namespace GLUnity.Effects
{
    public class ShakerEffect : Effect
    {

        [Dependency]
        private Shaker shaker = null;
        [SerializeField]
        private float strength = 0.1f;
        [SerializeField]
        private int repeatCount = 3;


        public override void Trigger()
        {
            if (shaker)
            {
                shaker.Shake(strength, repeatCount);
            }
            else
            {
                Debug.LogError("missing shaker");
            }
        }

    }
}
