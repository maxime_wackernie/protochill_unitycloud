﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.Effects
{
    public class SoundEffect : Effect
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        [SerializeField]
        private AudioClip clip = null;
        [SerializeField]
        private float pitch = 1f;
        #endregion

        #region private variables
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public override void Trigger()
        {
            SoundManager.Instance.PlaySound(clip, 0, false, pitch);
        }
        #endregion

    }

}
