﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.Effects
{
    public class FrameFlashEffect : Effect
    {
        [SerializeField]
        private PoolableObject flashQuadPrefab = null;

        [SerializeField]
        private int framesCount = 1;

        [SerializeField]
        private float delay = 0f;

        [SerializeField]
        private Color color = Color.white;

        private Camera cam = null;

        private IEnumerator frameFlashCoroutine = null;
        private PoolableObject flashQuad = null;

        private void Awake()
        {
            cam = Camera.main;
        }

        private IEnumerator FrameFlashCoroutine()
        {
            yield return new WaitForSeconds(delay);
            flashQuad = ObjectPool.Instance.GetAvailable(flashQuadPrefab);
            var renderer = flashQuad.GetComponent<Renderer>();
            if(renderer != null)
            {
                renderer.material.color = color;
                renderer.material.SetColor("_BaseMap", color);
            }
            for (int i = 0; i < framesCount; ++i)
            {
                flashQuad.transform.position = cam.transform.position + cam.transform.forward * (cam.nearClipPlane + 0.5f);
                yield return new WaitForEndOfFrame();
            }
            flashQuad.SetAvailable();
            frameFlashCoroutine = null;
        }

        public override void Trigger()
        {
            if(flashQuad != null)
            {
                flashQuad.SetAvailable();
            }
            if (frameFlashCoroutine != null)
            {
                StopCoroutine(frameFlashCoroutine);
                
            }

            if (flashQuadPrefab != null)
            {
                StartCoroutine(FrameFlashCoroutine());
            }
        }
    }

}
