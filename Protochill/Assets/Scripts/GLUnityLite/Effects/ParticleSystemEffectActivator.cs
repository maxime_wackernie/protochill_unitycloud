﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

namespace GLUnity.Effects
{
    public class ParticleSystemEffectActivator : PrefabEffectActivator
    {
        protected override float GetDuration(PoolableObject poolableObject)
        {
            ParticleSystem particleSystem = poolableObject.GetComponent<ParticleSystem>();
            if (particleSystem == null)
            {
                Debug.LogError("ParticleSystem not found in [" + name + "] => ParticleSystemEffectActivator");
                return base.GetDuration(poolableObject);
            }

            float res = particleSystem.main.duration;

            return res;
        }

    }
}
