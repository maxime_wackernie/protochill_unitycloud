﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.Effects
{
    public class FrameFreezeEffect : Effect
    {
        [SerializeField]
        private float delay = 0f;
        [SerializeField]
        private int framesCount = 1;

        private IEnumerator frameFreezeCoroutine = null;

        private IEnumerator FrameFreezeCoroutine()
        {
            yield return new WaitForSeconds(delay);
            Time.timeScale = 0f;
            for(int i=0; i < framesCount; ++i)
            {
                yield return new WaitForEndOfFrame();
            }
            
            Time.timeScale = 1f;

        }

        public override void Trigger()
        {
            if(frameFreezeCoroutine != null)
            {
                StopCoroutine(frameFreezeCoroutine);
                Time.timeScale = 1f;
                
            }
            frameFreezeCoroutine = FrameFreezeCoroutine();
            StartCoroutine(frameFreezeCoroutine);
        }

    }
}

