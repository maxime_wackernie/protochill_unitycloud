﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

using GLUnity.Tweens;

namespace GLUnity.Effects
{
    public class ChromaticAberrationEffect : Effect
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        [SerializeField]
        private PostProcessVolume volume = null;
        [SerializeField]
        private float intensity = 0.5f;
        [SerializeField]
        private float duration = 0.5f;
        [SerializeField]
        private float delay = 0f;
        [SerializeField]
        private float tweenDuration = 0.2f;
        #endregion

        #region private variables
        private ChromaticAberration aberration = null;
        private IEnumerator aberrationCoroutine = null;
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private IEnumerator TweenChromatic(float destination)
        {
            Tween t = TweenManager.Instance.DoFloatTween(EaseFunction.Linear, aberration.intensity, "value", destination, tweenDuration);
            while (!t.IsFinished())
            {
                yield return null;
            }

        }

        private IEnumerator AberrationCoroutine()
        {
            yield return new WaitForSeconds(delay);
            aberration.enabled.value = true;
            yield return StartCoroutine(TweenChromatic(intensity));            
            aberration.intensity.value = intensity;
            yield return new WaitForSeconds(duration);
            yield return StartCoroutine(TweenChromatic(0));
            aberration.enabled.value = false;
            aberrationCoroutine = null;
        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public override void Trigger()
        {
            if(volume == null)
            {
                Debug.LogError("missing post processing volume");
            }
            volume.profile.TryGetSettings<ChromaticAberration>(out aberration);
            if(aberration == null)
            {
                Debug.LogError("missing aberation effect on post processing volume");
            }

            if(aberrationCoroutine != null)
            {
                StopCoroutine(aberrationCoroutine);
                aberration.intensity.value = 0f;
                aberration.enabled.value = false;
                TweenManager.Instance.ClearTweens(this);
            }
            aberrationCoroutine = AberrationCoroutine();
            StartCoroutine(aberrationCoroutine);
        }
        #endregion

    }

}

