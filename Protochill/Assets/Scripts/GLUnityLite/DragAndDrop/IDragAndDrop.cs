﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.DragAndDrop
{
    public interface IBeginDragHandler
    {
        void BeginDragHandler(Drag drag);
    }

    public interface IMoveDragHandler
    {
        void MoveDragHandler(Drag drag);
    }

    public interface IDropDragHandler
    {
        void DropDragHandler(Drag drag);
    }

    public interface IDropHover
    {
        void Enter(Drag drag);
        void Exit(Drag drag);
    }

    public interface IEndDragHandler
    {
        void EndDragHandler(Drag drag);
    }
}
