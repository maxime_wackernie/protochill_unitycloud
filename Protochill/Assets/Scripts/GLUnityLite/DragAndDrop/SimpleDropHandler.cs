﻿using GLUnity.DragAndDrop;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleDropHandler : MonoBehaviour, IDropDragHandler, IDropHover
{
    public void DropDragHandler(Drag drag)
    {
        Debug.Log("Dropped " + drag.Target.name);
    }

    public void Enter(Drag drag)
    {
        Debug.Log("Enter " + drag.Target.name);
    }

    public void Exit(Drag drag)
    {
        Debug.Log("Exit " + drag.Target.name);
    }
}
