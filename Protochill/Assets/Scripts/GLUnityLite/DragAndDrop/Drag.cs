﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.DragAndDrop
{
    public class Drag
    {
        public Drag(Transform _target, Vector3 _startPosition, Vector3 _hitPoint, float _distance, IBeginDragHandler[] _iBeginDragHandlers, IMoveDragHandler[] _iMoveDragHandlers, IEndDragHandler[] _iEndDragHandlers)
        {
            target = _target;
            startPosition = _startPosition;
            position = startPosition;
            iBeginDragHandlers = _iBeginDragHandlers;
            iMoveDragHandlers = _iMoveDragHandlers;
            iEndDragHandlers = _iEndDragHandlers;
            hitPoint = _hitPoint;
            distance = _distance;

            if (iBeginDragHandlers == null)
                iBeginDragHandlers = new IBeginDragHandler[0];
            if (iMoveDragHandlers == null)
                iMoveDragHandlers = new IMoveDragHandler[0];
            if (iEndDragHandlers == null)
                iEndDragHandlers = new IEndDragHandler[0];
        }

        Transform target;
        Vector3 startPosition;
        Vector3 hitPoint;
        IBeginDragHandler[] iBeginDragHandlers;
        IMoveDragHandler[] iMoveDragHandlers;
        IEndDragHandler[] iEndDragHandlers;
        Transform drop;
        IDropDragHandler dropHandler;
        float distance;
        Vector3 position;
        Vector3 move;

        public Transform Target { get => target; }
        public Vector3 StartPosition { get => startPosition; }
        public IBeginDragHandler[] IBeginDragHandlers { get => iBeginDragHandlers; }
        public IMoveDragHandler[] IMoveDragHandlers { get => iMoveDragHandlers; }
        public IEndDragHandler[] IEndDragHandlers { get => iEndDragHandlers; }
        public Vector3 HitPoint { get => hitPoint; }
        public Transform Drop { get => drop; set => drop = value; }
        public bool DropHandled { get => drop != null; }
        public IDropDragHandler DropHandler { get => dropHandler; set => dropHandler = value; }
        public float Distance { get => distance; }
        public Vector3 Move { get => move; }

        public void MoveTo(Vector3 pos)
        {
            move = pos - position;
            position = pos;
        }
    }
}
