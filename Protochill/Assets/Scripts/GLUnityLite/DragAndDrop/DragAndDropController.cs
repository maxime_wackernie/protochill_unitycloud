﻿using GLUnity.DragAndDrop;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GLUnity.DragAndDrop
{
    public class DragAndDropController : MonoBehaviour
    {
        [SerializeField] string key = "Fire1";
        [SerializeField] LayerMask layerMask = new LayerMask();
        [SerializeField] Camera cam = null;
        [SerializeField] int maxCastResults = 5;
        [SerializeField] float maxDistance = 100f;
        Drag drag;
        RaycastHit[] raycasts;
        List<IDropHover> iDropEvents;
        Vector3 mousePosition;

        private void Awake()
        {
            iDropEvents = new List<IDropHover>();
            raycasts = new RaycastHit[maxCastResults];
            if (cam == null)
            {
                cam = Camera.main;
            }
        }

        void OnButtonDown()
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            int count = Physics.RaycastNonAlloc(ray, raycasts, maxDistance, layerMask.value);

            for (int i = 0; i < count; i++)
            {
                RaycastHit raycastHit = raycasts[i];
                IMoveDragHandler iMoveDragHandler = raycastHit.transform.GetComponent<IMoveDragHandler>();
                if (iMoveDragHandler != null)
                {
                    drag = new Drag(raycastHit.transform,
                        raycastHit.transform.position,
                        raycastHit.point,
                        raycastHit.distance,
                        raycastHit.transform.GetComponentsInChildren<IBeginDragHandler>(),
                        raycastHit.transform.GetComponentsInChildren<IMoveDragHandler>(),
                        raycastHit.transform.GetComponentsInChildren<IEndDragHandler>());
                    OnDragBegin();
                    return;
                }
            }

            mousePosition = Vector3.zero;
        }

        void OnDragBegin()
        {
            foreach (IBeginDragHandler iBegin in drag.IBeginDragHandlers)
            {
                iBegin.BeginDragHandler(drag);
            }
        }

        void OnDrag()
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            int count = Physics.RaycastNonAlloc(ray, raycasts, maxDistance, layerMask.value);
            List<IDropHover> temp = new List<IDropHover>();
            for (int i = 0; i < count; i++)
            {
                RaycastHit raycastHit = raycasts[i];
                IDropHover iDropDragHandler = raycastHit.transform.GetComponent<IDropHover>();

                if (iDropDragHandler != null)
                {
                    temp.Add(iDropDragHandler);
                }
            }

            drag.MoveTo(ray.GetPoint(drag.Distance));

            for (int i = iDropEvents.Count - 1; i >= 0; i--)
            {
                IDropHover iDropDragHandler = iDropEvents[i];
                if (!temp.Contains(iDropDragHandler))
                {
                    iDropEvents.Remove(iDropDragHandler);
                    iDropDragHandler.Exit(drag);
                }
            }

            foreach (IDropHover iDropDragHandler in temp)
            {
                if (!iDropEvents.Contains(iDropDragHandler))
                {
                    iDropEvents.Add(iDropDragHandler);
                    iDropDragHandler.Enter(drag);
                }
            }

            foreach (IMoveDragHandler iMove in drag.IMoveDragHandlers)
            {
                iMove.MoveDragHandler(drag);
            }
        }

        void OnButtonUp()
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            int count = Physics.RaycastNonAlloc(ray, raycasts, maxDistance, layerMask.value);
            for (int i = 0; i < count; i++)
            {
                RaycastHit raycastHit = raycasts[i];
                IDropDragHandler iDropDragHandler = raycastHit.transform.GetComponent<IDropDragHandler>();

                if (iDropDragHandler != null)
                {
                    drag.Drop = raycastHit.transform;
                    drag.DropHandler = iDropDragHandler;
                    iDropDragHandler.DropDragHandler(drag);
                    continue;
                }
            }

            foreach (IEndDragHandler iBegin in drag.IEndDragHandlers)
            {
                iBegin.EndDragHandler(drag);
            }

            foreach (IDropHover IDropHover in iDropEvents)
            {
                IDropHover.Exit(drag);
            }

            drag = null;
            iDropEvents.Clear();
        }

        void Update()
        {
            if (Input.GetButtonDown(key))
            {
                OnButtonDown();
            }

            if (drag != null)
            {
                if (Input.mousePosition != mousePosition && Input.GetButton(key))
                {
                    OnDrag();
                }

                if (Input.GetButtonUp(key))
                {
                    OnButtonUp();
                }
            }
        }

        void Reset()
        {
            layerMask.value = LayerMask.NameToLayer("Everything");
            cam = Camera.main;
        }
    }
}
