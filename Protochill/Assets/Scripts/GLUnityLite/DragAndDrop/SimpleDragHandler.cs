﻿using GLUnity.DragAndDrop;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleDragHandler : MonoBehaviour, IMoveDragHandler, IEndDragHandler, IBeginDragHandler
{
    public void BeginDragHandler(Drag drag)
    {
        Debug.Log("BeginDrag : " + name);
    }

    public void EndDragHandler(Drag drag)
    {
        if (drag.DropHandled)
        {
            Debug.Log("Drop Handled by " + drag.Drop.name);
        }
        else
        {
            Debug.Log("No drophandler " + name);
            transform.position = drag.StartPosition;
        }
    }

    public void MoveDragHandler(Drag drag)
    {
        transform.position += drag.Move;
    }
}
