﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GLUnity
{
    public class Billboard : MonoBehaviour
    {

        private Camera cam = null;

        private void Awake()
        {
            cam = Camera.main;
        }

        private void Update()
        {
            transform.forward = cam.transform.forward;
        }
    }

}
