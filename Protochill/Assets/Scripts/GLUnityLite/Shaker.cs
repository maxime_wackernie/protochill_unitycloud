﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

using GLUnity;
using GLUnity.IOC;

namespace GLUnity
{
    [IOCExpose]
    public class Shaker : MonoBehaviour
    {
        [SerializeField]
        private float waitTime = 0.01f;
        [SerializeField]
        private CinemachineVirtualCamera cinemachineCam = null;

        private IEnumerator shakeCoroutine = null;
        private Vector3 initialTransformPos = Vector3.zero;

        private void Move(float angle, float strength)
        {
            transform.AddLocalPositionX(Mathf.Cos(angle) * strength);
            transform.AddLocalPositionY(Mathf.Sin(angle) * strength);

        }

        private IEnumerator ScreenshakeCoroutine(float strength, int repeatCount)
        {
            if(cinemachineCam != null)
            {
                var noise = cinemachineCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                noise.m_AmplitudeGain = strength;
                noise.m_FrequencyGain = repeatCount;
                yield return new WaitForSeconds(repeatCount * 2 * waitTime);
                noise.m_AmplitudeGain = 0;
                noise.m_FrequencyGain = 0;
                yield break;
            }

            initialTransformPos = transform.localPosition;
            for (int i = 0; i < repeatCount / 2; ++i)
            {
                float angle = Random.Range(0, Mathf.PI);
                Move(angle, strength);
                yield return new WaitForSeconds(waitTime);
                Move(-angle, strength);
                yield return new WaitForSeconds(waitTime);

            }
            transform.localPosition = initialTransformPos;
            shakeCoroutine = null;


        }

        public void Shake(float strength = 0.1f, int repeatCount = 10)
        {
            if(shakeCoroutine != null)
            {
                StopCoroutine(shakeCoroutine);
                transform.localPosition = initialTransformPos;

            }
            shakeCoroutine = ScreenshakeCoroutine(strength, repeatCount);
            StartCoroutine(shakeCoroutine);
        }
    }
}
