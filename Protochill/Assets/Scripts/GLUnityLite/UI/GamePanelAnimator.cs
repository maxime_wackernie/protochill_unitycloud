﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.UI
{
    [RequireComponent(typeof(Animator))]
    public class GamePanelAnimator : MonoBehaviour
    {
        public enum ANIMATOR_STATE
        {
            ENTER,
            EXIT
        }

        public delegate void GamePanelAnimatorDelegate(ANIMATOR_STATE state, string stateName);
        private GamePanelAnimatorDelegate OnStateChangedEvent;

        public const string INTRO_TRIGGER = "Intro";
        public const string OUTRO_TRIGGER = "Outro";
        public const string HIDDEN_TRIGGER = "Hidden";

        private readonly int INTRO_HASH = Animator.StringToHash(INTRO_TRIGGER);
        private readonly int OUTRO_HASH = Animator.StringToHash(OUTRO_TRIGGER);
        private readonly int HIDDEN_HASH = Animator.StringToHash(HIDDEN_TRIGGER);

        private Animator animator = null;

        [SerializeField] private bool disableOnOutro = true;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        public void Intro()
        {
            if (gameObject.activeInHierarchy == false)
                gameObject.SetActive(true);

            animator.SetTrigger(INTRO_TRIGGER);
        }

        public void Outro()
        {
            animator.SetTrigger(OUTRO_TRIGGER);
        }

        public void StateChanged(ANIMATOR_STATE state, int hash)
        {
            string stateName = "";
            if (hash == INTRO_HASH)
            {
                stateName = INTRO_TRIGGER;
            }
            else if (hash == OUTRO_HASH)
            {
                stateName = OUTRO_TRIGGER;

                if (disableOnOutro && state == ANIMATOR_STATE.EXIT)
                    gameObject.SetActive(false);
            }
            else if (hash == HIDDEN_HASH)
            {
                stateName = HIDDEN_TRIGGER;

                if (disableOnOutro && state == ANIMATOR_STATE.ENTER)
                    gameObject.SetActive(false);
            }
            else return;

            if (OnStateChangedEvent != null)
                OnStateChangedEvent.Invoke(state, stateName);
        }

        public void SubscribeToOnStateChangedEvent(GamePanelAnimatorDelegate method)
        {
            if (method != null)
                OnStateChangedEvent += method;
        }

        public void UnsubscribeFromOnStateChangedEvent(GamePanelAnimatorDelegate method)
        {
            if (method != null)
                OnStateChangedEvent -= method;
        }
    }
}
