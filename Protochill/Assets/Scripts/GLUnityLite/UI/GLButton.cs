﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GLUnity.UI
{
    public class GLButton : MonoBehaviour
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private RectTransform rectTransform;
        private bool down = false;
        private Button buttonUI;
        private int touchId = -1;
        #endregion

        #region protected variables
        #endregion

        #region properties
        public Button ButtonUI
        {
            get
            {
                return buttonUI;
            }
        }

        public bool interactable
        {
            get
            {
                if(buttonUI == null)
                {
                    return false;
                }
                return buttonUI.interactable;
            }
            set
            {
                if(buttonUI != null)
                {
                    buttonUI.interactable = value;
                }                
            }
        }

        #endregion

        #region events
        public delegate void ButtonTouchedDelegate(GLButton button);
        public event ButtonTouchedDelegate OnButtonDown;
        public event ButtonTouchedDelegate OnButtonUp;
        #endregion

        #region constructor
        #endregion

        #region private methods
        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            buttonUI = GetComponent<Button>();
        }

        private void Update()
        {
            Touch[] touches = Input.touches;
            if (touches.Length > 0)
            {
                foreach (Touch touch in touches)
                {
                    switch (touch.phase)
                    {
                        case TouchPhase.Began:
                            if (IsInsideButton(touch.position) && buttonUI.interactable && !down)
                            {
                                touchId = touch.fingerId;
                                down = true;
                                if (OnButtonDown != null)
                                {
                                    OnButtonDown(this);
                                }
                            }
                            break;
                        case TouchPhase.Ended:
                            if (touch.fingerId == touchId && down && buttonUI.interactable)
                            {
                                down = false;
                                if (OnButtonUp != null)
                                {
                                    OnButtonUp(this);
                                }
                            }
                            break;
                    }
                }
                return;
            }
            if (Input.GetMouseButtonDown(0) && IsInsideButton(Input.mousePosition) && buttonUI.interactable && !down)
            {
                down = true;
                if (OnButtonDown != null)
                {
                    OnButtonDown(this);
                }
            }
            if (Input.GetMouseButtonUp(0) && down && buttonUI.interactable)
            {
                down = false;
                if (OnButtonUp != null)
                {
                    OnButtonUp(this);
                }
            }
        }

        private bool IsInsideButton(Vector3 position)
        {
            Vector3 localPosition = transform.InverseTransformPoint(position);
            return rectTransform.rect.Contains(localPosition);
        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        #endregion
    }
}
