﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GLUnity.IOC;


namespace GLUnity.UI
{
    public class GamePanel : MonoBehaviour
    {
        public delegate void PanelDelegate(GamePanel p);
        public event PanelDelegate introEvent = null;
        public event PanelDelegate outroEvent = null;
        [Header("Data Binding")]
        [SerializeField]
        private Button actionButton = null;


        [Header("Paramaters")]
        [SerializeField]
        private GameManager.GAME_STATE lookupState = GameManager.GAME_STATE.INTRO;
        [SerializeField]
        private GameManager.GAME_STATE nextState = GameManager.GAME_STATE.PLAYING;

        [Dependency]
        private GameManager gameManager = null;
        private GamePanelAnimator panelAnimator = null;

        private bool displayed = false;


        protected virtual void Start()
        {

            panelAnimator = GetComponent<GamePanelAnimator>();
            gameManager.stateChangedEvent += GameManager_StateChangedEvent;

            if (actionButton != null)
            {
                actionButton.onClick.AddListener(() =>
                {
                    GoToNextState();
                });
            }


            CheckGameManagerState();
        }


        private void CheckGameManagerState()
        {
            if (!displayed && gameManager.State == lookupState)
            {
                displayed = true;
                Intro();
            }
            else if (displayed && gameManager.State != lookupState)
            {
                Outro();
                displayed = false;
            }
        }

        protected virtual void Intro()
        {
            if (panelAnimator != null)
            {
                panelAnimator.Intro();
            }
            introEvent?.Invoke(this);
        }

        protected virtual void Outro()
        {
            if (panelAnimator != null)
            {
                panelAnimator.Outro();
            }
            outroEvent?.Invoke(this);
        }

        private void GameManager_StateChangedEvent(GameManager.GAME_STATE state)
        {
            CheckGameManagerState();
        }

        public void GoToNextState()
        {
            if (gameManager.State != lookupState)
            {
                return;
            }
            gameManager.State = nextState;
        }
    }
}


