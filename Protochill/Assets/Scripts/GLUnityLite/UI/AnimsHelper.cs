﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GLUnity.UI
{
    public class AnimsHelper : MonoBehaviour
    {

        #region constants
        public const string b_SHOWN = "isShown";
        #endregion

        #region enums
        #endregion

        #region exposed variables
        [SerializeField]
        private Animator[] animators = null;
        #endregion

        #region private variables
        private bool displayed = true;
        private bool autoDeactivate = true;
        #endregion

        #region protected variables
        #endregion

        #region properties
        public bool Displayed
        {
            get
            {
                return displayed;
            }
        }

        public bool AutoDeactivate
        {
            set
            {
                autoDeactivate = value;
            }
        }
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private void Update()
        {
            if (!displayed && autoDeactivate)
            {
                int hiddenCount = 0;
                foreach (var animator in animators)
                {
                    if (animator.gameObject.activeSelf &&
                        animator.runtimeAnimatorController != null &&
                        animator.GetCurrentAnimatorStateInfo(0).IsName("Hidden"))
                    {
                        ++hiddenCount;
                    }
                }
                if (hiddenCount == animators.Length)
                {
                    gameObject.SetActive(false);
                }
            }
        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public void ShowHide(bool show)
        {
            foreach (var animator in animators)
            {
                animator.SetBool(b_SHOWN, show);
            }
            displayed = show;
        }

        public void Show()
        {
            gameObject.SetActive(true);
            ShowHide(true);

        }

        public void Hide()
        {
            ShowHide(false);
        }

        public void WillDisplay()
        {
            gameObject.SetActive(true);
            displayed = true;
        }
        #endregion
    }
}
