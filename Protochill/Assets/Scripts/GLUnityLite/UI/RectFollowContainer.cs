﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

[RequireComponent(typeof(RectTransform))]
public class RectFollowContainer : MonoBehaviour
{
    [SerializeField]
    private Camera myCamera = null;

    [SerializeField]
    private Canvas canvas = null;

    [SerializeField]
    private bool update = false;

    private RectTransform canvasRect;
    private RectTransform rectTransform;
    private Vector3 referencePoint;

    public Camera MyCamera { get { return myCamera; } }
    public RectTransform CanvasRect { get { return canvasRect; } }

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasRect = canvas.GetComponent<RectTransform>();

        referencePoint = myCamera.transform.position + myCamera.transform.forward * 10f;
    }

    private void UpdateCanvasPosition()
    {
        Vector2 canvasPos = TransformUtils.WorldPosToCanvasPos(referencePoint, Vector3.zero, myCamera, canvasRect);
        rectTransform.anchoredPosition = canvasPos;
    }

    private void LateUpdate()
    {
        if (update)
        {
            UpdateCanvasPosition();
        }
    }
}