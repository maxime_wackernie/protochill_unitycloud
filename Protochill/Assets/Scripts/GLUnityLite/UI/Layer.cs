﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLUnity.UI
{
    public class Layer : MonoBehaviour
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private Panel currentPanel = null;
        private Dictionary<Panel, Panel> prefabToInstance = new Dictionary<Panel, Panel>();

        #endregion

        #region protected variables
        #endregion

        #region properties
        public int LayerIndex { get; set; }
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private void DoSetCurrentPanel(Panel panelPrefab)
        {
            Panel newPanel = null;
            prefabToInstance.TryGetValue(panelPrefab, out newPanel);
            if (newPanel == null)
            {
                newPanel = Instantiate(panelPrefab, transform);
                prefabToInstance[panelPrefab] = newPanel;
            }

            newPanel.Display();
            currentPanel = newPanel;
        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public void SetCurrentPanel(Panel panelPrefab)
        {
            if (currentPanel != null)
            {
                Panel.PanelDelegate onHiddenDelegate = null;
                onHiddenDelegate = (Panel p) =>
                {
                    p.OnHidden -= onHiddenDelegate;
                    DoSetCurrentPanel(panelPrefab);
                };
                currentPanel.OnHidden += onHiddenDelegate;
                HideCurrentPanel();
                return;
            }
            DoSetCurrentPanel(panelPrefab);
        }

        public void HideCurrentPanel()
        {
            if (currentPanel != null)
            {
                currentPanel.Hide();
                currentPanel = null;
            }
        }
        #endregion
    }

}
