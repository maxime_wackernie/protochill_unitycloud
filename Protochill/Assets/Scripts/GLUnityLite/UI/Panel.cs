﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GLUnity.Tweens;
using System;

namespace GLUnity.UI
{
    public class Panel : MonoBehaviour
    {

        public delegate void PanelDelegate(Panel p);
        public event PanelDelegate OnHidden = null;

        #region constants
        private const string INTRO_TRIGGER = "Intro";
        private const string OUTRO_TRIGGER = "Outro";

        #endregion

        #region enums
        public enum STATE
        {
            DISPLAYING,
            DISPLAYED,
            HIDDING,
            HIDDEN
        }
        #endregion

        #region exposed variables
        [SerializeField]
        private Animator animator = null;
        #endregion

        #region private variables
        private Image[] images = new Image[0];
        private Button[] buttons = new Button[0];
        #endregion

        #region protected variables
        #endregion

        #region properties
        public STATE State { get; private set; } = STATE.HIDDEN;
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private void RegisterTween(Tween tween, Action action)
        {
            Tween.TweenFinishedDelegate tweenFinished = null;
            tweenFinished = (Tween t) =>
            {
                t.OnTweenFinished -= tweenFinished;
                action();
            };
            tween.OnTweenFinished += tweenFinished;
        }

        private void SetRaycasActive(bool active)
        {
            foreach (var image in images)
            {
                image.raycastTarget = active;
            }
            foreach (var button in buttons)
            {
                button.interactable = active;
            }
        }
        #endregion

        #region protected methods
        protected virtual void Awake()
        {
            images = GetComponentsInChildren<Image>();
            buttons = GetComponentsInChildren<Button>();
        }

        protected virtual void IntroTween()
        {
            transform.localScale = Vector3.zero;
            Tween tween = TweenManager.Instance.DoScaleTween(EaseFunction.BounceOut, gameObject, Vector3.one, 0.5f);
            RegisterTween(tween, OnDisplayedEvent);
        }

        protected virtual void OutroTween()
        {
            transform.localScale = Vector3.one;
            Tween tween = TweenManager.Instance.DoScaleTween(EaseFunction.BounceIn, gameObject, Vector3.zero, 0.5f);
            RegisterTween(tween, OnHiddenEvent);
        }
        #endregion

        #region public methods
        public void Display()
        {
            SetRaycasActive(false);
            if (animator != null)
            {
                animator.SetTrigger(INTRO_TRIGGER);
            }
            else
            {
                IntroTween();
            }
            State = STATE.DISPLAYING;
        }

        public void Hide()
        {
            if (animator != null)
            {
                animator.SetTrigger(OUTRO_TRIGGER);
            }
            else
            {
                OutroTween();
            }
            SetRaycasActive(false);
            State = STATE.HIDDING;
        }

        public void OnHiddenEvent()
        {
            State = STATE.HIDDEN;
            OnHidden?.Invoke(this);
        }

        public void OnDisplayedEvent()
        {
            State = STATE.DISPLAYED;
            SetRaycasActive(true);
        }
        #endregion
    }

}
