﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using GLUnity;

[RequireComponent(typeof(RectTransform))]
public class RectFollowTarget : PoolableObject
{
    [Header("RectFollowTarget")]
    [SerializeField]
    protected Transform target = null;

    [SerializeField]
    private Vector3 offset = Vector3.zero;

    [SerializeField]
    private bool update = false;

    private RectFollowContainer rectFollowContainer;
    private RectTransform rectTransform;

    public RectTransform RectTransform { get { return rectTransform; } }
    public Transform Target { get { return target; } }
    public Vector3 Offset { get { return offset; } }


    protected virtual void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        rectFollowContainer = GetComponentInParent<RectFollowContainer>();

        Assert.IsTrue(rectFollowContainer != null, "RectFollowTarget has no RectFollowContainer in GetComponentInParent.");
    }

    private void Start()
    {
        UpdateCanvasPosition();
    }

    public void Init(Transform target)
    {
        this.target = target;

        UpdateCanvasPosition();
    }

    public void UpdateCanvasPosition()
    {
        if (target != null)
        {
            Vector2 canvasPos = TransformUtils.WorldPosToCanvasPos(target.position, offset, rectFollowContainer.MyCamera, rectFollowContainer.CanvasRect);
            rectTransform.anchoredPosition = canvasPos;
        }
    }

    private void LateUpdate()
    {
        if (update)
        {
            UpdateCanvasPosition();
        }
    }
}
