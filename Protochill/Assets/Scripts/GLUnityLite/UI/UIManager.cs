﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;

namespace GLUnity.UI
{
    public class UIManager : Singleton<UIManager>
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        [SerializeField]
        private Canvas canvas = null;
        [SerializeField]
        private List<Layer> layers = new List<Layer>();
        #endregion

        #region private variables
        #endregion

        #region protected variables
        #endregion

        #region properties
        public Canvas Canvas
        {
            get
            {
                return canvas;
            }
        }
        #endregion

        #region events
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private void Awake()
        {
            if (canvas == null)
            {
                canvas = FindObjectOfType<Canvas>();
            }
            if (layers.Count == 0)
            {
                AddLayer(0);
            }

        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public void SetCurrentPanel(Panel panelPrefab, int layerIndex = 0)
        {
            if (layerIndex < 0 || layerIndex >= layers.Count)
            {
                throw new System.Exception($"Wrong layer index {layerIndex} should be between 0 and {layers.Count}");
            }
            layers[layerIndex].SetCurrentPanel(panelPrefab);
        }

        public void HideCurrentPanel(int layerIndex = 0)
        {
            layers[layerIndex].HideCurrentPanel();
        }

        public void AddLayer(int layerIndex)
        {
            GameObject layerGO = new GameObject($"Layer_{layers.Count}");
            RectTransform rectT = layerGO.AddComponent<RectTransform>();

            rectT.anchorMin = new Vector2(0, 0);
            rectT.anchorMax = new Vector2(1, 1);
            rectT.offsetMin = new Vector2(0, 0);
            rectT.offsetMax = new Vector2(0, 0);

            Layer layer = layerGO.AddComponent<Layer>();
            layer.LayerIndex = layerIndex;
            rectT.SetParent(canvas.transform, false);
            layers.Add(layer);
            layers.Sort((Layer a, Layer b) => { return a.LayerIndex - b.LayerIndex; });
        }
        #endregion
    }
}


