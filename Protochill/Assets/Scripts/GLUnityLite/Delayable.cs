﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GLUnity
{
    public class Delayable : MonoBehaviour
    {

        #region constants
        #endregion

        #region enums
        #endregion

        #region exposed variables
        #endregion

        #region private variables
        private IEnumerator coroutine = null;
        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        public delegate void DelayDelegate();
        public delegate bool IntervalDelegate();
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods
        private IEnumerator DelayCoroutine(float delay, DelayDelegate delayDelegate)
        {
            yield return new WaitForSeconds(delay);
            delayDelegate();
            Destroy(this);
        }

        private IEnumerator IntervalCorountine(float interval, IntervalDelegate intervalDelegate, DelayDelegate endDel, int maxIterations = -1)
        {
            for (int i = 0; maxIterations == -1 ? true : i < maxIterations; ++i)
            {
                yield return new WaitForSeconds(interval);
                if (intervalDelegate())
                {
                    yield break;
                }
            }
            endDel();
        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        public static Delayable StartWithDelay(GameObject gameObject, float delay, DelayDelegate delayDelegate)
        {
            var delayable = gameObject.AddComponent<Delayable>();
            delayable.StartWithDelay(delay, delayDelegate);
            return delayable;
        }

        public static void StartInterval(GameObject gameObject, float interval, IntervalDelegate intervalDel, DelayDelegate endDel, int maxIterations = -1)
        {
            var delayable = gameObject.AddComponent<Delayable>();
            delayable.StartInterval(interval, intervalDel, endDel, maxIterations);
        }

        public void StartWithDelay(float delay, DelayDelegate delayDelegate)
        {
            if (coroutine != null)
            {
                ClearDelay();
            }
            if (delay == 0)
            {
                delayDelegate();
                return;
            }
            coroutine = DelayCoroutine(delay, delayDelegate);
            StartCoroutine(coroutine);
        }

        public void StartInterval(float interval, IntervalDelegate intervalDel, DelayDelegate endDel, int maxIterations = -1)
        {
            if (coroutine != null)
            {
                ClearDelay();
            }
            if (interval == 0)
            {
                intervalDel();
                endDel();
                return;
            }
            coroutine = IntervalCorountine(interval, intervalDel, endDel, maxIterations);
            StartCoroutine(coroutine);
        }

        public void ClearDelay()
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
            }
            coroutine = null;
        }
        #endregion
    }

}
