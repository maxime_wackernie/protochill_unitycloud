﻿using System.Collections.Generic;
using System;
using UnityEngine.Assertions;

namespace GLUnity
{
    public class TimerManager : Singleton<TimerManager>
    {
        #region private variables
        // Use for loop
        private List<Timer> timers;
        // Use for get
        private Dictionary<int, Timer> idTimerDictionary;
        #endregion

        #region private methods
        private void Awake()
        {
            timers = new List<Timer>();
            idTimerDictionary = new Dictionary<int, Timer>();
        }

        private void Update()
        {
            if (timers.Count > 0)
            {
                for (int i = timers.Count - 1; i > -1; --i)
                {
                    Timer timer = timers[i];

                    if (timer.State == ETimerState.RUNNING)
                    {
                        timer.AddTime();
                    }
                    else if (timer.State == ETimerState.REMOVING)
                    {
                        timers.RemoveAt(i);
                    }
                }
            }
        }

        private bool GetTimer(int id, out Timer timer)
        {
            if (idTimerDictionary.TryGetValue(id, out timer) == false)
            {
                throw new System.Exception("Timer not found. id: [" + id + "]");
            }

            return true;
        }

        private int GenerateUniqueId()
        {
            return Guid.NewGuid().GetHashCode();
        }

        private void AssertTimerDuration(float duration)
        {
            Assert.AreNotApproximatelyEqual(0f, duration, "Timer duration is equal to zero.");
            Assert.IsFalse(duration < 0f, "Timer duration is inferior than zero.");
        }

        #endregion

        #region public methods
        /// <summary>
        /// Create a new timer.
        /// </summary>
        /// <param name="duration"></param>
        /// <param name="startOnCreation"></param>
        /// <param name="loop"></param>
        /// <param name="finishedCallback"></param>
        /// <param name="runningCallback"> Callback while the timer is running.</param>
        /// <param name="useDeltatime">Should use deltatime, if false will use unscaled deltatime.</param>
        /// <returns> The timer's unique id. </returns>
        public int AddTimer(float duration, bool startOnCreation, bool loop = false, System.Action<int> completeCallback = null, System.Action<UpdateTimerData> updateCallback = null, bool useDeltatime = true)
        {
            AssertTimerDuration(duration);

            int id = GenerateUniqueId();

            Timer timer = new Timer(id, duration, startOnCreation, loop, completeCallback, updateCallback, useDeltatime);

            timers.Add(timer);
            idTimerDictionary.Add(id, timer);

            return id;
        }

        public bool RemoveTimer(int id)
        {
            Timer timer;

            if (GetTimer(id, out timer) == false)
                return false;

            timer.Remove();

            return idTimerDictionary.Remove(id);
        }

        /// <summary>
        /// Reset then start the timer.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="reset"></param>
        public void StartTimer(int id, bool reset = false)
        {
            Timer timer;

            if (GetTimer(id, out timer))
                timer.Start();
        }

        /// <summary>
        /// Pause the timer.
        /// </summary>
        /// <param name="id"></param>
        public void PauseTimer(int id)
        {
            Timer timer;

            if (GetTimer(id, out timer))
                timer.Pause();
        }

        /// <summary>
        /// Resume the timer.
        /// </summary>
        /// <param name="id"></param>
        public void ResumeTimer(int id)
        {
            Timer timer;

            if (GetTimer(id, out timer))
                timer.Resume();
        }

        /// <summary>
        /// Reset then stop the timer.
        /// </summary>
        /// <param name="id"></param>
        public void StopTimer(int id)
        {
            Timer timer;

            if (GetTimer(id, out timer))
                timer.Stop();
        }

        /// <summary>
        /// Set the timer's duration.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="duration"></param>
        /// <param name="reset"> Should reset the timer's progression </param> 
        public void SetTimerDuration(int id, float duration, bool reset = false)
        {
            AssertTimerDuration(duration);

            Timer timer;
            if (GetTimer(id, out timer))
                timer.SetDuration(duration, reset);
        }

        public void SubscribeToComplete(int id, Action<int> callback)
        {
            Timer timer;
            if (GetTimer(id, out timer))
                timer.SubscribeToComplete(callback);
        }

        public void SubscribeToUpdate(int id, Action<UpdateTimerData> callback)
        {
            Timer timer;
            if (GetTimer(id, out timer))
                timer.SubscribeToUpdate(callback);
        }

        public void UnsubscribeFromComplete(int id, Action<int> callback)
        {
            Timer timer;
            if (GetTimer(id, out timer))
                timer.UnsubscribeFromComplete(callback);
        }

        public void UnsubscribeFromUpdate(int id, System.Action<UpdateTimerData> callback)
        {
            Timer timer;
            if (GetTimer(id, out timer))
                timer.UnsubscribeFromUpdate(callback);
        }

        /// <summary>
        /// Get the timer's remaining time.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public float GetTimerTimeLeft(int id)
        {
            Timer timer;

            if (GetTimer(id, out timer))
                return timer.GetTimeLeft();

            return 0f;
        }

        /// <summary>
        /// Get the timer's elapsed percentage [0, 1]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public float GetTimerElapsedPercentage(int id)
        {
            Timer timer;

            if (GetTimer(id, out timer))
                return timer.GetElapsedPercentage();

            return 0f;
        }


        public bool IsTimerRunning(int id)
        {
            Timer timer;

            if (GetTimer(id, out timer))
                return timer.State == ETimerState.RUNNING;

            return false;
        }

        public bool HasTimer(int id)
        {
            return idTimerDictionary.ContainsKey(id);
        }
        #endregion
    }
}