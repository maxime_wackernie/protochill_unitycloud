﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ETimerState
{
    RUNNING,
    PAUSED,
    FINISHED,
    REMOVING
}

public struct UpdateTimerData
{
    public UpdateTimerData(Timer timer)
    {
        this.timer = timer;
    }

    private Timer timer;
   
    public int Id { get { return timer.Id; } }
    public float CurrentTime { get { return timer.CurrentTime; } }
    public float TimeLeft { get { return timer.GetTimeLeft(); } }
    public float Duration { get { return timer.Duration; } }
    public float ElapsedPercentage { get { return timer.GetElapsedPercentage(); } }
}

public class Timer 
{
    private int id;
    private float currentTime;
    private float duration;

    private bool loop;
    private bool useDeltatime;

    private UpdateTimerData updateTimerData;

    private System.Action<int> completeCallback;
    private System.Action<UpdateTimerData> updateCallback;

    private ETimerState state;
    public ETimerState State { get { return state; } }
    public int Id { get { return id; } }
    public float Duration { get { return duration; } }
    public float CurrentTime { get { return currentTime; } }


    public Timer(int id, float duration, bool startOnCreation, bool loop = false, System.Action<int> completeCallback = null, System.Action<UpdateTimerData> updateCallback = null, bool useDeltatime = true)
    {
        this.id = id;
        this.duration = duration;
        this.loop = loop;
        this.useDeltatime = useDeltatime;
        this.state = startOnCreation ? ETimerState.RUNNING : ETimerState.PAUSED;
        this.completeCallback += completeCallback;
        this.updateCallback += updateCallback;

        this.updateTimerData = new UpdateTimerData(this);
    }

    public void Start()
    {
        Reset();
        state = ETimerState.RUNNING;
    }

    public void Resume()
    {
        state = ETimerState.RUNNING;
    }

    public void Pause()
    {
        state = ETimerState.PAUSED;
    }

    public void Stop()
    {
        Reset();
        state = ETimerState.FINISHED;
    }

    private void Reset()
    {
        currentTime = 0f;
    }

    public void SetDuration(float duration, bool reset = false)
    {
        if (reset)
            Reset();

        this.duration = duration;
    }

    public void SubscribeToComplete(System.Action<int> callback)
    {
        completeCallback += callback;
    }

    public void UnsubscribeFromComplete(System.Action<int> callback)
    {
        completeCallback -= callback;
    }

    public void SubscribeToUpdate(System.Action<UpdateTimerData> callback)
    {
        updateCallback += callback;
    }

    public void UnsubscribeFromUpdate(System.Action<UpdateTimerData> callback)
    {
        updateCallback -= callback;
    }

    public void Remove()
    {
        state = ETimerState.REMOVING;
    }

    public float GetTimeLeft()
    {
        return duration - currentTime;
    }

    public float GetElapsedPercentage()
    {
        return currentTime / duration;
    }

    public void AddTime()
    {
        currentTime += useDeltatime ? Time.deltaTime : Time.unscaledDeltaTime;

        if (currentTime > duration)
        {
            if (loop)
            {
                Reset();
            }
            else
            {
                state = ETimerState.FINISHED;
            }

            if (completeCallback != null)
                completeCallback.Invoke(id);
        }
        else
        {
            if (updateCallback != null)
            {
                updateCallback.Invoke(updateTimerData);
            }
        }
    }
}
