﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GLUnity
{
    public class InputManager : Singleton<InputManager>
    {
        [SerializeField]
        private UnityEvent mouseButtonDownEvent = null;
        [SerializeField]
        private UnityEvent mouseButtonUpEvent = null;

        public struct HoldGesture
        {
            public Vector2 startPos;
            public Vector2 endPos;

            public override string ToString()
            {
                return $"HoldGesture : startPos {startPos} endPos {endPos}";
            }
        }

        public struct TapGesture
        {
            public Vector2 tapPos;

            public override string ToString()
            {
                return $"TapGesture : tapPos {tapPos}";
            }

        }

        public struct SwipeGesture
        {
            public SWIPE_DIRECTION swipeDirection;
            public Vector2 rawDirection;
            public override string ToString()
            {
                return $"SwipeGesture : swipeDirection {swipeDirection} rawDirection {rawDirection}";
            }

        }

        #region constants
        #endregion

        #region enums
        public enum SWIPE_DIRECTION
        {
            NONE,
            UP,
            UP_RIGHT,
            RIGHT,
            DOWN_RIGHT,
            DOWN,
            UP_LEFT,
            LEFT,
            DOWN_LEFT
        }

        private enum INPUT_TYPE
        {
            NONE,
            SWIPE,
            HOLD,
            TAP
        }
        #endregion

        #region exposed variables
        [SerializeField]
        private float minSwipeDistance = 10.0f;
        [SerializeField]
        private float minHoldSecs = 1f;
        #endregion

        #region private variables
        private bool touchDown = false;
        private Vector2 startPos = Vector2.zero;
        private float startTime = 0;
        private INPUT_TYPE inputType = INPUT_TYPE.NONE;
        private int currentTouchId = -1;

        private HoldGesture holdGesture;
        private TapGesture tapGesture;
        private SwipeGesture swipeGesture;

        #endregion

        #region protected variables
        #endregion

        #region properties
        #endregion

        #region events
        public delegate void SwipeDelegate(SwipeGesture swipeGesture);
        public delegate void TapDelegate(TapGesture tapGesture);
        public delegate void HoldDelegate(HoldGesture holdGesture);

        public event SwipeDelegate swipeEvent = null;
        public event HoldDelegate startHoldEvent = null;
        public event HoldDelegate endHoldEvent = null;
        public event TapDelegate tapEvent = null;
        #endregion

        #region Constructor / Destructor
        #endregion

        #region private methods

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                StartTouch(Input.mousePosition);

                if (mouseButtonDownEvent != null)
                {
                    mouseButtonDownEvent.Invoke();
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                EndTouch(Input.mousePosition);

                if (mouseButtonUpEvent != null)
                {
                    mouseButtonUpEvent.Invoke();
                }
            }
            else if (touchDown)
            {
                HoldTouch(Input.mousePosition);
            }
            foreach (Touch touch in Input.touches)
            {
                Vector2 touchPos = new Vector2(touch.position.x, touch.position.y);
                if(!touchDown && touch.phase == TouchPhase.Began)
                {
                    touchDown = true;
                    currentTouchId = touch.fingerId;
                    StartTouch(touchPos);
                }
                else if(touch.fingerId == currentTouchId)
                {
                    switch(touch.phase)
                    {
                        case TouchPhase.Ended:
                        case TouchPhase.Canceled:
                            EndTouch(touchPos);
                            break;
                        default:
                            HoldTouch(touchPos);
                            break;
                    }
                }
            }
        }

        private void StartTouch(Vector2 pos)
        {
            touchDown = true;
            this.startPos = pos;
            startTime = Time.time;
        }

        private void HoldTouch(Vector2 pos)
        {
            if (Time.time - startTime > minHoldSecs && 
                Vector2.Distance(pos, startPos) < minSwipeDistance && 
                inputType == INPUT_TYPE.NONE)
            {
                inputType = INPUT_TYPE.HOLD;
                holdGesture.startPos = startPos;
                holdGesture.endPos = Vector2.zero;
                startHoldEvent?.Invoke(holdGesture);
            }
        }

        private void EndTouch(Vector2 endPos)
        {
            switch(inputType)
            {
                case INPUT_TYPE.NONE:
                    float distance = Vector2.Distance(startPos, endPos);
                    if (distance > minSwipeDistance)
                    {
                        var rawDirection = endPos - startPos;
                        var direction = GetSwipeDirection(rawDirection);
                        swipeGesture.swipeDirection = direction;
                        swipeGesture.rawDirection = rawDirection;
                        swipeEvent?.Invoke(swipeGesture);
                    }
                    else
                    {
                        tapGesture.tapPos = endPos;
                        tapEvent?.Invoke(tapGesture);
                    }
                    break;
                case INPUT_TYPE.HOLD:
                    holdGesture.startPos = startPos;
                    holdGesture.endPos = endPos;
                    endHoldEvent?.Invoke(holdGesture);
                    break;
            }
            inputType = INPUT_TYPE.NONE;
            touchDown = false;

        }

        private SWIPE_DIRECTION GetSwipeDirection(Vector2 direction)
        {
            SWIPE_DIRECTION dir = SWIPE_DIRECTION.NONE;
            var angle = Vector2.SignedAngle(Vector2.up, direction.normalized);
            if(angle > 0)
            {
                dir = GetDirection(angle, SWIPE_DIRECTION.UP_LEFT);
            }
            else
            {
                dir = GetDirection(angle, SWIPE_DIRECTION.UP_RIGHT);
            }
            return dir;
        }

        private SWIPE_DIRECTION GetDirection(float angle, SWIPE_DIRECTION startDirection)
        {
            angle = Mathf.Abs(angle);
            if(angle < 22.5f)
            {
                return SWIPE_DIRECTION.UP;
            }
            else if(angle < 67.5f)
            {
                return startDirection;
            }
            else if(angle < 112.5f)
            {
                return (SWIPE_DIRECTION)((int)startDirection + 1);
            }
            else if(angle < 157.5f)
            {
                return (SWIPE_DIRECTION)((int)startDirection + 2);
            }
            return SWIPE_DIRECTION.DOWN;
            
        }
        #endregion

        #region protected methods
        #endregion

        #region public methods
        #endregion
    }

}
