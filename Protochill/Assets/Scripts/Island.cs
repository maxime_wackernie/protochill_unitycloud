﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLUnity;
using DG.Tweening;
using Facebook.Unity.Mobile.IOS;
using MoreMountains.NiceVibrations;

[System.Serializable]
public struct IslandSave
{
    public bool isUnlocked;
    public bool canBuy;
    public MyTileSave[] tileSaves;
}

[RequireComponent(typeof(MyTileMapComponent))]
public class Island : MonoBehaviour
{
    [SerializeField]
    private float price = 500f;
    [SerializeField]
    private bool startCanBuy = false;
    [SerializeField]
    private bool isUnlocked = false;
    [SerializeField]
    private Transform unlockTileTarget = null;
    [SerializeField]
    private Transform cloud = null;
    [SerializeField]
    private Vector3 cloudDirection = Vector3.forward;
    [SerializeField]
    private Transform[] bridges = null;
    [SerializeField]
    private Island[] canBuyIslands = null;

    private bool canBuy;
    private MyTileMapComponent tileMapComponent;

    //private static float cloudTransparencyValue = 2.29f;
    //private static int cloudTransparencyId = Shader.PropertyToID("Cloud_Transparency");

    public int IslandId { get { return transform.GetSiblingIndex(); } }

    public float resetIslandPrice = 0f;

    public bool IsUnlocked 
    {
        get { return isUnlocked; } 
        set
        {
            isUnlocked = value;

            if (bridges != null)
            {
                foreach (Transform bridge in bridges)
                {
                    bridge.Find("BrokenBridge").gameObject.SetActive(!value);
                    bridge.Find("ReparedBridge").gameObject.SetActive(value);
                }
            }
        }
    }

    public MyTileMapComponent TileMapComponent { get { return tileMapComponent; } }

    private RepairUI repairUI;

    private void Awake()
    {
        IsUnlocked = isUnlocked;

        tileMapComponent = GetComponent<MyTileMapComponent>();
    }

    private void OnDrawGizmos()
    {
        if (cloud != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(cloud.position, cloud.position + cloudDirection.normalized * 5f);
        }
    }

    public void Init()
    {
        if (PrototypeProfile.Instance.ProfileData.islandSaves[IslandId].tileSaves == null)
        {
            PrototypeProfile.Instance.ProfileData.islandSaves[IslandId].tileSaves = new MyTileSave[tileMapComponent.TileMap.TileCount];
        }

        // set to tiles if true (default is false)
        SetIsUncloked(isUnlocked, false);

        if (startCanBuy || canBuy)
        {
            SetCanBuy();
        }
    }

    public void Save()
    {
        PrototypeProfile.Instance.ProfileData.islandSaves[IslandId].isUnlocked = isUnlocked;
        PrototypeProfile.Instance.ProfileData.islandSaves[IslandId].canBuy = canBuy;

        foreach (MyTile tile in tileMapComponent.TileMap.GetTiles())
        {
            tile.Save();
        }
    }

    public void Load()
    {
        if (PrototypeProfile.Instance.ProfileData.islandSaves != null)
        {
            Load(PrototypeProfile.Instance.ProfileData.islandSaves[IslandId]);
        }
    }

    private void Load(IslandSave islandSave)
    {
        IsUnlocked = islandSave.isUnlocked;
        canBuy = islandSave.canBuy;

        Init();

        foreach (MyTileSave tileSave in islandSave.tileSaves)
        {
            TileMapComponent.TileMap.GetTile(tileSave.gridPosition.ToVector2Int()).Load(this, tileSave);
        }
    }

    public void ResetIsland()
    {
        float time = 0f;

        MyTile[] tiles = tileMapComponent.TileMap.GetTiles();

        foreach (MyTile tile in tiles)
        {
            if (tile.TileResource != null)
            {
                tile.StartReset();
            }
        }

        foreach (MyTile tile in tiles)
        {
            if (tile.TileResource != null)
            {
                tile.BounceTile(TweenValueManager.Instance.islandTilesDent, time, TweenValueManager.Instance.islandTilesDuration, Ease.OutCubic);
                time += TweenValueManager.Instance.islandTilesDelay;
                tile.ResetTile();
            }
        }
    }

    public List<MyTile> GetOccupedTiles()
    {
        List<MyTile> tiles = new List<MyTile>();
        foreach (MyTile tile in tileMapComponent.TileMap.GetTiles())
        {
            if (tile.TileResource != null)
                tiles.Add(tile);
        }
        return tiles;
    }

    public List<Animal> GetAllAnimals()
    {
        List<Animal> animals = new List<Animal>();
        foreach (MyTile tile in tileMapComponent.TileMap.GetTiles())
        {
            Habitat h = tile.GetComponent<Habitat>();
            if (h == null)
                continue;
            foreach (Animal a in h.Animals)
                animals.Add(a);     
        }
        return animals;
    }

    public void OnBuy()
    {
        foreach (Island island in canBuyIslands)
        {
            island.SetCanBuy();
        }

        Destroy(repairUI.gameObject);
        repairUI = null;
        SetIsUncloked(true, true);
    }    

    private void SetCanBuy()
    {
        if (isUnlocked == false)
        {
            canBuy = true;

            if (repairUI == null)
            {
                repairUI = PrefabHolder.Instance.InstantiatePrefab<RepairUI>("repair");
                repairUI.Init(this, unlockTileTarget, price);
            }
        }
    }

    private void SetIsUncloked(bool value, bool playAnimation)
    {
        IsUnlocked = value;

        if (value)
        {
            float time = 2.5f;
            foreach (MyTile tile in tileMapComponent.TileMap.GetTiles())
            {
                if (playAnimation)
                {
                    tile.BounceTile(TweenValueManager.Instance.islandTilesDent, time, TweenValueManager.Instance.islandTilesDuration, Ease.OutCubic,
                        () => { tile.IsUnlocked = true; } );

                    time += TweenValueManager.Instance.islandTilesDelay;
                    MMVibrationManager.Haptic(HapticTypes.MediumImpact);
                }
                else
                {
                    tile.IsUnlocked = value;
                }
            }
        }

        if (cloud != null)
        {
            if (playAnimation == false)
            {
                if (value)
                {
                    Destroy(cloud.gameObject);
                }
                else
                {
                    cloud.gameObject.SetActive(true);
                    //cloud.GetComponentInChildren<MeshRenderer>().sharedMaterial.SetFloat(cloudTransparencyId, cloudTransparencyValue);
                }
            }
        }

        if (value && playAnimation)
        {
            if (bridges != null)
            {
                foreach (Transform bridge in bridges)
                {
                    Transform target = bridge.Find("ReparedBridge");
                    Transform bridgePart;
                    Vector3 scale;
                    for (int i = 0; i < target.childCount; ++i)
                    {
                        bridgePart = target.GetChild(i);

                        scale = bridgePart.transform.localScale;
                        bridgePart.localScale = Vector3.zero;

                        Sequence sequence = DOTween.Sequence();
                        sequence.SetDelay(i * 0.05f);
                        sequence.Append(bridgePart.DOScale(scale * 1.1f, 0.1f));
                        sequence.Append(bridgePart.DOScale(scale, 0.05f));
                    }
                }
            }

            CloudAnimn(TweenValueManager.Instance.cloudMoveDuration, TweenValueManager.Instance.cloudMoveDistance);
        }
    }

    private void CloudAnimn(float duration, float distance)
    {
        if (cloud == null)
            return;

        Material material = cloud.GetComponentInChildren<MeshRenderer>().sharedMaterial;

        Vector3 targetPosition = cloud.position + cloudDirection.normalized * distance;

        cloud.DOMove(targetPosition, duration).SetEase(Ease.InOutSine).onComplete += () => { Destroy(cloud.gameObject); };

        /*Sequence cloudSequence = DOTween.Sequence();
        cloudSequence.Append(cloud.DOMove(targetPosition, duration));
        cloudSequence.Insert(duration / 2f, material.DOFloat(0f, cloudTransparencyId, duration / 2f));
        cloudSequence.onComplete += () => { Destroy(cloud.gameObject); };*/
    }
}
