﻿using GLUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenValueManager : Singleton<TweenValueManager>
{
    [Header("Spawn Resource")]
    [Range(0f, 1f)]
    public float generalDentRandomness = 0f;

    [Header("Spawn Resource")]
    public float heightSpawnOffset = 1.5f;
    public float spawnDuration = 0.3f;
    public float heightMoveDuration = 0.2f;
    public float mainTileDent = -0.3f;
    public float neighboursDent = -0.2f;
    public float mainTileDentDelay = 0.2f;
    public float neighboursDentTotalDuration = 0.32f;

    [Header("Click Resource")]
    public float clickTileDent = -0.15f;
    public float clickTileDuration = 0.15f;

    [Header("Island")]
    public float islandTilesDent = -0.15f;
    public float islandTilesDuration = 0.2f;
    public float islandTilesDelay = 0.03f;

    [Header("Cloud")]
    public float cloudMoveDuration = 10f;
    public float cloudMoveDistance = 20f;
    public float cloudScaleDuration = 1.5f;

    [Header("Occuped")]
    public bool heightOnOccupedTiles = true;
    public float occupedHeight = 0.2f;
    public float occupedDuration = 2f;

    [Header("Road")]
    public float spawnRoadDelay = 0.8f;
}
