﻿using Facebook.Unity.Settings;
using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
 using UnityEditor;
#endif 
using UnityEngine;

public class Protolytics : MonoBehaviour
{
    [SerializeField] GameObject gameAnalytics = null;
    [SerializeField] string iosGAKey = string.Empty;
    [SerializeField] string iosGASecretKey = string.Empty;
    [SerializeField] string appIDFB = string.Empty;

    private void Awake()
    {
        if (transform.parent == null)
            DontDestroyOnLoad(this);
        InitGameAnalytics();
        InitFacebook();
    }

    private void InitGameAnalytics()
    {
        GameObject clone = Instantiate(gameAnalytics);
        clone.name = "GameAnalytics";
        clone.SetActive(true);
        clone.transform.SetParent(transform);
        GameAnalytics.Initialize();
    }

    private void InitFacebook()
    {
        GameObject clone = new GameObject("Facebook");
        clone.AddComponent<FacebookManager>();
        clone.SetActive(true);
        clone.transform.SetParent(transform);
    }

    private void OnValidate()
    {
        // Apply ios Gameanalytics Key
        if (!GameAnalytics.SettingsGA.Platforms.Contains(RuntimePlatform.IPhonePlayer))
            GameAnalytics.SettingsGA.AddPlatform(RuntimePlatform.IPhonePlayer);
        var platformIndex = GameAnalytics.SettingsGA.Platforms.IndexOf(RuntimePlatform.IPhonePlayer);
        GameAnalytics.SettingsGA.UpdateGameKey(platformIndex, iosGAKey);
        GameAnalytics.SettingsGA.UpdateSecretKey(platformIndex, iosGASecretKey);
        GameAnalytics.SettingsGA.Build[platformIndex] = Application.version;
        GameAnalytics.SettingsGA.InfoLogBuild = true;
        GameAnalytics.SettingsGA.InfoLogEditor = true;


        // Apply Facebook gamekey
        FacebookSettings.AppIds = new List<string> { appIDFB };
        FacebookSettings.AppLabels = new List<string> { Application.productName };
#if UNITY_EDITOR
        EditorUtility.SetDirty(FacebookSettings.Instance);
#endif
        if (string.IsNullOrEmpty(appIDFB))
            Debug.LogError("appIDFB empty");

        if (string.IsNullOrEmpty(iosGAKey) || string.IsNullOrEmpty(iosGASecretKey))
            Debug.LogError("Ga keys empty");
    }

#if UNITY_EDITOR
    private void Reset()
    {
        gameAnalytics = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/ProtoLytics/GameAnalytics/Plugins/Prefabs/GameAnalytics.prefab");
    }
#endif
}
