﻿using UnityEngine;
using Facebook.Unity;
using System.Collections.Generic;

[DefaultExecutionOrder(10000)]
public class FacebookManager : MonoBehaviour
{
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            Debug.Log("Facebook initialized ");
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
            Debug.Log("Facebook AppID : " + FB.AppId);

            var tutParams = new Dictionary<string, object>();
            tutParams[AppEventParameterName.ContentID] = "Launched";
            tutParams[AppEventParameterName.Description] = "Launched";
            tutParams[AppEventParameterName.Success] = "1";

            FB.LogAppEvent(
                AppEventName.CompletedTutorial,
                parameters: tutParams
            );
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
           //Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
           // Time.timeScale = 1;
        }
    }
}
