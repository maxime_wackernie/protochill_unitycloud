﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ProtolyticsEditor : Editor
{
    [MenuItem("GAMELOFT/Protolytics/Spawn Protolytics")]
    private static void EditSettings()
    {
        if (!GameObject.Find("Protolytics"))
        {
            GameObject clone = new GameObject("Protolytics");
            clone.AddComponent<Protolytics>();
            GameObject parent = GameObject.Find("[DEPENDENCIES]");
            if (parent != null)
                clone.transform.SetParent(parent.transform);
            Selection.activeGameObject = clone;
        }
    }
}
