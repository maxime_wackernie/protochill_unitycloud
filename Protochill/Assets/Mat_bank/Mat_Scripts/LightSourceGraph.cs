﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LightSourceGraph : MonoBehaviour
{
    [SerializeField] string nameID = "LightDirection";

    private void Update()
    {
        Shader.SetGlobalVector(nameID, -transform.forward);
    }
}
